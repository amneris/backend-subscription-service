# Subscription Service

[![Build Status](http://jenkins.aba.land:8080/buildStatus/icon?job=Backend/Subscription Service/branches/develop)](http://jenkins.aba.land:8080/job/Backend/job/Subscription%20Service/job/branches/job/develop/)
[![Quality Gate](https://sonar.aba.land/api/badges/gate?key=com.abaenglish.backend.service%3Asubscription-service%3Adevelop)](https://sonar.aba.land/overview?id=com.abaenglish.backend.service%3Asubscription-service%3Adevelop)
[![Coverage](https://sonar.aba.land/api/badges/measure?key=com.abaenglish.backend.service%3Asubscription-service%3Adevelop&metric=coverage)](https://sonar.aba.land/overview?id=com.abaenglish.backend.service%3Asubscription-service%3Adevelop)

This microservice manages plans and subscriptions.

**Table of Contents**
<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Getting started](#getting-started)
	- [Database](#database)
	- [Message Broker](#message-broker)
- [How to use it](#how-to-use-it)

<!-- /TOC -->

## Getting started

This section is dedicated to show how to contribute to this project setting up the local environment.

## Prequisites

[Permit access to Nexus](https://github.com/abaenglish/software-guidebook/blob/master/tools/nexus.md)

### Database

This microservice needs a database connection and a schema called ```subscription-service```, you can use two methods.

* Installing [MySQL](https://dev.mysql.com/downloads/mysql/) and adding subscription-service schema.

or

* Running a docker image: ```docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=admin -e MYSQL_DATABASE=subscription-service --name mysql mysql:5.7```

### Message Broker

Subscription service needs a message broker. We use RabbitMQ so you have two options.

* Installing [RabbitMQ](https://www.rabbitmq.com/download.html).

or

* Running a docker image: ```docker run -p 4369:4369 -p 5671-5672:5671-5672 -p 15671-15672:15671-15672 -p 25672:25672 --name rabbitmq rabbitmq:3-management```

## How to use it

This section defines how to execute subscription service to be used as a resource for other projects as if it was running on a server in http://localhost:8080.

You can execute it as a service using our docker image as follows: ```docker run -p 8080:8080 --link mysql:database --link rabbitmq:rabbitmq nexus.aba.land:5000/subscription-service:latest --spring.datasource.url="jdbc:mysql://database/subscription-service?useSSL=false" --spring.rabbitmq.host=rabbitmq```

This method will need a database and a message broker. You can use the docker images described in the [previous section](#getting-started).

If you want to execute the service with an in-memory database and a dummy message broker you can execute it with additional parameters: ```docker run -p 8080:8080 nexus.aba.land:5000/subscription-service:1.1.1 --spring.datasource.url="jdbc:h2:mem:subscription;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE" --spring.datasource.driverClassName="org.h2.Driver" --spring.datasource.username="sa" --spring.datasource.password --spring.jpa.database-platform="org.hibernate.dialect.H2Dialect" --amqp.dummy=true```
