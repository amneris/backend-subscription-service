# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased
### Add
- Add QueryDSLConfiguration to allow complex queries by JPAQueryFactory
- Add snapshot repository
- Add new Payment Notification from zuora processors: PaymentProcessed from renewals
- Add new Subscription Notification Controller with processors: AmendRenewals, AmendCancellation

### Change
- Upgrade aba-boot dependency to last snapshot version
- Adapt test and publishers to the new version of EventPublisher
- Modify Notification controller endpoint to return a response 

### Remove
- Remove all service, controllers and more with plans
- Remove Refund/Renewal/Cancellation endpoints from SubscriptionController
- Remove entities related with CRONS process
- Removed smallestSubscriptionPeriod from application.yml

## [1.4.9]
### Add
- Add new background process that retry to send zuora billing information if it didn't send before.

## [1.4.8]
### Add
- Logs to trace errors finding transactions
### Changed
- Throw an exception when a transaction is not found
- Fix timestamp stored on PaymentTransaction

## [1.4.7]
### Changed
- Set zuora subscription cancellation effective date to due date from the invoice related.
- Change Response status of 'zuoraNotification' endpoint to 200 instead 202, to avoid Fail status on zuora callout
### Add
- New property o application.yml: Flag used creating account in zuora. Indicates if the customer wants to receive invoices through email.

## [1.4.4]
### Changed
- Update aba-boot to 1.4.6 to use new base image (openjdk).

## [1.4.3]
### Add
- Add new endpoint to receive notifications from Zuora
- Process payment declined notifications to cancel subscription related

## [1.4.2]
### Added
- add cache via aba-boot-starter-cache only when spring.enabled.cache: true is present in application.yml

## [1.4.1]
### Change
- Change External Zuora Service, change GMT -7 to Timezone Europe/Madrid
- Reduce header size in error messages sent to RabbitMQ 
- Refactor routingKeys names

## [1.4.0]
### Add
- Added parameter 'output' to cancelled, renewed and refunded RequestMapping.
- Add version 1.3.8 of aba-boot to log all requests done to the microservice.
- Add Publishers and Consumers for Refunds, Renewals and Cancellations
- Added user-service feign client
- Removed abawebapps external service
- Implements eureka server in this microservice
- Created CRUD transaction
- Added spring state machine
- Get partnerId from InsertUserResponse
- Added partnerId to CreateSubscriptionResponse
- Return null where output=false in renewed, cancelled and refunded
- Deleted in application.yml unused variables
- Completed cycle of payment transactions and its errors, includes spring state machine to process all payment transaction statuses (OPEN, PROCESS_ZUORA, PROCESS_ABA, CLOSED, ERROR)
- New endpoint to open transaction 'POST /api/v1/transactions'
- New endpoint to obtain transaction 'GET /api/v1/transactions/{publicId}' with expand options (dueDate, invoice)
- New endpoint to send event and update transaction 'PUT /api/v1/transactions/{publicId}'
- Adapt new Paypal payment way to transactions cycle
- Update external Zuora service, change timezone to GMT-7
- Remove hostedpages & signature
- Remove hostedpages & signature SQL
- Remove Country references
- Comment ErrorMessages changes & SQL version

### Added
- Setting the default initial dates for refunds, renewals and cancels through V1.5.0.04

### Change
- Change Currency, Country, UserZuora and InsertUser request and response from user-service
- Use of ServiceException from aba-boot instead of subscription-service

### Remove
- Remove unnnecessary exceptions from TransactionService methods

## [1.1.1]
### Added
- Get partnerId from InsertUserResponse
- Added partnerId to CreateSubscriptionResponse

## [1.0.20]
### Change
- Refactor external services.
- Delete Account Controller
- Deleted methods Subscription Controller/Services/...
- Added end-point and servce for refund
- Updated external-service to 1.0.30
- Fixed camelCase in RefundResponse
- Comented SQL UPDATE in V0.0.1.07__datasetAdyen.sql
- Changed in application.yml the ip from docker wm to localhost
- Changed point by comma in EUR currency.
- Changed end point for renew, refund and cancel and added persintence.
- Added external service 1.0.34 and it test for renew, cancel and refund
- Store PayPal IPN information and control fraud by PayPal transaction id
- Remove external services and added three new externals services, external abawebapps service, external zuora service and external selligent service
- Added Publichers

## [1.0.19]
### Change
- Updated correctly price in the subcription response.
- Added invoiceAmount to subscription response.
- Added invoiceNumber to subscription response.
- Added subscriptionInfo List object to subscription response
- Added paymentId to subscription response

## [1.0.13]
### Change
- Change and add exceptions in subscription service.

## [1.0.7]
### Add
- Update pom with a new version from external-sevices version 1.0.5.

## [1.0.6]
### Add
- Removed external services from this code and added from a external project.
- Remove all code from subcription rest-service and replace by another from using soap technology.

## [1.0.3]
### Add
- Added idCountry for new answer from abawebappsExternalService.getCountry.
- Substituted id in Plan class on priceList, answer from PlanController, by idproducts from abawebapps temporarily for a test in production.

## [1.0.2]
### Add
- Added paremeter for admin 99 products when catalog url is called.
- Added this file.
