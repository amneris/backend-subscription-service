package com.abaenglish.subscription.queue.publisher;

import com.abaenglish.boot.amqp.publisher.EventPublisher;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.queue.event.*;
import com.abaenglish.subscription.service.dto.subscription.CreateSubscriptionAbaServiceResponse;
import com.abaenglish.subscription.service.impl.SubscriptionService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import static org.apache.commons.lang.StringUtils.isEmpty;

@Aspect
@Component
public class PaymentTransactionEventPublisher extends EventPublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentTransactionEventPublisher.class);

    @Autowired
    private SubscriptionService subscriptionService;

    @AfterReturning(pointcut = "execution(* com.abaenglish.subscription.service.impl.TransactionService.openTransaction(..))", returning = "paymentTransaction")
    public void processOpenTransaction(PaymentTransaction paymentTransaction) {

        final StartTransactionEvent transactionEvent = StartTransactionEvent.Builder.aStartTransactionEvent()
                .publicId(paymentTransaction.getPublicId())
                .build();

        sendEvent(transactionEvent);
        LOGGER.info("Open transaction [{}]", paymentTransaction.getPublicId());
    }

    @AfterReturning(pointcut = "execution(* com.abaenglish.subscription.service.impl.TransactionService.processZuora(..))", returning = "publicId")
    public void processZuora(String publicId) {

        final ZuoraConfirmedEvent transactionEvent = ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent()
                .publicId(publicId)
                .build();

        sendEvent(transactionEvent);
        LOGGER.info("Confirmed subscription Zuora [{}]", publicId);
    }

	@AfterThrowing(pointcut = "execution(* com.abaenglish.subscription.service.impl.TransactionService.processZuora(..))", throwing = "e")
    public void processZuoraError(JoinPoint joinPoint, Throwable e) {
        Assert.isTrue(joinPoint.getArgs().length == 1);
        final String publicId = (String) joinPoint.getArgs()[0];
        LOGGER.error(String.format("There was an error in 'processZuora' with public id [%s], move event to error queue", publicId), e);
        //send error to the deadletter queue
        sendError(ZuoraConfirmedErrorEvent.Builder.aZuoraConfirmedErrorEvent().publicId(publicId).build(), e);
    }
    
    @AfterReturning(pointcut = "execution(* com.abaenglish.subscription.service.impl.TransactionService.processABA(..))", returning = "publicId")
    public void processABA(String publicId) {

        final AbaConfirmedEvent transactionEvent = AbaConfirmedEvent.Builder.aAbaConfirmedEvent()
                .publicId(publicId)
                .build();

        sendEvent(transactionEvent);
        LOGGER.info("Confirmed subscription ABA [{}]", publicId);
    }

    @AfterThrowing(pointcut = "execution(* com.abaenglish.subscription.service.impl.TransactionService.processABA(..))", throwing = "e")
    public void processABAError(JoinPoint joinPoint, Throwable e) {
        Assert.isTrue(joinPoint.getArgs().length == 1);
        final String publicId = (String) joinPoint.getArgs()[0];
        LOGGER.error(String.format("There was an error in 'processABA' with public id [%s], move event to error queue", publicId), e);
        //send error to the deadletter queue
        sendError(AbaConfirmedEventError.Builder.anAbaConfirmedEventError().publicId(publicId).build(), e);
    }

    // TODO it will be replace for next iteration ZOR-546
    @AfterReturning(pointcut = "execution(* com.abaenglish.subscription.service.impl.SubscriptionService.createSubscriptionABA(..))", returning = "createSubscriptionAbaServiceResponse")
    public void processToABABillInfo(final CreateSubscriptionAbaServiceResponse createSubscriptionAbaServiceResponse) {
        LOGGER.debug("Check that the bill information was set");
        final boolean setAgain = isEmpty(createSubscriptionAbaServiceResponse.getInsertUserRequest().getInvoiceNumber())
                || createSubscriptionAbaServiceResponse.getInsertUserRequest().getFinalPrice() == null;
        LOGGER.info("Bill information must be set again: ", setAgain);
        if(setAgain){
            subscriptionService.collectZuoraBillInformation(createSubscriptionAbaServiceResponse.getPaymentTransaction());
        }
    }
}
