package com.abaenglish.subscription.queue.consumer;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.queue.consumer.factory.PaymentTransactionFactory;
import com.abaenglish.subscription.queue.event.ZuoraConfirmedEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ZuoraConfirmedConsumer {

    private static final String QUEUE_NAME = "subscription-service.transaction-zuora-confirmed";

    @Autowired
    private PaymentTransactionFactory paymentTransactionFactory;

    @RabbitListener(queues = QUEUE_NAME)
    public void onMessage(ZuoraConfirmedEvent event) throws ServiceException {
        paymentTransactionFactory.zuoraConfirmed(event, QUEUE_NAME);
    }
}
