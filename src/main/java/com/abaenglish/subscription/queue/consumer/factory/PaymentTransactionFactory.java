package com.abaenglish.subscription.queue.consumer.factory;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.queue.event.AbaConfirmedEvent;
import com.abaenglish.subscription.queue.event.StartTransactionEvent;
import com.abaenglish.subscription.queue.event.ZuoraConfirmedEvent;
import com.abaenglish.subscription.service.ITransactionService;
import com.abaenglish.subscription.statemachine.Events;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentTransactionFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentTransactionFactory.class);

    @Autowired
    private ITransactionService paymentTransactionService;

    public void startPaymentTransaction(StartTransactionEvent event, String queue) throws ServiceException  {
        printEventReceived(queue, event.getEventType(), event.getPublicId());
        paymentTransactionService.change(event.getPublicId(), Events.START_PROCESS.name());
    }

    public void zuoraConfirmed(ZuoraConfirmedEvent event, String queue) throws ServiceException {
        printEventReceived(queue, event.getEventType(), event.getPublicId());
        paymentTransactionService.change(event.getPublicId(), Events.CONFIRMED_ZUORA.name());
    }

    public void abaConfirmed(AbaConfirmedEvent event, String queue) throws ServiceException {
        printEventReceived(queue, event.getEventType(), event.getPublicId());
        paymentTransactionService.change(event.getPublicId(), Events.CONFIRMED_ABA.name());
    }

    private void printEventReceived(String queue, String eventType, String publicId) {
        LOGGER.info("Received from {} queue <{}> with public id", queue, eventType, publicId);
    }
}
