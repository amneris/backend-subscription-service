package com.abaenglish.subscription.queue.consumer;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.queue.consumer.factory.PaymentTransactionFactory;
import com.abaenglish.subscription.queue.event.StartTransactionEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionStartConsumer {

    private static final String QUEUE_NAME = "subscription-service.transaction-start";

    @Autowired
    private PaymentTransactionFactory paymentTransactionFactory;

    @RabbitListener(queues = QUEUE_NAME, containerFactory = "rabbitRetryListenerContainerFactory")
    public void onMessage(StartTransactionEvent event) throws ServiceException {
        paymentTransactionFactory.startPaymentTransaction(event, QUEUE_NAME);
    }
}
