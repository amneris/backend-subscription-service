package com.abaenglish.subscription.queue;

import com.abaenglish.boot.amqp.domain.DomainEventType;

public enum PaymentTransactionEventType implements DomainEventType {

    START("subscription-service", "subscription-service.transaction-start"),
    ZUORA_CONFIRMED("subscription-service", "subscription-service.transaction-zuora-confirmed"),
    ZUORA_CONFIRMED_ERROR("subscription-service", "subscription-service.transaction-zuora-confirmed.deadletters"),

    ABA_CONFIRMED("subscription-service", "subscription-service.transaction-aba-confirmed"),
    ABA_CONFIRMED_ERROR("subscription-service", "subscription-service.transaction-aba-confirmed.deadletters");

    private String exchange;
    private String routingKey;

    PaymentTransactionEventType(String exchange, String routingKey) {
        this.exchange = exchange;
        this.routingKey = routingKey;
    }

    @Override
    public String getExchange() {
        return exchange;
    }

    @Override
    public String getRoutingKey() {
        return routingKey;
    }
}
