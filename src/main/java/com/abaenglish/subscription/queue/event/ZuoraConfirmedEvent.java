package com.abaenglish.subscription.queue.event;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import com.abaenglish.subscription.queue.PaymentTransactionEventType;

public class ZuoraConfirmedEvent extends DomainEvent {

    private static final long serialVersionUID = -5818094868206872929L;
    private String publicId;

    public ZuoraConfirmedEvent() {
        super(PaymentTransactionEventType.ZUORA_CONFIRMED);
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public static final class Builder {
        private String publicId;

        private Builder() {
        }

        public static ZuoraConfirmedEvent.Builder aZuoraConfirmedEvent() {
            return new ZuoraConfirmedEvent.Builder();
        }

        public ZuoraConfirmedEvent.Builder publicId(String publicId) {
            this.publicId = publicId;
            return this;
        }

        public ZuoraConfirmedEvent build() {
            ZuoraConfirmedEvent startTransactionEvent = new ZuoraConfirmedEvent();
            startTransactionEvent.setPublicId(publicId);
            return startTransactionEvent;
        }
    }
}
