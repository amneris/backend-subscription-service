package com.abaenglish.subscription.queue.event;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import com.abaenglish.subscription.queue.PaymentTransactionEventType;

public class AbaConfirmedEvent extends DomainEvent {

    private static final long serialVersionUID = -5700714873994900608L;
    private String publicId;

    public AbaConfirmedEvent() {
        super(PaymentTransactionEventType.ABA_CONFIRMED);
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public static final class Builder {
        private String publicId;

        private Builder() {
        }

        public static AbaConfirmedEvent.Builder aAbaConfirmedEvent() {
            return new AbaConfirmedEvent.Builder();
        }

        public AbaConfirmedEvent.Builder publicId(String publicId) {
            this.publicId = publicId;
            return this;
        }

        public AbaConfirmedEvent build() {
            AbaConfirmedEvent startTransactionEvent = new AbaConfirmedEvent();
            startTransactionEvent.setPublicId(publicId);
            return startTransactionEvent;
        }
    }
}
