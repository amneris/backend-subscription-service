package com.abaenglish.subscription.queue.event;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import com.abaenglish.subscription.queue.PaymentTransactionEventType;

public class ZuoraConfirmedErrorEvent extends DomainEvent {

    private static final long serialVersionUID = 7892423917100507713L;
    private String publicId;

    public ZuoraConfirmedErrorEvent() {
        super(PaymentTransactionEventType.ZUORA_CONFIRMED_ERROR);
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public static final class Builder {
        private String publicId;

        private Builder() {
        }

        public static Builder aZuoraConfirmedErrorEvent() {
            return new Builder();
        }

        public Builder publicId(String publicId) {
            this.publicId = publicId;
            return this;
        }

        public ZuoraConfirmedErrorEvent build() {
            ZuoraConfirmedErrorEvent zuoraConfirmedErrorEvent = new ZuoraConfirmedErrorEvent();
            zuoraConfirmedErrorEvent.setPublicId(publicId);
            return zuoraConfirmedErrorEvent;
        }
    }
}
