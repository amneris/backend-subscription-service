package com.abaenglish.subscription.queue.event;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import com.abaenglish.subscription.queue.PaymentTransactionEventType;

public class StartTransactionEvent extends DomainEvent {

    private static final long serialVersionUID = -9065140354019659780L;

    private String publicId;

    public StartTransactionEvent() {
        super(PaymentTransactionEventType.START);
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public static final class Builder {
        private String publicId;

        private Builder() {
        }

        public static Builder aStartTransactionEvent() {
            return new Builder();
        }

        public Builder publicId(String publicId) {
            this.publicId = publicId;
            return this;
        }

        public StartTransactionEvent build() {
            StartTransactionEvent startTransactionEvent = new StartTransactionEvent();
            startTransactionEvent.setPublicId(publicId);
            return startTransactionEvent;
        }
    }
}
