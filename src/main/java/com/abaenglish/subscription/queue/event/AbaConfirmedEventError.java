package com.abaenglish.subscription.queue.event;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import com.abaenglish.subscription.queue.PaymentTransactionEventType;

public class AbaConfirmedEventError extends DomainEvent {

    private static final long serialVersionUID = -3929625269338242807L;
    private String publicId;

    public AbaConfirmedEventError() {
        super(PaymentTransactionEventType.ABA_CONFIRMED_ERROR);
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }


    public static final class Builder {
        private String publicId;

        private Builder() {
        }

        public static Builder anAbaConfirmedEventError() {
            return new Builder();
        }

        public Builder publicId(String publicId) {
            this.publicId = publicId;
            return this;
        }

        public AbaConfirmedEventError build() {
            AbaConfirmedEventError abaConfirmedEventError = new AbaConfirmedEventError();
            abaConfirmedEventError.setPublicId(publicId);
            return abaConfirmedEventError;
        }
    }
}
