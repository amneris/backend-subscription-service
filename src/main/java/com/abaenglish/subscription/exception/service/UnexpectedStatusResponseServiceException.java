package com.abaenglish.subscription.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.exception.ErrorMessages;

public class UnexpectedStatusResponseServiceException extends ServiceException {

    public UnexpectedStatusResponseServiceException(ErrorMessages errorMessages) {
        super(errorMessages.getError());
    }

    public UnexpectedStatusResponseServiceException(ErrorMessages errorMessages, Throwable e) {
        super(errorMessages.getError().getMessage(), e);
    }

    public UnexpectedStatusResponseServiceException(String message) {
        super(message, null);
    }
}
