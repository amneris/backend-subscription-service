package com.abaenglish.subscription.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.exception.ErrorMessages;

public class BadRequestServiceException extends ServiceException {

    public BadRequestServiceException(ErrorMessages errorMessages) {
        super(errorMessages.getError());
    }

    public BadRequestServiceException(ErrorMessages errorMessages, Throwable e) {
        super(errorMessages.getError().getMessage(), e);
    }
}
