package com.abaenglish.subscription.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.exception.ErrorMessages;

public class IllegalArgumentServiceException extends ServiceException {

    public IllegalArgumentServiceException(ErrorMessages errorMessages) {
        super(errorMessages.getError());
    }

    public IllegalArgumentServiceException(ErrorMessages errorMessages, Throwable e) {
        super(errorMessages.getError().getMessage(), e);
    }

    public IllegalArgumentServiceException(String message) {
        super(message, null);
    }
}
