package com.abaenglish.subscription.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.exception.ErrorMessages;

public class ConflictServiceException extends ServiceException {

    private Exception originalException;

    public ConflictServiceException(ErrorMessages errorMessages) {
        super(errorMessages.getError());
    }

    public ConflictServiceException(String message, Throwable e) {
        super(message, e);
    }
}
