package com.abaenglish.subscription.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.exception.ErrorMessages;

public class ExpandedServiceException extends ServiceException {
    public ExpandedServiceException(String message, ErrorMessages errorMessage, Throwable t) {
        super(errorMessage.getError().getMessage() + ": " + message, t);
    }

    public ExpandedServiceException(String message, ErrorMessages errorMessage) {
        super(errorMessage.getError().getMessage() + ": " + message, null);
    }
}
