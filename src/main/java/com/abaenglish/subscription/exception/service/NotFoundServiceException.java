package com.abaenglish.subscription.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.exception.ErrorMessages;

public class NotFoundServiceException extends ServiceException {

    public NotFoundServiceException(ErrorMessages errorMessages) {
        super(errorMessages.getError());
    }

    public NotFoundServiceException(String message, Object... argArray) {
        super(String.format(message, argArray), null);
    }
}
