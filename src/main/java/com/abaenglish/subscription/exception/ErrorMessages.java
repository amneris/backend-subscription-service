package com.abaenglish.subscription.exception;

import com.abaenglish.boot.exception.CodeMessage;

/**
 * Move BAD_ORIGIN(SUB0001), ENVIRONMENT_NOT_FOUND(SUB00029) & COUNTRY_NOT_FOUND(SUB0012) to paymentform service
 * Remove SELLIGENT_MALFORMED_RESPONSE(SUB0007) to subscription service
 * Move SELLIGENT_RATE_PLAN_NOT_FOUND_ZUORA(SUB0003), CURRENCY_NOT_FOUND(SUB0011) to plan service
 */

public enum ErrorMessages {
    
    USER_NOT_FOUND(new CodeMessage("SUB0004", "User not found")),
    BAD_REQUEST_SUBSCRIPTION(new CodeMessage("SUB0005", "Problem with subscription")),
    INTERNAL_ZUORA_ERROR(new CodeMessage("SUB0006", "Internal Zuora error when try subscribe")),
    USER_EXISTS_IN_ZUORA(new CodeMessage("SUB0008", "User already exits in zuora")),
    WRONG_PARAMETER_VALUE(new CodeMessage("SUB0009", "Some parameter has a wrong value")),
    WRONG_PARAMETER_RANGE(new CodeMessage("SUB0010", "Some parameters are not within the correct range")),
    PAYMENT_TRANSACTION_NOT_FOUND(new CodeMessage("SUB0013", "Payment transaction not found")),
    EXPAND_BADREQUEST(new CodeMessage("SUB0014", "Wrong values on expand parameter")),
    CANCEL_SUBSCRIPTION_ERROR(new CodeMessage("SUB0015", "Cannot cancel the subscription")),
    SUBSCRIPTION_NOT_FOUND(new CodeMessage("SUB0016", "Subscription not found")),
    MANY_SUBSCRIPTIONS(new CodeMessage("SUB0017", "Too many subscriptions found for this id")),
    ZUORA_TOO_MUCH_INVOICES(new CodeMessage("SUB0018", "Too much invoices found for payment")),
    CANCEL_ACCOUNT_ERROR(new CodeMessage("SUB0019", "Cannot cancel the account")),
    NOTIFICATION_NOT_FOUND(new CodeMessage("SUB0020", "Not found notification by given id")),
    BAD_REQUEST_ABBAWEBAPPS(new CodeMessage("SUB0021", "Problem with abbawebapps")),
    WRONG_STATUS_RESPONSE(new CodeMessage("SUB0022", "We were waiting for another status response")),
    PAYMENT_PROCESSED_WRONG_ORIGIN(new CodeMessage("SUB0023", "The payment id is not from a renewal subscription.")),
    INVALID_NOTIFICATION_EVENT_CATEGORY(new CodeMessage("SUB0024", "Cannot process this event category.")),
    NOTIFICATION_ALREADY_PROCESSED(new CodeMessage("SUB0025", "This notification was already processed.")),
    STILL_PROCESSING_STATUS(new CodeMessage("SUB0026", "Zuora respond that is still processing a request."));

    private final CodeMessage error;

    ErrorMessages(CodeMessage errorMessage) {
        error = new CodeMessage(errorMessage.getCode(), errorMessage.getMessage());
    }

    public CodeMessage getError() {
        return error;
    }

}
