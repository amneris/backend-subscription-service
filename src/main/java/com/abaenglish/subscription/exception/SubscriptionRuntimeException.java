package com.abaenglish.subscription.exception;

public class SubscriptionRuntimeException extends RuntimeException {
    public SubscriptionRuntimeException(Throwable cause) {
        super(cause);
    }

}
