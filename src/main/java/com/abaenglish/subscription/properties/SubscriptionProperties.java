package com.abaenglish.subscription.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "subscription")
public class SubscriptionProperties {

    private Hostedpage hostedpage;
    private Cancellation cancellation;
    private BillInformation billInformation;


    public Hostedpage getHostedpage() {
        return hostedpage;
    }

    public void setHostedpage(Hostedpage hostedpage) {
        this.hostedpage = hostedpage;
    }

    public Cancellation getCancellation() {
        return cancellation;
    }

    public void setCancellation(Cancellation cancellation) {
        this.cancellation = cancellation;
    }

    public BillInformation getBillInformation() {
        return billInformation;
    }

    public void setBillInformation(BillInformation billInformation) {
        this.billInformation = billInformation;
    }

    public static class Cancellation {

        private boolean zuoraGenerateInvoice;
        private boolean zuoraProcessPayments;
        private Integer guarantee;

        public boolean getZuoraGenerateInvoice() {
            return zuoraGenerateInvoice;
        }

        public void setZuoraGenerateInvoice(boolean zuoraGenerateInvoice) {
            this.zuoraGenerateInvoice = zuoraGenerateInvoice;
        }

        public boolean getZuoraProcessPayments() {
            return zuoraProcessPayments;
        }

        public void setZuoraProcessPayments(boolean zuoraProcessPayments) {
            this.zuoraProcessPayments = zuoraProcessPayments;
        }

        public Integer getGuarantee() {
            return guarantee;
        }

        public void setGuarantee(Integer guarantee) {
            this.guarantee = guarantee;
        }
    }

    public static class Hostedpage {

        private String defaultcountryiso3;

        public String getDefaultcountryiso3() {
            return defaultcountryiso3;
        }

        public void setDefaultcountryiso3(String defaultcountryiso3) {
            this.defaultcountryiso3 = defaultcountryiso3;
        }
    }

    public static class BillInformation {

        private int maxAttempt;
        private long timeToSleep;

        public int getMaxAttempt() {
            return maxAttempt;
        }

        public void setMaxAttempt(int maxAttempt) {
            this.maxAttempt = maxAttempt;
        }

        public long getTimeToSleep() {
            return timeToSleep;
        }

        public void setTimeToSleep(long timeToSleep) {
            this.timeToSleep = timeToSleep;
        }
    }
}
