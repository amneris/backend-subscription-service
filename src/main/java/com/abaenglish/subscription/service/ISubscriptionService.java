package com.abaenglish.subscription.service;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.abawebapps.domain.RefundPaymentsRequest;
import com.abaenglish.external.abawebapps.domain.SubscriptionInfoResponse;
import com.abaenglish.external.paypal.domain.PaypalExternalTokenRequest;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.exception.service.NotFoundServiceException;
import com.abaenglish.subscription.service.dto.subscription.CreateSubscriptionServiceResponse;
import com.abaenglish.subscription.service.dto.subscription.ResultOperationServiceDto;
import com.abaenglish.subscription.service.dto.subscription.SubscriptionServiceRequest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ISubscriptionService {

    CreateSubscriptionServiceResponse createSubscription(SubscriptionServiceRequest subscription) throws ApiException, ServiceException;

    String createToken(PaypalExternalTokenRequest request) throws ServiceException;

    /**
     * Given an active subscription identifier, cancel it in Zuora platform.
     *
     * Check if given subscription identifier exists in zuora,
     * else return {@link NotFoundServiceException} when does not exists
     * or {@link com.abaenglish.subscription.exception.service.IllegalArgumentServiceException} when is not unique.
     *
     * Then cancel subscription in zuora with
     * {@code subscriptionId}, {@code contractEffectiveDate}, {@code effectiveDate} and {@code name} provided.
     *
     * @param subscriptionId
     *          Active subscription identifier
     * @param contractEffectiveDate
     *          ContractEffectiveDate is the date that your customer provided as the notice of cancellation
     * @param effectiveDate
     *          Effective Date is the date that actually specifies when an amend() to cancel a subscription should take effect.
     * @param name
     *          Specify a name descriptive for the cancellation
     *
     * @throws ServiceException
     */
    void cancelSubscriptionZuora(String subscriptionId, Date contractEffectiveDate, Date effectiveDate, String name) throws ServiceException;

    /**
     * Given a payment id from zuora, check if it is from a subscription renewal and then register it on abawebapps
     *
     * @param paymentId
     * @return
     * @throws ServiceException
     */
    List<ResultOperationServiceDto> registerRenewalPaymentOnSubscription(String paymentId) throws ServiceException;

    /**
     * Find a unique and completed amendment by criteria provided.
     * If subscriptionId is provided, then find an amendment in zuora for this filter
     * If amendmendId is provided, then find an amendment in zuora for this filter
     * If type is provided, then find an amendment in zuora for this filter
     *
     * @param subscriptionId
     * @param amendmentId
     * @param type
     * @return
     * @throws ServiceException
     *             If the amendment is not found or is not unique or is not completed, then return an error
     */
    ZuoraAmendmentResponse findAmendCompleted(Optional<String> subscriptionId, Optional<String> amendmentId, Optional<ZuoraExternalServiceSoap.ZAmendmentTypes> type) throws  ServiceException;

    List<ResultOperationServiceDto> registerRefundPaymentOnSubscription(final String refundId) throws ServiceException;

    List<SubscriptionInfoResponse> refundPaymentsAbbawebapps(List<RefundPaymentsRequest> refundPaymentsRequests) throws ServiceException;

    /**
     * Given an amendmendId and subscriptionOriginalId
     * Check that the amendment is a RENEWAL type and renew this subscription in abawebapps
     * Also check that this amendment is the last of the subscriptionOriginalId
     *
     * @param amendmentId
     * @param subscriptionOriginalId
     * @return
     * @throws ServiceException
     */
    List<ResultOperationServiceDto> registerRenewalSubscription(final String amendmentId, final String subscriptionOriginalId) throws ServiceException;

    /**
     * Given an amendmendId and subscriptionOriginalId
     * Check that the amendment is a CANCELLATION type and cancel this subscription in abawebapps
     *
     * @param amendmentId
     * @param subscriptionOriginalId
     * @return
     * @throws ServiceException
     */
    List<ResultOperationServiceDto> registerCancellationSubscription(final String amendmentId, final String subscriptionOriginalId) throws ServiceException;
}
