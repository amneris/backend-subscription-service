package com.abaenglish.subscription.service.dto.notification;

import java.io.Serializable;

public class PaymentDeclinedNotificationServiceDto extends NotificationServiceDto implements Serializable{

    private static final long serialVersionUID = 788314146240652911L;
    private String paymentId;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public static class Builder extends NotificationServiceDto.Builder<PaymentDeclinedNotificationServiceDto, PaymentDeclinedNotificationServiceDto.Builder> {

        public PaymentDeclinedNotificationServiceDto.Builder paymentId(String paymentId ) {
            type.setPaymentId(paymentId);
            return b;
        }
        @Override
        protected PaymentDeclinedNotificationServiceDto createObj() { return new PaymentDeclinedNotificationServiceDto(); }
        @Override
        protected PaymentDeclinedNotificationServiceDto.Builder getThis() { return this; }
    }
}
