package com.abaenglish.subscription.service.dto;

import java.io.Serializable;

public class InvoicePaymentServiceDto implements Serializable {

    private static final long serialVersionUID = 6147068124254391534L;

    private String paymentId;
    private String paymentDate;
    private String invoiceId;
    private String invoiceNumber;
    private String invoiceSubscriptionId;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceSubscriptionId() {
        return invoiceSubscriptionId;
    }

    public void setInvoiceSubscriptionId(String invoiceSubscriptionId) {
        this.invoiceSubscriptionId = invoiceSubscriptionId;
    }

    public static final class Builder {
        private String paymentId;
        private String paymentDate;
        private String invoiceId;
        private String invoiceNumber;
        private String invoiceSubscriptionId;

        private Builder() {
        }

        public static Builder anInvoicePaymentServiceDto() {
            return new Builder();
        }

        public Builder invoiceSubscriptionId(String invoiceSubscriptionId) {
            this.invoiceSubscriptionId = invoiceSubscriptionId;
            return this;
        }


        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder paymentDate(String paymentDate) {
            this.paymentDate = paymentDate;
            return this;
        }

        public Builder invoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public InvoicePaymentServiceDto build() {
            InvoicePaymentServiceDto invoicePaymentDto = new InvoicePaymentServiceDto();
            invoicePaymentDto.setPaymentId(paymentId);
            invoicePaymentDto.setPaymentDate(paymentDate);
            invoicePaymentDto.setInvoiceId(invoiceId);
            invoicePaymentDto.setInvoiceNumber(invoiceNumber);
            invoicePaymentDto.setInvoiceSubscriptionId(invoiceSubscriptionId);
            return invoicePaymentDto;
        }
    }
}
