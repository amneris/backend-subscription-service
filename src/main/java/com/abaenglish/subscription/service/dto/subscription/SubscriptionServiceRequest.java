package com.abaenglish.subscription.service.dto.subscription;

import com.abaenglish.subscription.domain.SubscriptionType;

import java.util.List;

public class SubscriptionServiceRequest {

    private Long userId;
    private String paymentId;
    private List<String> ratePlanId;
    private Integer period;
    private SubscriptionType type;
    private String paypalBaid;
    private String paypalEmail;
    private String paypalType;
    private String gatewayName;
    private String paypalToken;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public List<String> getRatePlanId() {
        return ratePlanId;
    }

    public void setRatePlanId(List<String> ratePlanId) {
        this.ratePlanId = ratePlanId;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public SubscriptionType getType() {
        return type;
    }

    public void setType(SubscriptionType type) {
        this.type = type;
    }

    public String getPaypalBaid() {
        return paypalBaid;
    }

    public void setPaypalBaid(String paypalBaid) {
        this.paypalBaid = paypalBaid;
    }

    public String getPaypalEmail() {
        return paypalEmail;
    }

    public void setPaypalEmail(String paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    public String getPaypalType() {
        return paypalType;
    }

    public void setPaypalType(String paypalType) {
        this.paypalType = paypalType;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getPaypalToken() {
        return paypalToken;
    }

    public void setPaypalToken(String paypalToken) {
        this.paypalToken = paypalToken;
    }

    public static final class Builder {
        private Long userId;
        private String paymentId;
        private List<String> ratePlanId;
        private Integer period;
        private SubscriptionType type;
        private String paypalBaid;
        private String paypalEmail;
        private String paypalType;
        private String gatewayName;
        private String paypalToken;

        private Builder() {
        }

        public static Builder aSubscriptionServiceRequest() {
            return new Builder();
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder ratePlanId(List<String> ratePlanId) {
            this.ratePlanId = ratePlanId;
            return this;
        }

        public Builder period(Integer period) {
            this.period = period;
            return this;
        }

        public Builder type(SubscriptionType type) {
            this.type = type;
            return this;
        }

        public Builder paypalBaid(String paypalBaid) {
            this.paypalBaid = paypalBaid;
            return this;
        }

        public Builder paypalEmail(String paypalEmail) {
            this.paypalEmail = paypalEmail;
            return this;
        }

        public Builder paypalType(String paypalType) {
            this.paypalType = paypalType;
            return this;
        }

        public Builder gatewayName(String gatewayName) {
            this.gatewayName = gatewayName;
            return this;
        }

        public Builder paypalToken(String paypalToken) {
            this.paypalToken = paypalToken;
            return this;
        }

        public SubscriptionServiceRequest build() {
            SubscriptionServiceRequest subscriptionServiceRequest = new SubscriptionServiceRequest();
            subscriptionServiceRequest.setUserId(userId);
            subscriptionServiceRequest.setPaymentId(paymentId);
            subscriptionServiceRequest.setRatePlanId(ratePlanId);
            subscriptionServiceRequest.setPeriod(period);
            subscriptionServiceRequest.setType(type);
            subscriptionServiceRequest.setPaypalBaid(paypalBaid);
            subscriptionServiceRequest.setPaypalEmail(paypalEmail);
            subscriptionServiceRequest.setPaypalType(paypalType);
            subscriptionServiceRequest.setGatewayName(gatewayName);
            subscriptionServiceRequest.setPaypalToken(paypalToken);
            return subscriptionServiceRequest;
        }
    }
}
