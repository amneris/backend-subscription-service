package com.abaenglish.subscription.service.dto.subscription;

import java.io.Serializable;

public class ResultOperationServiceDto implements Serializable {

    private static final long serialVersionUID = 5613651303514428727L;
    private String subscriptionId;
    private String amendmendId;
    private Boolean success;
    private String warnings;

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getAmendmendId() {
        return amendmendId;
    }

    public void setAmendmendId(String amendmendId) {
        this.amendmendId = amendmendId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getWarnings() {
        return warnings;
    }

    public void setWarnings(String warnings) {
        this.warnings = warnings;
    }

    public static final class Builder {
        private String subscriptionId;
        private String amendmendId;
        private Boolean success;
        private String warnings;

        private Builder() {
        }

        public static Builder aResultOperationServiceDto() {
            return new Builder();
        }

        public Builder subscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public Builder amendmendId(String amendmendId) {
            this.amendmendId = amendmendId;
            return this;
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public Builder warnings(String warnings) {
            this.warnings = warnings;
            return this;
        }

        public ResultOperationServiceDto build() {
            ResultOperationServiceDto resultOperationServiceDto = new ResultOperationServiceDto();
            resultOperationServiceDto.setSubscriptionId(subscriptionId);
            resultOperationServiceDto.setAmendmendId(amendmendId);
            resultOperationServiceDto.setSuccess(success);
            resultOperationServiceDto.setWarnings(warnings);
            return resultOperationServiceDto;
        }
    }
}
