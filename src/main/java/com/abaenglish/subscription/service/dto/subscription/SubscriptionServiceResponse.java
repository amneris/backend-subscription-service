package com.abaenglish.subscription.service.dto.subscription;

import java.io.Serializable;

public class SubscriptionServiceResponse implements Serializable {

	private static final long serialVersionUID = 7175157863202239397L;
    private String originalSubscriptionId;
    private String accountId;
    private String termStartDate;
    private String termEndDate;
    private String cancelledDate;
    private String amendmentId;

    public String getOriginalSubscriptionId() {
        return originalSubscriptionId;
    }

    public void setOriginalSubscriptionId(String originalSubscriptionId) {
        this.originalSubscriptionId = originalSubscriptionId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTermStartDate() {
        return termStartDate;
    }

    public void setTermStartDate(String termStartDate) {
        this.termStartDate = termStartDate;
    }

    public String getTermEndDate() {
        return termEndDate;
    }

    public void setTermEndDate(String termEndDate) {
        this.termEndDate = termEndDate;
    }

    public String getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(String cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getAmendmentId() {
        return amendmentId;
    }

    public void setAmendmentId(String amendmentId) {
        this.amendmentId = amendmentId;
    }

    public static final class Builder {
        private String originalSubscriptionId;
        private String accountId;
        private String termStartDate;
        private String termEndDate;
        private String cancelledDate;
        private String amendmentId;
        
        private Builder() {
        }

        public static Builder aSubscriptionServiceResponse() {
            return new Builder();
        }

        public Builder originalSubscriptionId(String originalSubscriptionId) {
            this.originalSubscriptionId = originalSubscriptionId;
            return this;
        }

        public Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder termStartDate(String termStartDate) {
            this.termStartDate = termStartDate;
            return this;
        }

        public Builder termEndDate(String termEndDate) {
            this.termEndDate = termEndDate;
            return this;
        }

        public Builder cancelledDate(String cancelledDate) {
            this.cancelledDate = cancelledDate;
            return this;
        }

        public Builder amendmentId(String amendmentId) {
            this.amendmentId = amendmentId;
            return this;
        }

        public SubscriptionServiceResponse build() {
            SubscriptionServiceResponse subscriptionServiceResponse = new SubscriptionServiceResponse();
            subscriptionServiceResponse.setOriginalSubscriptionId(originalSubscriptionId);
            subscriptionServiceResponse.setAccountId(accountId);
            subscriptionServiceResponse.setTermStartDate(termStartDate);
            subscriptionServiceResponse.setTermEndDate(termEndDate);
            subscriptionServiceResponse.setCancelledDate(cancelledDate);
            subscriptionServiceResponse.setAmendmentId(amendmentId);
            return subscriptionServiceResponse;
        }
    }
}
