package com.abaenglish.subscription.service.dto.subscription;

import java.io.Serializable;

public class PaypalCustomerDetails implements Serializable {

    private static final long serialVersionUID = -3189344213477312031L;
    private String token;
    private String payerEmail;
    private String payerId;
    private String checkoutStatus;
    private String transactionID;
    private String transactionType;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public String getCheckoutStatus() {
        return checkoutStatus;
    }

    public void setCheckoutStatus(String checkoutStatus) {
        this.checkoutStatus = checkoutStatus;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public static final class Builder {
        private String token;
        private String payerEmail;
        private String payerId;
        private String checkoutStatus;
        private String transactionID;
        private String transactionType;

        private Builder() {
        }

        public static Builder aPaypalCustomerDetails() {
            return new Builder();
        }

        public Builder token(String token) {
            this.token = token;
            return this;
        }

        public Builder payerEmail(String payerEmail) {
            this.payerEmail = payerEmail;
            return this;
        }

        public Builder payerId(String payerId) {
            this.payerId = payerId;
            return this;
        }

        public Builder checkoutStatus(String checkoutStatus) {
            this.checkoutStatus = checkoutStatus;
            return this;
        }

        public Builder transactionID(String transactionID) {
            this.transactionID = transactionID;
            return this;
        }

        public Builder transactionType(String transactionType) {
            this.transactionType = transactionType;
            return this;
        }

        public PaypalCustomerDetails build() {
            PaypalCustomerDetails paypalCustomerDetails = new PaypalCustomerDetails();
            paypalCustomerDetails.setToken(token);
            paypalCustomerDetails.setPayerEmail(payerEmail);
            paypalCustomerDetails.setPayerId(payerId);
            paypalCustomerDetails.setCheckoutStatus(checkoutStatus);
            paypalCustomerDetails.setTransactionID(transactionID);
            paypalCustomerDetails.setTransactionType(transactionType);
            return paypalCustomerDetails;
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "paypalToken='" + token + '\'' +
                    ", payerEmail='" + payerEmail + '\'' +
                    ", payerId='" + payerId + '\'' +
                    ", checkoutStatus='" + checkoutStatus + '\'' +
                    ", transactionID='" + transactionID + '\'' +
                    ", transactionType='" + transactionType + '\'' +
                    '}';
        }
    }
}
