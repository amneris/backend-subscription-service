package com.abaenglish.subscription.service.dto.notification;

import java.io.Serializable;

public class ZuoraNotificationServiceRequest implements Serializable {

    private static final long serialVersionUID = -45915228422872735L;

    private String eventDate;
    private String eventCategory;
    private String eventTimestamp;

    private String accountId;
    private String accountNumber;

    private String paymentGatewayState;
    private String paymentGatewayResponse;
    private String paymentGatewayResponseCode;

    private String paymentId;
    private String paymentStatus;
    private String paymentReferenceId;

    private String refundId;

    private String subscriptionVersionAmendmentId;
    private String subscriptionVersionAmendmentType;
    private String subscriptionOriginalId;

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getEventTimestamp() {
        return eventTimestamp;
    }

    public void setEventTimestamp(String eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPaymentGatewayState() {
        return paymentGatewayState;
    }

    public void setPaymentGatewayState(String paymentGatewayState) {
        this.paymentGatewayState = paymentGatewayState;
    }

    public String getPaymentGatewayResponse() {
        return paymentGatewayResponse;
    }

    public void setPaymentGatewayResponse(String paymentGatewayResponse) {
        this.paymentGatewayResponse = paymentGatewayResponse;
    }

    public String getPaymentGatewayResponseCode() {
        return paymentGatewayResponseCode;
    }

    public void setPaymentGatewayResponseCode(String paymentGatewayResponseCode) {
        this.paymentGatewayResponseCode = paymentGatewayResponseCode;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentReferenceId() {
        return paymentReferenceId;
    }

    public void setPaymentReferenceId(String paymentReferenceId) {
        this.paymentReferenceId = paymentReferenceId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getSubscriptionVersionAmendmentId() {
        return subscriptionVersionAmendmentId;
    }

    public void setSubscriptionVersionAmendmentId(String subscriptionVersionAmendmentId) {
        this.subscriptionVersionAmendmentId = subscriptionVersionAmendmentId;
    }

    public String getSubscriptionVersionAmendmentType() {
        return subscriptionVersionAmendmentType;
    }

    public void setSubscriptionVersionAmendmentType(String subscriptionVersionAmendmentType) {
        this.subscriptionVersionAmendmentType = subscriptionVersionAmendmentType;
    }

    public String getSubscriptionOriginalId() {
        return subscriptionOriginalId;
    }

    public void setSubscriptionOriginalId(String subscriptionOriginalId) {
        this.subscriptionOriginalId = subscriptionOriginalId;
    }

    @Override
    public String toString() {
        return "ZuoraNotificationServiceRequest{" +
                "eventDate='" + eventDate + '\'' +
                ", eventCategory='" + eventCategory + '\'' +
                ", eventTimestamp='" + eventTimestamp + '\'' +
                ", accountId='" + accountId + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", paymentGatewayState='" + paymentGatewayState + '\'' +
                ", paymentGatewayResponse='" + paymentGatewayResponse + '\'' +
                ", paymentGatewayResponseCode='" + paymentGatewayResponseCode + '\'' +
                ", paymentId='" + paymentId + '\'' +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", paymentReferenceId='" + paymentReferenceId + '\'' +
                ", refundId='" + refundId + '\'' +
                ", subscriptionVersionAmendmentId='" + subscriptionVersionAmendmentId + '\'' +
                ", subscriptionVersionAmendmentType='" + subscriptionVersionAmendmentType + '\'' +
                ", subscriptionOriginalId='" + subscriptionOriginalId + '\'' +
                '}';
    }

    public static final class Builder {
        private String eventDate;
        private String eventCategory;
        private String eventTimestamp;
        private String accountId;
        private String accountNumber;
        private String paymentGatewayState;
        private String paymentGatewayResponse;
        private String paymentGatewayResponseCode;
        private String paymentId;
        private String paymentStatus;
        private String paymentReferenceId;
        private String refundId;
        private String subscriptionVersionAmendmentId;
        private String subscriptionVersionAmendmentType;
        private String subscriptionOriginalId;

        private Builder() {
        }

        public static Builder aZuoraNotificationServiceRequest() {
            return new Builder();
        }

        public Builder eventDate(String eventDate) {
            this.eventDate = eventDate;
            return this;
        }

        public Builder eventCategory(String eventCategory) {
            this.eventCategory = eventCategory;
            return this;
        }

        public Builder eventTimestamp(String eventTimestamp) {
            this.eventTimestamp = eventTimestamp;
            return this;
        }

        public Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public Builder accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        public Builder paymentGatewayState(String paymentGatewayState) {
            this.paymentGatewayState = paymentGatewayState;
            return this;
        }

        public Builder paymentGatewayResponse(String paymentGatewayResponse) {
            this.paymentGatewayResponse = paymentGatewayResponse;
            return this;
        }

        public Builder paymentGatewayResponseCode(String paymentGatewayResponseCode) {
            this.paymentGatewayResponseCode = paymentGatewayResponseCode;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder paymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
            return this;
        }

        public Builder paymentReferenceId(String paymentReferenceId) {
            this.paymentReferenceId = paymentReferenceId;
            return this;
        }

        public Builder refundId(String refundId) {
            this.refundId = refundId;
            return this;
        }

        public Builder subscriptionVersionAmendmentId(String subscriptionVersionAmendmentId) {
            this.subscriptionVersionAmendmentId = subscriptionVersionAmendmentId;
            return this;
        }

        public Builder subscriptionVersionAmendmentType(String subscriptionVersionAmendmentType) {
            this.subscriptionVersionAmendmentType = subscriptionVersionAmendmentType;
            return this;
        }

        public Builder subscriptionOriginalId(String subscriptionOriginalId) {
            this.subscriptionOriginalId = subscriptionOriginalId;
            return this;
        }

        public ZuoraNotificationServiceRequest build() {
            ZuoraNotificationServiceRequest zuoraNotificationServiceRequest = new ZuoraNotificationServiceRequest();
            zuoraNotificationServiceRequest.setEventDate(eventDate);
            zuoraNotificationServiceRequest.setEventCategory(eventCategory);
            zuoraNotificationServiceRequest.setEventTimestamp(eventTimestamp);
            zuoraNotificationServiceRequest.setAccountId(accountId);
            zuoraNotificationServiceRequest.setAccountNumber(accountNumber);
            zuoraNotificationServiceRequest.setPaymentGatewayState(paymentGatewayState);
            zuoraNotificationServiceRequest.setPaymentGatewayResponse(paymentGatewayResponse);
            zuoraNotificationServiceRequest.setPaymentGatewayResponseCode(paymentGatewayResponseCode);
            zuoraNotificationServiceRequest.setPaymentId(paymentId);
            zuoraNotificationServiceRequest.setPaymentStatus(paymentStatus);
            zuoraNotificationServiceRequest.setPaymentReferenceId(paymentReferenceId);
            zuoraNotificationServiceRequest.setRefundId(refundId);
            zuoraNotificationServiceRequest.setSubscriptionVersionAmendmentId(subscriptionVersionAmendmentId);
            zuoraNotificationServiceRequest.setSubscriptionVersionAmendmentType(subscriptionVersionAmendmentType);
            zuoraNotificationServiceRequest.setSubscriptionOriginalId(subscriptionOriginalId);
            return zuoraNotificationServiceRequest;
        }
    }
}
