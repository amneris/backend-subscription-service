package com.abaenglish.subscription.service.dto.notification;

import java.io.Serializable;

public class PaymentProcessedNotificationServiceDto extends NotificationServiceDto implements Serializable{

    private static final long serialVersionUID = 3720266476139398087L;
    private String paymentId;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public static class Builder extends NotificationServiceDto.Builder<PaymentProcessedNotificationServiceDto, PaymentProcessedNotificationServiceDto.Builder> {

        public PaymentProcessedNotificationServiceDto.Builder paymentId(String paymentId ) {
            type.setPaymentId(paymentId);
            return b;
        }
        @Override
        protected PaymentProcessedNotificationServiceDto createObj() { return new PaymentProcessedNotificationServiceDto(); }
        @Override
        protected PaymentProcessedNotificationServiceDto.Builder getThis() { return this; }
    }
}
