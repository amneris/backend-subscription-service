package com.abaenglish.subscription.service.dto.notification;

import java.io.Serializable;

public class AmendProcessedNotificationServiceDto extends NotificationServiceDto implements Serializable {

    private static final long serialVersionUID = -3248851936769955741L;
    private String amendId;
    private String subscriptionOriginalId;
    private AmendmentType amendType;

    public String getAmendId() {
        return amendId;
    }

    public void setAmendId(String amendId) {
        this.amendId = amendId;
    }

    public AmendmentType getAmendType() {
        return amendType;
    }

    public void setAmendType(AmendmentType amendType) {
        this.amendType = amendType;
    }

    public String getSubscriptionOriginalId() {
        return subscriptionOriginalId;
    }

    public void setSubscriptionOriginalId(String subscriptionOriginalId) {
        this.subscriptionOriginalId = subscriptionOriginalId;
    }

    public static class Builder extends NotificationServiceDto.Builder<AmendProcessedNotificationServiceDto, AmendProcessedNotificationServiceDto.Builder> {

        public AmendProcessedNotificationServiceDto.Builder amendId(String amendId ) {
            type.setAmendId(amendId);
            return b;
        }
        public AmendProcessedNotificationServiceDto.Builder amendType(AmendmentType amendType ) {
            type.setAmendType(amendType);
            return b;
        }
        public AmendProcessedNotificationServiceDto.Builder subscriptionOriginalId(String originalId ) {
            type.setSubscriptionOriginalId(originalId);
            return b;
        }
        @Override
        protected AmendProcessedNotificationServiceDto createObj() { return new AmendProcessedNotificationServiceDto(); }
        @Override
        protected AmendProcessedNotificationServiceDto.Builder getThis() { return this; }
    }
}
