package com.abaenglish.subscription.service.dto.notification;

import com.abaenglish.subscription.controller.v1.dto.notification.NotificationEventCategories;

import java.io.Serializable;

public abstract class NotificationServiceDto implements Serializable {

    protected Long notificationId;
    protected NotificationEventCategories eventCategory;
    protected Boolean success;
    protected String errors;


    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    public NotificationEventCategories getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(NotificationEventCategories eventCategory) {
        this.eventCategory = eventCategory;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    protected abstract static class Builder<T extends NotificationServiceDto, B extends NotificationServiceDto.Builder<T, B>> {
        protected T type;
        protected B b;

        public Builder() {
            type = createObj();
            b = getThis();
        }

        public B notificationId(Long notificationId) {
            this.type.setNotificationId(notificationId);
            return b;
        }

        public B eventCategory(NotificationEventCategories eventCategory) {
            this.type.setEventCategory(eventCategory);
            return b;
        }

        public B success(Boolean success) {
            this.type.setSuccess(success);
            return b;
        }

        public B errors(String errors) {
            this.type.setErrors(errors);
            return b;
        }

        public T build() { return type; }
        protected abstract T createObj();
        protected abstract B getThis();
    }
}
