package com.abaenglish.subscription.service.dto.transaction;

import com.abaenglish.subscription.statemachine.States;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class TransactionServiceResponse implements Serializable{

    private static final long serialVersionUID = 6308109729887227530L;
    private Long id;
    private States state;
    private String publicId;
    private Long userId;
    private String type;
    private Integer period;
    private String subscriptionIdZuora;
    private String subscriptionNumberZuora;
    private String accountIdZuora;
    private String accountNumberZuora;
    private String invoiceIdZuora;
    // TODO needs LocalDateTimeConverter for Orika to auto-map
//    private LocalDateTime creationDate;
    private String dueDate;
    private BigDecimal finalPrice;
    private List<InvoiceItemInfo> invoiceItems;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public States getState() {
        return state;
    }

    public void setState(States state) {
        this.state = state;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getSubscriptionIdZuora() {
        return subscriptionIdZuora;
    }

    public void setSubscriptionIdZuora(String subscriptionIdZuora) {
        this.subscriptionIdZuora = subscriptionIdZuora;
    }

    public String getSubscriptionNumberZuora() {
        return subscriptionNumberZuora;
    }

    public void setSubscriptionNumberZuora(String subscriptionNumberZuora) {
        this.subscriptionNumberZuora = subscriptionNumberZuora;
    }

    public String getAccountIdZuora() {
        return accountIdZuora;
    }

    public void setAccountIdZuora(String accountIdZuora) {
        this.accountIdZuora = accountIdZuora;
    }

    public String getAccountNumberZuora() {
        return accountNumberZuora;
    }

    public void setAccountNumberZuora(String accountNumberZuora) {
        this.accountNumberZuora = accountNumberZuora;
    }

    public String getInvoiceIdZuora() {
        return invoiceIdZuora;
    }

    public void setInvoiceIdZuora(String invoiceIdZuora) {
        this.invoiceIdZuora = invoiceIdZuora;
    }

//    public LocalDateTime getCreationDate() {
//        return creationDate;
//    }
//
//    public void setCreationDate(LocalDateTime creationDate) {
//        this.creationDate = creationDate;
//    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public List<InvoiceItemInfo> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(List<InvoiceItemInfo> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public static final class Builder {
        private Long id;
        private States state;
        private String publicId;
        private Long userId;
        private String type;
        private Integer period;
        private String subscriptionIdZuora;
        private String subscriptionNumberZuora;
        private String accountIdZuora;
        private String accountNumberZuora;
        private String invoiceIdZuora;
//        private LocalDateTime creationDate;
        private String dueDate;
        private BigDecimal finalPrice;
        private List<InvoiceItemInfo> invoiceItems;

        private Builder() {
        }

        public static Builder aTransactionServiceResponse() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder state(States state) {
            this.state = state;
            return this;
        }

        public Builder publicId(String publicId) {
            this.publicId = publicId;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder period(Integer period) {
            this.period = period;
            return this;
        }

        public Builder subscriptionIdZuora(String subscriptionIdZuora) {
            this.subscriptionIdZuora = subscriptionIdZuora;
            return this;
        }

        public Builder subscriptionNumberZuora(String subscriptionNumberZuora) {
            this.subscriptionNumberZuora = subscriptionNumberZuora;
            return this;
        }

        public Builder accountIdZuora(String accountIdZuora) {
            this.accountIdZuora = accountIdZuora;
            return this;
        }

        public Builder accountNumberZuora(String accountNumberZuora) {
            this.accountNumberZuora = accountNumberZuora;
            return this;
        }

        public Builder invoiceIdZuora(String invoiceIdZuora) {
            this.invoiceIdZuora = invoiceIdZuora;
            return this;
        }

//        public Builder creationDate(LocalDateTime creationDate) {
//            this.creationDate = creationDate;
//            return this;
//        }

        public Builder dueDate(String dueDate) {
            this.dueDate = dueDate;
            return this;
        }

        public Builder finalPrice(BigDecimal finalPrice) {
            this.finalPrice = finalPrice;
            return this;
        }

        public Builder invoiceItems(List<InvoiceItemInfo> invoiceItems) {
            this.invoiceItems = invoiceItems;
            return this;
        }

        public TransactionServiceResponse build() {
            TransactionServiceResponse transactionServiceResponse = new TransactionServiceResponse();
            transactionServiceResponse.setId(id);
            transactionServiceResponse.setState(state);
            transactionServiceResponse.setPublicId(publicId);
            transactionServiceResponse.setUserId(userId);
            transactionServiceResponse.setType(type);
            transactionServiceResponse.setPeriod(period);
            transactionServiceResponse.setSubscriptionIdZuora(subscriptionIdZuora);
            transactionServiceResponse.setSubscriptionNumberZuora(subscriptionNumberZuora);
            transactionServiceResponse.setAccountIdZuora(accountIdZuora);
            transactionServiceResponse.setAccountNumberZuora(accountNumberZuora);
            transactionServiceResponse.setInvoiceIdZuora(invoiceIdZuora);
//            transactionServiceResponse.setCreat/ionDate(creationDate);
            transactionServiceResponse.setDueDate(dueDate);
            transactionServiceResponse.setFinalPrice(finalPrice);
            transactionServiceResponse.setInvoiceItems(invoiceItems);
            return transactionServiceResponse;
        }
    }
}
