package com.abaenglish.subscription.service.dto.subscription;

import com.abaenglish.external.abawebapps.domain.InsertUserRequest;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.user.controller.v1.dto.response.InsertUserApiResponse;

import java.io.Serializable;

public class CreateSubscriptionAbaServiceResponse implements Serializable {

    private static final long serialVersionUID = 917155934356458480L;
    private InsertUserRequest insertUserRequest;
    private InsertUserApiResponse insertUserApiResponse;
    private PaymentTransaction paymentTransaction;

    public InsertUserRequest getInsertUserRequest() {
        return insertUserRequest;
    }

    public void setInsertUserRequest(InsertUserRequest insertUserRequest) {
        this.insertUserRequest = insertUserRequest;
    }

    public InsertUserApiResponse getInsertUserApiResponse() {
        return insertUserApiResponse;
    }

    public void setInsertUserApiResponse(InsertUserApiResponse insertUserApiResponse) {
        this.insertUserApiResponse = insertUserApiResponse;
    }

    public PaymentTransaction getPaymentTransaction() {
        return paymentTransaction;
    }

    public void setPaymentTransaction(PaymentTransaction paymentTransaction) {
        this.paymentTransaction = paymentTransaction;
    }

    public static final class Builder {
        private InsertUserRequest insertUserRequest;
        private InsertUserApiResponse insertUserApiResponse;
        private PaymentTransaction paymentTransaction;

        private Builder() {
        }

        public static Builder aCreateSubscriptionAbaServiceResponse() {
            return new Builder();
        }
        public Builder paymentTransaction(PaymentTransaction paymentTransaction) {
            this.paymentTransaction = paymentTransaction;
            return this;
        }

        public Builder insertUserRequest(InsertUserRequest insertUserRequest) {
            this.insertUserRequest = insertUserRequest;
            return this;
        }

        public Builder insertUserApiResponse(InsertUserApiResponse insertUserApiResponse) {
            this.insertUserApiResponse = insertUserApiResponse;
            return this;
        }

        public CreateSubscriptionAbaServiceResponse build() {
            CreateSubscriptionAbaServiceResponse createSubscriptionAbaServiceResponse = new CreateSubscriptionAbaServiceResponse();
            createSubscriptionAbaServiceResponse.setInsertUserRequest(insertUserRequest);
            createSubscriptionAbaServiceResponse.setInsertUserApiResponse(insertUserApiResponse);
            createSubscriptionAbaServiceResponse.setPaymentTransaction(paymentTransaction);
            return createSubscriptionAbaServiceResponse;
        }
    }
}
