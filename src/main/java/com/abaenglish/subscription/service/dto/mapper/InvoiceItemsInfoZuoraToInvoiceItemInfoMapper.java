package com.abaenglish.subscription.service.dto.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceItemsInfo;
import com.abaenglish.subscription.service.dto.transaction.InvoiceItemInfo;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class InvoiceItemsInfoZuoraToInvoiceItemInfoMapper extends BaseCustomMapper<InvoiceItemsInfo, InvoiceItemInfo> {

    @Override
    public void mapAtoB(InvoiceItemsInfo invoiceItemsInfoA, InvoiceItemInfo invoiceItemInfoB, MappingContext context) {
        super.mapAtoB(invoiceItemsInfoA, invoiceItemInfoB, context);
        invoiceItemInfoB.setTotal(invoiceItemsInfoA.getChargeAmount().add(invoiceItemsInfoA.getTaxAmount()));
    }
}
