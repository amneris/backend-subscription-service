package com.abaenglish.subscription.service.dto.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.external.zuora.soap.domain.subscription.ZuoraSubscriptionResponse;
import com.abaenglish.subscription.service.dto.subscription.SubscriptionServiceResponse;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ZuoraSubscriptionResponseToSubscriptionServiceResponseMapper extends BaseCustomMapper<ZuoraSubscriptionResponse, SubscriptionServiceResponse> {
    public ZuoraSubscriptionResponseToSubscriptionServiceResponseMapper() {
        super();
        addField("id", "originalSubscriptionId");
    }

    @Override
    public void mapAtoB(ZuoraSubscriptionResponse zuoraSubscriptionResponse, SubscriptionServiceResponse subscriptionServiceResponse, MappingContext context) {
        super.mapAtoB(zuoraSubscriptionResponse, subscriptionServiceResponse, context);
        Optional.of(zuoraSubscriptionResponse.getOriginalId())
                .ifPresent(subscriptionServiceResponse::setOriginalSubscriptionId);
    }
}
