package com.abaenglish.subscription.service.dto.notification;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum AmendmentType {

    RENEWAL("Renewal"),
    CANCELLATION("Cancellation");

    private String type;
    /*
        Avoid clone list from values().
        Use valuesAsSet() instead of values()
     */
    private static final Set<AmendmentType> VALUE_LIST = Stream.of(values()).collect(Collectors.toSet());

    AmendmentType(String type) {
        this.type = type;
    }

    public static Optional<AmendmentType> parseAmendmentType(final String type) {
        return VALUE_LIST.stream()
                .filter( e -> e.type.equalsIgnoreCase(type) )
                .findFirst();
    }
}
