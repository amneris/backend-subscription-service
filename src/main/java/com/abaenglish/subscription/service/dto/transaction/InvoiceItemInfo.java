package com.abaenglish.subscription.service.dto.transaction;

import java.io.Serializable;
import java.math.BigDecimal;

public class InvoiceItemInfo implements Serializable{

    private static final long serialVersionUID = -8903083050329984254L;
    private String productId;
    private String productName;
    private Integer quantity;
    private BigDecimal total;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public static final class Builder {
        private String productId;
        private String productName;
        private Integer quantity;
        private BigDecimal total;

        private Builder() {
        }

        public static Builder anInvoiceItemInfo() {
            return new Builder();
        }

        public Builder productId(String productId) {
            this.productId = productId;
            return this;
        }

        public Builder productName(String productName) {
            this.productName = productName;
            return this;
        }

        public Builder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder total(BigDecimal total) {
            this.total = total;
            return this;
        }

        public InvoiceItemInfo build() {
            InvoiceItemInfo invoiceItemInfo = new InvoiceItemInfo();
            invoiceItemInfo.setProductId(productId);
            invoiceItemInfo.setProductName(productName);
            invoiceItemInfo.setQuantity(quantity);
            invoiceItemInfo.setTotal(total);
            return invoiceItemInfo;
        }
    }
}
