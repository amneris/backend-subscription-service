package com.abaenglish.subscription.service.dto.transaction;

import com.abaenglish.subscription.domain.SubscriptionType;

import java.io.Serializable;
import java.util.List;

public class PayloadServiceRequest implements Serializable {

    private static final long serialVersionUID = 8144013647363933046L;
    private Long userId;
    private SubscriptionType type;
    private String paymentId;
    private List<String> ratePlans;
    private Integer period;
    private String paypalToken;
    private String gatewayName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public SubscriptionType getType() {
        return type;
    }

    public void setType(SubscriptionType type) {
        this.type = type;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public List<String> getRatePlans() {
        return ratePlans;
    }

    public void setRatePlans(List<String> ratePlans) {
        this.ratePlans = ratePlans;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getPaypalToken() {
        return paypalToken;
    }

    public void setPaypalToken(String paypalToken) {
        this.paypalToken = paypalToken;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    @Override
    public String toString() {
        return "PayloadServiceRequest{" +
                "userId=" + userId +
                ", type=" + type +
                ", paymentId='" + paymentId + '\'' +
                ", ratePlans=" + ratePlans +
                ", period=" + period +
                ", paypalToken='" + paypalToken + '\'' +
                ", gatewayName=" + gatewayName +
                '}';
    }

    public static final class Builder {
        private Long userId;
        private SubscriptionType type;
        private String paymentId;
        private List<String> ratePlans;
        private Integer period;
        private String gatewayName;
        private String paypalToken;

        private Builder() {
        }

        public static Builder aPayload() {
            return new Builder();
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder type(SubscriptionType type) {
            this.type = type;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder ratePlans(List<String> ratePlans) {
            this.ratePlans = ratePlans;
            return this;
        }

        public Builder period(Integer period) {
            this.period = period;
            return this;
        }

        public Builder paypalToken(String paypalToken) {
            this.paypalToken = paypalToken;
            return this;
        }

        public Builder gatewayName(String gatewayName) {
            this.gatewayName = gatewayName;
            return this;
        }

        public PayloadServiceRequest build() {
            PayloadServiceRequest payloadServiceRequest = new PayloadServiceRequest();
            payloadServiceRequest.setUserId(userId);
            payloadServiceRequest.setType(type);
            payloadServiceRequest.setPaymentId(paymentId);
            payloadServiceRequest.setRatePlans(ratePlans);
            payloadServiceRequest.setPeriod(period);
            payloadServiceRequest.setGatewayName(gatewayName);
            payloadServiceRequest.setPaypalToken(paypalToken);
            return payloadServiceRequest;
        }
    }
}
