package com.abaenglish.subscription.service.dto.transaction;

import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.exception.service.IllegalArgumentServiceException;

import java.util.EnumSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Stream;

public enum TransactionExpand {
    INVOICE("invoice"),  DUE_DATE("dueDate");
    private String paramName;

    TransactionExpand(String paramName) {
        this.paramName = paramName;
    }

    public static TransactionExpand getTransactionExpand(String paramName) {
        return Stream.of(values()).filter( val -> val.paramName.equals(paramName)).findFirst().get();
    }

    public String getParamName() {
        return paramName;
    }

    public static EnumSet<TransactionExpand> getTransactionExpandSet(Set<String> paramNames) throws IllegalArgumentServiceException {
        EnumSet enumset = EnumSet.noneOf(TransactionExpand.class);
        if(paramNames != null && !paramNames.isEmpty()) {
            try {
                paramNames.stream().forEach(param -> enumset.add(TransactionExpand.getTransactionExpand(param)));
            } catch (NoSuchElementException e) {
                throw new IllegalArgumentServiceException(ErrorMessages.EXPAND_BADREQUEST, e);
            }
        }
        return enumset;
    }
}
