package com.abaenglish.subscription.service.dto.subscription;

import com.abaenglish.user.controller.v1.dto.response.UserZuoraApiResponse;
import com.zuora.api.axis2.ZuoraServiceStub;

import java.math.BigDecimal;

public class CreateSubscriptionServiceResponse {

    private ZuoraServiceStub.SubscribeResult[] subscribeResults;
    private UserZuoraApiResponse userZuora;
    private BigDecimal invoiceAmount;
    private String confirmationNumber;
    private Integer period;
    private String paymentId;
    private String partnerId;

    public ZuoraServiceStub.SubscribeResult[] getSubscribeResults() {
        return subscribeResults;
    }

    public void setSubscribeResults(ZuoraServiceStub.SubscribeResult[] subscribeResults) {
        this.subscribeResults = subscribeResults;
    }

    public UserZuoraApiResponse getUserZuora() {
        return userZuora;
    }

    public void setUserZuora(UserZuoraApiResponse userZuora) {
        this.userZuora = userZuora;
    }

    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(BigDecimal invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber) {
        this.confirmationNumber = confirmationNumber;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public static final class Builder {
        private ZuoraServiceStub.SubscribeResult[] subscribeResults;
        private UserZuoraApiResponse userZuora;
        private BigDecimal invoiceAmount;
        private String invoiceNumber;
        private Integer period;
        private String paymentId;
        private String partnerId;

        private Builder() {
        }

        public static Builder aCreateSubscriptionServiceResponse() {
            return new Builder();
        }

        public Builder subscribeResults(ZuoraServiceStub.SubscribeResult[] subscribeResults) {
            this.subscribeResults = subscribeResults;
            return this;
        }

        public Builder userZuora(UserZuoraApiResponse userZuora) {
            this.userZuora = userZuora;
            return this;
        }

        public Builder invoiceAmount(BigDecimal invoiceAmount) {
            this.invoiceAmount = invoiceAmount;
            return this;
        }

        public Builder invoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
            return this;
        }

        public Builder period(Integer period) {
            this.period = period;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder partnerId(String partnerId) {
            this.partnerId = partnerId;
            return this;
        }

        public CreateSubscriptionServiceResponse build() {
            CreateSubscriptionServiceResponse createSubscriptionServiceResponse = new CreateSubscriptionServiceResponse();
            createSubscriptionServiceResponse.setSubscribeResults(subscribeResults);
            createSubscriptionServiceResponse.setUserZuora(userZuora);
            createSubscriptionServiceResponse.setInvoiceAmount(invoiceAmount);
            createSubscriptionServiceResponse.setConfirmationNumber(invoiceNumber);
            createSubscriptionServiceResponse.setPeriod(period);
            createSubscriptionServiceResponse.setPaymentId(paymentId);
            createSubscriptionServiceResponse.setPartnerId(partnerId);
            return createSubscriptionServiceResponse;
        }
    }
}
