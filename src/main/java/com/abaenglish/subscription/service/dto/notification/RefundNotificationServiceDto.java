package com.abaenglish.subscription.service.dto.notification;

import java.io.Serializable;

public class RefundNotificationServiceDto extends NotificationServiceDto implements Serializable{

    private static final long serialVersionUID = 788314146240652911L;

    private String refundId;

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public static class Builder extends NotificationServiceDto.Builder<RefundNotificationServiceDto, RefundNotificationServiceDto.Builder> {

        public RefundNotificationServiceDto.Builder refundId(String refundId ) {
            type.setRefundId(refundId);
            return b;
        }
        @Override
        protected RefundNotificationServiceDto createObj() { return new RefundNotificationServiceDto(); }
        @Override
        protected RefundNotificationServiceDto.Builder getThis() { return this; }
    }
}
