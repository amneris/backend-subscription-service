package com.abaenglish.subscription.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.service.dto.notification.NotificationServiceDto;

public interface INotificationFactory {

    /**
     * Process the notification received
     *
     * @param dto
     * @return
     * @throws ServiceException
     */
    NotificationServiceDto process(NotificationServiceDto dto) throws ServiceException;
}
