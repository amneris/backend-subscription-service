package com.abaenglish.subscription.service.aspect;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.domain.ZuoraNotificationStatus;
import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.exception.service.ConflictServiceException;
import com.abaenglish.subscription.service.INotificationService;
import com.abaenglish.subscription.service.dto.notification.NotificationServiceDto;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Aspect
@Component
public class NotificationAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationAspect.class);

    @Autowired
    INotificationService notificationService;

    @AfterReturning(value = "execution(* com.abaenglish.subscription.service.impl.NotificationFactory.process(..))", returning = "dto")
    public void traceSuccessCancelledSubscription(final JoinPoint joinPoint, final NotificationServiceDto dto) throws ServiceException {
        LOGGER.debug("After {}", joinPoint.getSignature().getName());
        if (dto.getSuccess()) {
            notificationService.updateZuoraNotificationStatus(dto.getNotificationId(), ZuoraNotificationStatus.PROCESSED, Optional.empty());
        } else {
            notificationService.updateZuoraNotificationStatus(dto.getNotificationId(), ZuoraNotificationStatus.ERROR, Optional.of("Errors from Aba: " + dto.getErrors()));
        }
    }

    @AfterThrowing(value = "execution(* com.abaenglish.subscription.service.impl.NotificationFactory.process(..))", throwing = "e")
    public void traceErrorCancelledSubscription(JoinPoint joinPoint, Throwable e) throws ServiceException{
        LOGGER.debug("After error produced {}", joinPoint.getSignature().getName());
        Assert.isTrue(joinPoint.getArgs().length == 1);
        NotificationServiceDto dto = (NotificationServiceDto) joinPoint.getArgs()[0];

        if(e instanceof ConflictServiceException
                && ((ConflictServiceException) e).getCodeMessage().equals(ErrorMessages.PAYMENT_PROCESSED_WRONG_ORIGIN.getError())) {
            // don't mark it as an error, but set the reason
            notificationService.updateZuoraNotificationStatus(dto.getNotificationId(), ZuoraNotificationStatus.PROCESSED, Optional.of(ExceptionUtils.getRootCauseMessage(e)));
        } else {
            notificationService.updateZuoraNotificationStatus(dto.getNotificationId(), ZuoraNotificationStatus.ERROR, Optional.of(ExceptionUtils.getRootCauseMessage(e)));
        }
    }

}
