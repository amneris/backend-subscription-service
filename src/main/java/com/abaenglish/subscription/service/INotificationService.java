package com.abaenglish.subscription.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.domain.ZuoraNotification;
import com.abaenglish.subscription.domain.ZuoraNotificationStatus;
import com.abaenglish.subscription.exception.service.NotFoundServiceException;
import com.abaenglish.subscription.service.dto.notification.NotificationServiceDto;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;

import java.util.Optional;

public interface INotificationService {

    /**
     * Given a zuora notification {@link ZuoraNotificationServiceRequest}
     * then apply process depends on {@link ZuoraNotificationServiceRequest#getEventCategory()}
     *
     * Current event categories wich apply a process:
     *  -   PaymentDeclined
     *  -   PaymentProcessed
     *  -   PaymentRefundProcessed
     *  -   AmendmentProcessed (Renewal and Cancellation)
     *
     * @param notification
     *          Zuora notification to process
     * @return
     *         the notification saved on the repository or null if nothing was saved
     *
     * @throws ServiceException
     */
    Optional<NotificationServiceDto> processZuoraNotification(ZuoraNotificationServiceRequest notification) throws ServiceException;

    /**
     * Given an id notification, update its status and errors information
     *
     * @param notificationId
     *          mandatory, notification repository identifier
     * @param status
     *          mandatory, notification status to update, values {@link ZuoraNotificationStatus#PROCESSED}, {@link ZuoraNotificationStatus#ERROR}
     * @param error
     *          If {@code status} is {@link ZuoraNotificationStatus#ERROR}, then {@code error} it is advisable to be informed
     * @throws NotFoundServiceException
     */
    Optional<ZuoraNotification> updateZuoraNotificationStatus(Long notificationId, ZuoraNotificationStatus status, Optional<String> error) throws NotFoundServiceException;
}
