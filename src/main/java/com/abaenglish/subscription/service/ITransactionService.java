package com.abaenglish.subscription.service;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.service.dto.transaction.PayloadServiceRequest;
import com.abaenglish.subscription.service.dto.transaction.TransactionExpand;
import com.abaenglish.subscription.service.dto.transaction.TransactionServiceResponse;
import com.abaenglish.subscription.statemachine.Events;
import com.abaenglish.subscription.statemachine.States;

import java.util.Set;

public interface ITransactionService {
    String processZuora(String publicId) throws ApiException, ServiceException;

    String processABA(String publicId) throws ApiException, ServiceException;

    /**
     * Get Payment transaction details by given #publicId.
     * If some #expand items are provided, then fill the transaction response with extra information.
     * Allowed expand items:
     *  -{@link TransactionExpand#INVOICE}
     *      Fill response with invoice information related
     *  -{@link TransactionExpand#DUE_DATE}
     *      Fill response with due date of the subscription
     *
     * @param publicId
     * @param expand
     * @return
     * @throws ServiceException
     */
    TransactionServiceResponse getPaymentTransactionExpandedByPublicId(String publicId, Set<TransactionExpand> expand) throws ServiceException;

    PaymentTransaction getPaymentTransactionByPublicId(String publicId) throws ServiceException;

    PaymentTransaction openTransaction(PayloadServiceRequest payloadServiceRequest) throws ServiceException;

    PaymentTransaction updateTransaction(PaymentTransaction paymentTransaction);

    void change(String id, String event) throws ServiceException;

    PaymentTransaction restoreTransaction(String publicId, Events event, States previousState) throws ServiceException;
}
