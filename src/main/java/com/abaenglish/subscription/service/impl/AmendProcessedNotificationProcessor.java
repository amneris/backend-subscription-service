package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.abaenglish.subscription.controller.v1.dto.notification.NotificationEventCategories;
import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.exception.service.IllegalArgumentServiceException;
import com.abaenglish.subscription.service.ISubscriptionService;
import com.abaenglish.subscription.service.dto.notification.AmendProcessedNotificationServiceDto;
import com.abaenglish.subscription.service.dto.notification.AmendmentType;
import com.abaenglish.subscription.service.dto.subscription.ResultOperationServiceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class AmendProcessedNotificationProcessor extends NotificationProcessor<AmendProcessedNotificationServiceDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmendProcessedNotificationProcessor.class);

    @Autowired
    private ISubscriptionService subscriptionService;

    public AmendProcessedNotificationProcessor() {
        super(NotificationEventCategories.AMENDMENT_PROCESSED);
    }

    @Override
    public AmendProcessedNotificationServiceDto process(AmendProcessedNotificationServiceDto dto) throws ServiceException {
        LOGGER.info("Processing notification '{}' with amend id '{}' and type '{}'", eventCategory.name(),  dto.getAmendId(), dto.getAmendType() != null ? dto.getAmendType().name() : "");

        // load amendment type if not exists in dto
        if(dto.getAmendType() == null) {
            ZuoraAmendmentResponse amend = subscriptionService.findAmendCompleted(Optional.empty(), Optional.of(dto.getAmendId()), Optional.empty());
            LOGGER.info("Amendment type was not provided, found one amendment for amendment id '{}' with type '{}'", dto.getAmendId(), amend.getType());
            final Optional<AmendmentType> amendmentType = AmendmentType.parseAmendmentType(amend.getType());
            amendmentType.ifPresent(dto::setAmendType);
        }

        switch (dto.getAmendType()) {

            case RENEWAL:
                List<ResultOperationServiceDto> resultOperationServiceDtos =  subscriptionService.registerRenewalSubscription(dto.getAmendId(), dto.getSubscriptionOriginalId());
                if(resultOperationServiceDtos != null && !resultOperationServiceDtos.isEmpty()) {
                    // expect only one result
                    dto.setSuccess(resultOperationServiceDtos.get(0).getSuccess());
                    dto.setErrors(resultOperationServiceDtos.get(0).getWarnings());
                }
                break;
            case CANCELLATION:
                List<ResultOperationServiceDto> resultCancellationOperationServiceDtos =  subscriptionService.registerCancellationSubscription(dto.getAmendId(), dto.getSubscriptionOriginalId());
                if(resultCancellationOperationServiceDtos != null && !resultCancellationOperationServiceDtos.isEmpty()) {
                    // expect only one result
                    dto.setSuccess(resultCancellationOperationServiceDtos.get(0).getSuccess());
                    dto.setErrors(resultCancellationOperationServiceDtos.get(0).getWarnings());
                }
                break;
            default:
                LOGGER.warn("Amendment processed has not valid type '{}'", dto.getAmendType());
                throw new IllegalArgumentServiceException(ErrorMessages.INVALID_NOTIFICATION_EVENT_CATEGORY);
        }
        return dto;
    }
}
