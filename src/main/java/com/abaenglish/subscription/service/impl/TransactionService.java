package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.exception.service.queue.RetryableServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceItemsInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.ZuoraSubscriptionResponse;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.exception.service.ConflictServiceException;
import com.abaenglish.subscription.exception.service.NotFoundServiceException;
import com.abaenglish.subscription.repository.PaymentTransactionRepository;
import com.abaenglish.subscription.repository.predicate.PaymentTransactionPredicate;
import com.abaenglish.subscription.service.ITransactionService;
import com.abaenglish.subscription.service.dto.transaction.InvoiceItemInfo;
import com.abaenglish.subscription.service.dto.transaction.PayloadServiceRequest;
import com.abaenglish.subscription.service.dto.transaction.TransactionExpand;
import com.abaenglish.subscription.service.dto.transaction.TransactionServiceResponse;
import com.abaenglish.subscription.statemachine.Events;
import com.abaenglish.subscription.statemachine.States;
import com.abaenglish.user.controller.v1.dto.response.UserZuoraApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class TransactionService implements ITransactionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionService.class);
    public static final String MESSAGE_HEADER_PUBLIC_ID_HEADER = "ptId";

    @Autowired
    private PersistStateMachineHandler persistStateMachineHandler;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private IUserServiceFeign userService;

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private OrikaBeanMapper mapper;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @PostConstruct
    public void init() {
        this.persistStateMachineHandler.addPersistStateChangeListener(new PaymentTransactionPersistStateChangeListener());
    }

    @Override
    public PaymentTransaction openTransaction(final PayloadServiceRequest payloadServiceRequest) throws ServiceException {

        // search user
        UserZuoraApiResponse userZuora = userService.getUser(payloadServiceRequest.getUserId());

        /*
        * Check if user is already registered in zuora, in this version of subscriptions
        * users must be new in zuora.
        */
        if(userZuora.getZuora() != null) {
            throw new ConflictServiceException(ErrorMessages.USER_EXISTS_IN_ZUORA);
        }

        // create transaction in repository
        PaymentTransaction paymentTransaction = PaymentTransaction.Builder.aPaymentTransaction()
                .publicId()
                .userId(userZuora.getUser().getId())
                .state(States.OPEN)
                .type(payloadServiceRequest.getType().name())
                .creationDate(LocalDateTime.now())
                .period(payloadServiceRequest.getPeriod())
                .payload(payloadServiceRequest)
                .paypalToken(payloadServiceRequest.getPaypalToken())
                .gatewayName(payloadServiceRequest.getGatewayName())
                .build();
        LOGGER.info("Create payment transaction [{}] *Before", paymentTransaction.toString());
        final PaymentTransaction paymentTransaction1 = paymentTransactionRepository.save(paymentTransaction);
        LOGGER.info("Create payment transaction [{}] *After", paymentTransaction.toString());
        return paymentTransaction;
    }

    @Override
    public PaymentTransaction updateTransaction(PaymentTransaction paymentTransaction) {
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("Update payment transaction to [{}] with public id {}", paymentTransaction.toString(), paymentTransaction.getPublicId());
        }
        return paymentTransactionRepository.save(paymentTransaction);
    }

    @Override
    public String processZuora(final String publicId) throws ApiException, ServiceException {

        PaymentTransaction paymentTransaction = getPaymentTransactionByPublicId(publicId);
        LOGGER.debug("Process Zuora payment transaction [{}]", paymentTransaction.toString());
        UserZuoraApiResponse userZuora = userService.getUser(paymentTransaction.getUserId());

        subscriptionService.createSubscriptionZuora(paymentTransaction, userZuora);

        return publicId;
    }

    @Override
    public String processABA(final String publicId) throws ApiException, ServiceException {

        PaymentTransaction paymentTransaction = getPaymentTransactionByPublicId(publicId);
        LOGGER.debug("Process ABA payment transaction [{}]", paymentTransaction.toString());

        UserZuoraApiResponse userZuora = userService.getUser(paymentTransaction.getUserId());

        subscriptionService.createSubscriptionABA(paymentTransaction, userZuora);

        return publicId;
    }

    @Override
    public TransactionServiceResponse getPaymentTransactionExpandedByPublicId(final String publicId, final Set<TransactionExpand> expand) throws ServiceException {
        final PaymentTransaction paymentTransaction = getPaymentTransactionByPublicId(publicId);
        final TransactionServiceResponse serviceResponse = mapper.map(paymentTransaction, TransactionServiceResponse.class);


        // load expand info for due date
        if(expand.contains(TransactionExpand.DUE_DATE)) {
            //search in subscription the due date
            final List<ZuoraSubscriptionResponse> subscriptionInfoResponse = zuoraExternalService.getSubscriptionInfoById(paymentTransaction.getSubscriptionIdZuora(), ZuoraExternalServiceSoap.ZStatus.ACTIVE);

            if (subscriptionInfoResponse.size() == 1 ) {
                serviceResponse.setDueDate(subscriptionInfoResponse.get(0).getTermEndDate());
            }
        }

        // load expand info for invoice items
        if(expand.contains(TransactionExpand.INVOICE) && paymentTransaction.getInvoiceIdZuora() != null) {
            final InvoiceInfo invoiceInfo = zuoraExternalService.getInvoiceInfo(paymentTransaction.getInvoiceIdZuora());
            serviceResponse.setFinalPrice(invoiceInfo.getAmount());
            final List<InvoiceItemsInfo> invoiceItemsInfo = zuoraExternalService.getInvoiceItemsInfo(paymentTransaction.getInvoiceIdZuora());
            if(!invoiceItemsInfo.isEmpty()) {
                serviceResponse.setInvoiceItems(mapper.mapAsList(invoiceItemsInfo, InvoiceItemInfo.class));
            }
        }
        return serviceResponse;
    }

    @Override
    public PaymentTransaction getPaymentTransactionByPublicId(final String publicId) throws ServiceException {
        LOGGER.info("Find payment transaction with public id {}", publicId);
        Optional<PaymentTransaction> paymentTransaction = Optional.ofNullable(paymentTransactionRepository.findOne(PaymentTransactionPredicate.findByPublicId(publicId)));
        if(paymentTransaction.isPresent()) {

            return paymentTransaction.get();
        }
        LOGGER.error("Payment transaction not found with public id {}", publicId);
        throw new NotFoundServiceException(ErrorMessages.PAYMENT_TRANSACTION_NOT_FOUND);
    }

    @Override
    public void change(String id, String event) throws ServiceException {
        try {
            PaymentTransaction pt = getPaymentTransactionByPublicId(id);
            Message message = MessageBuilder.withPayload(event).setHeader(MESSAGE_HEADER_PUBLIC_ID_HEADER, id).build();
            persistStateMachineHandler.handleEventWithState(message, pt.getState().name());
            LOGGER.debug("Event ({}) sent to state machine for payment transaction [{}] with state [{}]", event, id, pt.getState().name());
        } catch (Exception e) {
            LOGGER.error(String.format("Something went wrong sending event [%s] to state machine for payment transaction with public id [%s]: ", event, id), e);
            throw new RetryableServiceException(e);
        }
    }

    @Override
    public PaymentTransaction restoreTransaction(String publicId, Events event, States previousState) throws ServiceException {

        // update transaction state on the repository
        PaymentTransaction paymentTransaction = getPaymentTransactionByPublicId(publicId);
        LOGGER.debug("Update transaction [{}] from state <{}> to <{}>", publicId, paymentTransaction.getState().name(), previousState.name());
        paymentTransaction.setState(previousState);
        paymentTransaction = updateTransaction(paymentTransaction);

        // update on the state machine
        LOGGER.debug("Send event {} and publicId {} to the state machine", event.name(), publicId);
        change(publicId, event.name());

        return paymentTransaction;
    }

    private class PaymentTransactionPersistStateChangeListener implements PersistStateMachineHandler.PersistStateChangeListener {

        @Override
        public void onPersist(State<String, String> state, Message<String> message, Transition<String, String> transition, StateMachine<String, String> stateMachine) {
            if (message != null && message.getHeaders().containsKey(MESSAGE_HEADER_PUBLIC_ID_HEADER)) {
                final String publicId = message.getHeaders().get(MESSAGE_HEADER_PUBLIC_ID_HEADER, String.class);
                try {
                    PaymentTransaction pt = getPaymentTransactionByPublicId(publicId);
                    LOGGER.debug("Prepare to update state on payment transaction with public id [{}] from <{}> to <{}>", publicId, pt.getState(), state.getId());

                    pt.setState(States.valueOf(state.getId())); // update state
                    paymentTransactionRepository.save(pt);
                } catch (Exception e) {
                    LOGGER.error(String.format("Something went wrong updating state on transaction with public id [%s]: ", publicId), e);
                }
            }
        }
    }
}
