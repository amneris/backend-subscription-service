package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.BadRequestApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.external.abawebapps.domain.*;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.external.paypal.domain.PaypalExternalCustomerDetails;
import com.abaenglish.external.paypal.domain.PaypalExternalTokenRequest;
import com.abaenglish.external.paypal.service.IPaypalExternalService;
import com.abaenglish.external.zuora.soap.domain.subscription.*;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.dto.CancelSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.dto.CreateSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.abaenglish.external.zuora.soap.service.impl.SubscriptionStatus;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.domain.SubscriptionType;
import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.exception.service.*;
import com.abaenglish.subscription.properties.SubscriptionProperties;
import com.abaenglish.subscription.service.IPaymentService;
import com.abaenglish.subscription.service.ISubscriptionService;
import com.abaenglish.subscription.service.dto.InvoicePaymentServiceDto;
import com.abaenglish.subscription.service.dto.subscription.*;
import com.abaenglish.user.controller.v1.dto.response.InsertUserApiResponse;
import com.abaenglish.user.controller.v1.dto.response.UserZuoraApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import com.zuora.api.axis2.UnexpectedErrorFault;
import com.zuora.api.axis2.ZuoraServiceStub;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

@Service
public class SubscriptionService implements ISubscriptionService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);
    @Autowired
    private IAbawebappsExternalService abawebappsExternalService;
    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalServiceSoap;
    @Autowired
    private IPaypalExternalService paypalService;
    @Autowired
    private OrikaBeanMapper mapper;
    @Autowired
    private IUserServiceFeign userService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private SubscriptionProperties subscriptionProperties;

    @Autowired
    private IPaymentService paymentService;

    @Override
    public List<SubscriptionInfoResponse> refundPaymentsAbbawebapps(List<RefundPaymentsRequest> refundPaymentsRequests) throws ServiceException {

        try {
            return abawebappsExternalService.refundSubscriptionsZuora(refundPaymentsRequests);
        } catch (NotFoundExternalException e) {
            logger.error("User not found in abawebapps Exception", e);
            throw new NotFoundServiceException(ErrorMessages.USER_NOT_FOUND);
        }
    }

    public void createSubscriptionZuora(PaymentTransaction paymentTransaction, UserZuoraApiResponse userZuora) throws ApiException, ServiceException {

        CreateSubscriptionRequest request = prepareRequestZuora(paymentTransaction, userZuora);
        paymentTransaction.setPeriod(request.getPeriod());
        ZuoraServiceStub.SubscribeResult[] results;
        try {
            results = zuoraExternalServiceSoap.createSubscription(request);
            updatePaymentTransactionByZuoraResponse(results[0], paymentTransaction);

        } catch (UnexpectedErrorFault | RemoteException e) {
            logger.error("Bad request subscribe request SOAP in Zuora. Exception", e);
            throw new BadRequestApiException(ErrorMessages.BAD_REQUEST_SUBSCRIPTION.getError());
        }
    }

    private void updatePaymentTransactionByZuoraResponse(final ZuoraServiceStub.SubscribeResult result, final PaymentTransaction paymentTransaction) throws ApiException {
        if (result.getSuccess()) {

            Optional.ofNullable(result.getSubscriptionId()).ifPresent( val ->
                    paymentTransaction.setSubscriptionIdZuora(val.toString()));

            Optional.ofNullable(result.getAccountId()).ifPresent( val ->
                    paymentTransaction.setAccountIdZuora(val.getID()));

            Optional.ofNullable(result.getAccountNumber()).ifPresent(paymentTransaction::setAccountNumberZuora);

            Optional.ofNullable(result.getSubscriptionNumber()).ifPresent(paymentTransaction::setSubscriptionNumberZuora);

            Optional.ofNullable(result.getInvoiceId()).ifPresent( val ->
                    paymentTransaction.setInvoiceIdZuora(val.toString()));

            Optional.ofNullable(result.getTotalTcv()).ifPresent( val ->
                    paymentTransaction.setPrice(val.setScale(2, BigDecimal.ROUND_HALF_UP)));

            transactionService.updateTransaction(paymentTransaction);

        } else {
            for (Integer count = 0; count < result.getErrors().length; count++) {
                logger.error(result.getErrors()[count].getCode().getValue(), result.getErrors()[count]);
            }
            throw new BadRequestApiException(ErrorMessages.INTERNAL_ZUORA_ERROR.getError());
        }
    }

    // TODO it will be replace for next iteration ZOR-546
    /**
     * Reties collect billing information from zuora to abawebapps only if paymentTransaction has getInvoiceIdZuora provided
     * Retry {@code getMaxAttempt} times if it is not collected and wait to retry {@code getTimeToSleep} to try again.
     *
     * @param paymentTransaction
     */
    public void collectZuoraBillInformation(final PaymentTransaction paymentTransaction) {

        if (isNotEmpty(paymentTransaction.getInvoiceIdZuora())) {
            boolean collected = false;
            try {
                for (int attempt = 0; attempt < subscriptionProperties.getBillInformation().getMaxAttempt() && !collected; attempt++) {

                    Optional<UpdateInvoiceResponse> updateInvoiceResponse = updateAbaFromZuoraBillInformation(paymentTransaction);
                    collected = updateInvoiceResponse.map(UpdateInvoiceResponse::getSuccess).orElse(false);

                    // check if info is collected to try again or not
                    if (!collected) {
                        logger.warn("Zuora billing information cannot be updated by [{}', try again {} attempt",
                                updateInvoiceResponse.map(UpdateInvoiceResponse::getWarnings).orElse(""),
                                attempt);
                        Thread.sleep(subscriptionProperties.getBillInformation().getTimeToSleep()); //TODO remove when queue retry will be implemented
                    }
                }
            } catch (Exception e) {
                logger.error("Bad request get invoice info for payment transaction {} and invoice id {} ", paymentTransaction.getPublicId(), paymentTransaction.getInvoiceIdZuora(), e);
            }
        } else {
            logger.error("There is not Invoice id zuora on Payment transaction '{}'", paymentTransaction.getPublicId());
        }
    }

    /**
     * Collect billing information from zuora and send it to abawebapps
     *
     * @param paymentTransaction
     * @return
     */
    private Optional<UpdateInvoiceResponse> updateAbaFromZuoraBillInformation(final PaymentTransaction paymentTransaction) {
        Optional<UpdateInvoiceResponse> updateInvoiceResponse = Optional.empty();
        logger.info("Collect invoice information from zuora, for invoice id {}", paymentTransaction.getInvoiceIdZuora());

        // collect from zuora
        final InvoiceInfo invoiceInfo = zuoraExternalServiceSoap.getInvoiceInfo(paymentTransaction.getInvoiceIdZuora());

        if(invoiceInfo != null && isNotEmpty(invoiceInfo.getInvoiceNumber())) {
            //update abawebapps
            updateInvoiceResponse = Optional.of(
                    abawebappsExternalService.updateInvoiceZuora(UpdateInvoiceRequest.Builder.anUpdateInvoiceRequest()
                            .invoiceId(paymentTransaction.getInvoiceIdZuora())
                            .invoiceNumber(invoiceInfo.getInvoiceNumber())
                            .totalAmount(invoiceInfo.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP))
                            .build()));
        }
        return updateInvoiceResponse;
    }

    public CreateSubscriptionAbaServiceResponse createSubscriptionABA(PaymentTransaction paymentTransaction, UserZuoraApiResponse userZuora) throws ApiException {

        InvoiceInfo invoiceInfo = new InvoiceInfo();
        InsertUserRequest iuq = new InsertUserRequest();

        BigDecimal finalPrice = new BigDecimal(0);

        try {
            if (paymentTransaction.getInvoiceIdZuora() != null) {
                invoiceInfo = zuoraExternalServiceSoap.getInvoiceInfo(paymentTransaction.getInvoiceIdZuora());

                finalPrice = invoiceInfo.getAmount();
            }
        } catch (Exception e) {
            logger.error("Bad request get amount. Exception: " + e);
        }

        iuq.setZuoraAccountId(paymentTransaction.getAccountIdZuora());
        iuq.setUserId(userZuora.getUser().getId());
        iuq.setZuoraAccountNumber(paymentTransaction.getAccountNumberZuora());

        iuq.setPeriodId(paymentTransaction.getPeriod());

        iuq.setCurrency(userZuora.getUser().getCurrency());
        iuq.setIdCountry(userZuora.getUser().getCountry());

        iuq.setInvoiceId(paymentTransaction.getInvoiceIdZuora());

        iuq.setSubscriptionId(paymentTransaction.getSubscriptionIdZuora());

        List<ZuoraSubscriptionResponse> subscriptionInfoResponse = zuoraExternalServiceSoap.getSubscriptionInfoById(paymentTransaction.getSubscriptionIdZuora(), ZuoraExternalServiceSoap.ZStatus.ACTIVE);

        if (!subscriptionInfoResponse.isEmpty()) {

            List<SubscriptionAbawebappsInfo> subscriptionAbawebappsRequest = mapper.mapAsList(subscriptionInfoResponse, SubscriptionAbawebappsInfo.class);
            iuq.setSubscriptionsList(subscriptionAbawebappsRequest);
        }

        iuq.setSubscriptionNumber(paymentTransaction.getSubscriptionNumberZuora());

        //@TODO
        List<String> ratePlanId = paymentTransaction.getPayload().getRatePlans();

        if (ratePlanId != null) {
            if (!ratePlanId.isEmpty()) {
                iuq.setIdRatePlanCharge(ratePlanId.get(0));
            }
            if (ratePlanId.size() > 1) {
                iuq.setIdDiscount(ratePlanId.get(1));
            }
        }

        iuq.setInvoiceNumber(invoiceInfo.getInvoiceNumber());
        iuq.setPrice(paymentTransaction.getPrice());
        iuq.setFinalPrice(finalPrice.setScale(2, BigDecimal.ROUND_HALF_UP));
        final CreateSubscriptionAbaServiceResponse serviceResponse = CreateSubscriptionAbaServiceResponse.Builder.aCreateSubscriptionAbaServiceResponse()
                .insertUserRequest(iuq)
                .paymentTransaction(paymentTransaction)
                .build();
        try {
            serviceResponse.setInsertUserApiResponse(userService.insertUser(iuq));
        } catch (Exception e) {
            logger.error("Bad request insert user in ABAWEBAPPS. Exception: " + e);
        }
        return serviceResponse;
    }

    /**
     * @param subscription
     * @return
     * @throws ApiException
     * @throws ServiceException
     * @deprecated remove this methdd when client user transactions
     */
    @Deprecated
    @Override
    public CreateSubscriptionServiceResponse createSubscription(SubscriptionServiceRequest subscription) throws ApiException, ServiceException {

        CreateSubscriptionServiceResponse responseService = new CreateSubscriptionServiceResponse();
        responseService.setPeriod(subscription.getPeriod());
        ZuoraServiceStub.SubscribeResult[] results;

        UserZuoraApiResponse userZuora;
        userZuora = userService.getUser(subscription.getUserId());
        CreateSubscriptionRequest request = prepareRequestZuora(subscription, userZuora);

        try {
            results = zuoraExternalServiceSoap.createSubscription(request);
        } catch (UnexpectedErrorFault | RemoteException e) {
            logger.error("Bad request subscribe request SOAP in Zuora. Exception: " + e);
            throw new BadRequestApiException(ErrorMessages.BAD_REQUEST_SUBSCRIPTION.getError());
        }

        BigDecimal finalPrice = new BigDecimal(0);

        try {
            if (results[0].getInvoiceId() != null) {
                finalPrice = zuoraExternalServiceSoap.getInvoiceAmount(results[0].getInvoiceId().toString());

                responseService.setInvoiceAmount(finalPrice);
            }
        } catch (Exception e) {
            logger.error("Bad request get amount. Exception: " + e);
        }

        if (results[0].getSuccess()) {

            InsertUserRequest iuq = new InsertUserRequest();

            iuq.setZuoraAccountId(results[0].getAccountId().getID());
            iuq.setUserId(userZuora.getUser().getId());
            iuq.setZuoraAccountNumber(results[0].getAccountNumber());

            iuq.setPeriodId(subscription.getPeriod());

            iuq.setCurrency(userZuora.getUser().getCurrency());
            iuq.setIdCountry(userZuora.getUser().getCountry());

            if (results[0].getInvoiceId() != null) {
                iuq.setInvoiceId(results[0].getInvoiceId().toString());
            }

            if (results[0].getInvoiceNumber() != null) {
                iuq.setInvoiceNumber(results[0].getInvoiceNumber());
            }

            iuq.setPrice(results[0].getTotalTcv().setScale(2, BigDecimal.ROUND_HALF_UP));
            iuq.setFinalPrice(finalPrice.setScale(2, BigDecimal.ROUND_HALF_UP));

            if (results[0].getSubscriptionId() != null) {
                iuq.setSubscriptionId(results[0].getSubscriptionId().toString());

                List<com.abaenglish.external.zuora.soap.domain.subscription.ZuoraSubscriptionResponse> subscriptionInfoResponse = zuoraExternalServiceSoap.getSubscriptionInfoById(results[0].getSubscriptionId().toString(), ZuoraExternalServiceSoap.ZStatus.ACTIVE);

                if (!subscriptionInfoResponse.isEmpty()) {

                    List<SubscriptionAbawebappsInfo> subscriptionAbawebappsRequest = mapper.mapAsList(subscriptionInfoResponse, SubscriptionAbawebappsInfo.class);
                    iuq.setSubscriptionsList(subscriptionAbawebappsRequest);
                }
            }

            if (results[0].getSubscriptionNumber() != null) {
                iuq.setSubscriptionNumber(results[0].getSubscriptionNumber());
                responseService.setConfirmationNumber(results[0].getSubscriptionNumber());
            }

            List<String> ratePlanId = subscription.getRatePlanId();

            if (ratePlanId != null) {
                if (!ratePlanId.isEmpty()) {
                    iuq.setIdRatePlanCharge(ratePlanId.get(0));
                }
                if (ratePlanId.size() > 1) {
                    iuq.setIdDiscount(ratePlanId.get(1));
                }
            }

            try {
                InsertUserApiResponse insertUserResponse = userService.insertUser(iuq);

                responseService.setPaymentId(insertUserResponse.getPaymentId());
                responseService.setPartnerId(insertUserResponse.getPartnerId());

            } catch (Exception e) {
                logger.error("Bad request insert user in ABAWEBAPPS. Exception: " + e);
                throw new BadRequestApiException(ErrorMessages.BAD_REQUEST_SUBSCRIPTION.getError());
            }

        } else {
            for (Integer count = 0; count < results[0].getErrors().length; count++) {
                logger.error(results[0].getErrors()[count].getCode().getValue(), results[0].getErrors()[count]);
            }
            throw new BadRequestApiException(ErrorMessages.INTERNAL_ZUORA_ERROR.getError());
        }

        responseService.setSubscribeResults(results);
        responseService.setUserZuora(userZuora);

        return responseService;
    }

    /**
     * @deprecated remove this methdd when client user transactions
     *
     * @param subscription
     * @param userZuora
     * @return
     * @throws ServiceException
     * @deprecated remove this methdd when client user transactions
     */
    @Deprecated
    private CreateSubscriptionRequest prepareRequestZuora(SubscriptionServiceRequest subscription, UserZuoraApiResponse userZuora) throws ServiceException{
        CreateSubscriptionRequest request = new CreateSubscriptionRequest();
        request.setUserId(subscription.getUserId());
        request.setName(userZuora.getUser().getName());
        request.setSurname(userZuora.getUser().getSurname());
        request.setCountry(userZuora.getUser().getCountry());
        request.setCurrency(userZuora.getUser().getCurrency());
        request.setEmail(userZuora.getUser().getEmail());
        request.setType(subscription.getType().name());
        request.setPaymentId(subscription.getPaymentId());
        request.setRatePlanId(subscription.getRatePlanId());
        request.setPeriod(subscription.getPeriod());
        request.setGatewayName(subscription.getGatewayName());

        // paypal attributes
        request.setPaypalEmail(subscription.getPaypalEmail());

        // obtain baid from paypal
        if(subscription.getType().equals(SubscriptionType.PayPal)) {
            //search paypal customer details
            final PaypalExternalCustomerDetails paypalCustomerDetails = paypalService.getExpressCheckoutDetails(subscription.getPaypalToken());
            PaypalCustomerDetails customerDetails = mapper.map(paypalCustomerDetails, PaypalCustomerDetails.class);
            request.setPaypalEmail(customerDetails.getPayerEmail());
            request.setPaypalBaid(paypalService.createBillingAgreement(subscription.getPaypalToken()));
        }

        if (userZuora.getZuora() != null) {
            request.setZuoraAccountId(userZuora.getZuora().getZuoraAccountId());
            request.setZuoraAccountNumber(userZuora.getZuora().getZuoraAccountNumber());
        }

        return request;
    }


    CreateSubscriptionRequest prepareRequestZuora(PaymentTransaction transaction, UserZuoraApiResponse userZuora) throws ServiceException {
        final CreateSubscriptionRequest.Builder request = CreateSubscriptionRequest.Builder.aCreateSubscriptionRequest()
                .userId(transaction.getUserId())
                .name(userZuora.getUser().getName())
                .surname(userZuora.getUser().getSurname())
                .country(userZuora.getUser().getCountry())
                .currency(userZuora.getUser().getCurrency())
                .email(userZuora.getUser().getEmail())
                .type(transaction.getType())
                .paymentId(transaction.getPayload().getPaymentId())
                .ratePlanId(transaction.getPayload().getRatePlans())
                .period(transaction.getPayload().getPeriod())
                .gatewayName(transaction.getGatewayName());

        // obtain baid from paypal
        if(transaction.getType().equals(SubscriptionType.PayPal.name())) {
            //search paypal customer details
            final PaypalExternalCustomerDetails paypalCustomerDetails = paypalService.getExpressCheckoutDetails(transaction.getPaypalToken());
            PaypalCustomerDetails customerDetails = mapper.map(paypalCustomerDetails, PaypalCustomerDetails.class);
            request.paypalEmail(customerDetails.getPayerEmail());
            request.paypalBaid(paypalService.createBillingAgreement(transaction.getPaypalToken()));
        }

        if (userZuora.getZuora() != null) {
            request.zuoraAccountId(userZuora.getZuora().getZuoraAccountId())
                    .zuoraAccountNumber(userZuora.getZuora().getZuoraAccountNumber());
        }

        return request.build();
    }

    @Override
    public String createToken(PaypalExternalTokenRequest request) throws ServiceException {
        return paypalService.createToken(request);
    }

    /**
     * @see ISubscriptionService#cancelSubscriptionZuora(String, Date, Date, String)
     */
    @Override
    public void cancelSubscriptionZuora(final String subscriptionId, final Date contractEffectiveDate, final Date effectiveDate, final String name) throws ServiceException {

        // search active subscription if exists
        List<ZuoraSubscriptionResponse> subscriptions = zuoraExternalServiceSoap.getSubscriptionInfoById(subscriptionId, ZuoraExternalServiceSoap.ZStatus.ACTIVE);

        if (subscriptions != null && !subscriptions.isEmpty()) {

            // check there is only one subscription active for id provided
            if (subscriptions.size() > 1) {
                logger.error("Too much subscription found for id '{}'", subscriptionId);
                throw new IllegalArgumentServiceException(ErrorMessages.MANY_SUBSCRIPTIONS);
            }
            logger.debug("Active subscription found for id '{}'", subscriptionId);
            try {
                ZuoraServiceStub.AmendResult[] amendResults = zuoraExternalServiceSoap.cancelSubscription(
                        CancelSubscriptionRequest.Builder.aCancelSubscriptionRequest()
                                .subscriptionId(subscriptions.get(0).getId())
                                .contractEffectiveDate(contractEffectiveDate)
                                .effectiveDate(effectiveDate)
                                .name(name)
                                .generateInvoice(subscriptionProperties.getCancellation().getZuoraGenerateInvoice())
                                .processPayments(subscriptionProperties.getCancellation().getZuoraProcessPayments())
                                .build());

                // if amendment was created successful, then cancel in abawabapps
                if (amendResults[0].getSuccess()) {
                    logger.info("Subscription with id '{}' successfully cancelled in zuora, amendments created: {}", subscriptionId, Arrays.toString(amendResults[0].getAmendmentIds()));

                } else if (amendResults[0].getErrors().length > 0) {
                    // map error details to propagate messages
                    throw new ExpandedServiceException(
                            String.join("|", Stream.of(amendResults[0].getErrors()).map(i -> i.getField() + "-" + i.getCode() + "-" + i.getMessage()).collect(Collectors.toSet())),
                            ErrorMessages.CANCEL_SUBSCRIPTION_ERROR);

                } else {
                    throw new ServiceException(ErrorMessages.CANCEL_SUBSCRIPTION_ERROR.getError());
                }
            } catch (UnexpectedErrorFault | RemoteException e) {
                logger.error("There was an error cancelling subscription", e);
                throw new IllegalArgumentServiceException(ErrorMessages.CANCEL_SUBSCRIPTION_ERROR, e);
            }

        } else {
            logger.error("Active subscription not found for id '{}'", subscriptionId);
            throw new NotFoundServiceException(ErrorMessages.SUBSCRIPTION_NOT_FOUND);
        }
    }

    /**
     * @see ISubscriptionService#registerRenewalPaymentOnSubscription(String)
     */
    @Override
    public List<ResultOperationServiceDto> registerRenewalPaymentOnSubscription(final String paymentId) throws ServiceException {

        final InvoicePaymentServiceDto invoicePaymentServiceDto = paymentService.findInvoicePaymentByPaymentId(paymentId, true);

        // search active subscription related on the invoices obtained
        final ZuoraSubscriptionResponse subscription = findActiveSubscriptionById(invoicePaymentServiceDto.getInvoiceSubscriptionId());

        /*
            renewal amend must be related with previous subscription version, if it is not, then is not a renewal payment
         */
        if(StringUtils.isNotEmpty(subscription.getPreviousSubscriptionId())) {
            final RenewPaymentRequest renewPaymentRequest = RenewPaymentRequest.Builder.aRenewPaymentRequest()
                    .originalSubscriptionId(subscription.getOriginalId())
                    .nextRenewalDate(subscription.getTermEndDate())
                    .invoiceId(invoicePaymentServiceDto.getInvoiceId())
                    .invoiceNumber(invoicePaymentServiceDto.getInvoiceNumber())
                    .paymentDate(invoicePaymentServiceDto.getPaymentDate())
                    .build();

        /*
            check that the last amend related with last version is a renewal
        */
            logger.info("Current subscription is '{}', search renewal amend related with previous subscription '{}'", subscription.getId(), subscription.getPreviousSubscriptionId());
            final ZuoraAmendmentResponse amend = findAmendCompleted(Optional.of(subscription.getPreviousSubscriptionId()), Optional.empty(), Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal));
            logger.info("Renewal amend '{}' found for paymentId '{}'", amend.getCode(), paymentId);

            // send renewal billing information to abawebapps

            try {
                final List<SubscriptionInfoResponse> abaResponse = abawebappsExternalService.renewPaymentsZuora(Arrays.asList(renewPaymentRequest));
                return abaResponse.stream()
                        .map(r -> ResultOperationServiceDto.Builder.aResultOperationServiceDto()
                                .subscriptionId(r.getOriginalSubscriptionId())
                                .success(r.getSuccess())
                                .warnings(r.getWarnings())
                                .amendmendId(amend.getId())
                                .build())
                        .collect(Collectors.toList());
            } catch (NotFoundExternalException e) {
                throw new ServiceException("Cannot send billing information about the renewal to Aba.", e);
            }
        } else {
            throw new ConflictServiceException(ErrorMessages.PAYMENT_PROCESSED_WRONG_ORIGIN);
        }
    }

    /**
     * @see ISubscriptionService#findAmendCompleted(Optional, Optional, Optional)
     */
    @Override
    public ZuoraAmendmentResponse findAmendCompleted(final Optional<String> subscriptionId, final Optional<String> amendmentId, final Optional<ZuoraExternalServiceSoap.ZAmendmentTypes> type) throws  ServiceException {
        final List<ZuoraAmendmentResponse> amends = zuoraExternalServiceSoap.getAmendmentByIdFilters(subscriptionId, amendmentId, type);

        if(amends.isEmpty()) { // validate there are results
            throw new NotFoundServiceException(buildAmendErrorMessage(subscriptionId, amendmentId, type, "has no '%s' amend related"));

        } else if(amends.size() > 1){ // validate there only one result
            throw new ServiceException("Invalid amend results.", new IllegalStateException(buildAmendErrorMessage(subscriptionId, amendmentId, type, "has to many '%s' amends related.")));

        } else if(!"Completed".equalsIgnoreCase(amends.get(0).getStatus())){ // validate that the amends returned are completed
            throw new ServiceException("Invalid amend results.", new IllegalStateException(buildAmendErrorMessage(subscriptionId, amendmentId, type, "has '%s' amend related but not completed.")));
        }
        return amends.get(0);
    }

    private String buildAmendErrorMessage(final Optional<String> subscriptionId, final Optional<String> amendmentId, final Optional<ZuoraExternalServiceSoap.ZAmendmentTypes> type, final String error) {
        final StringBuilder sb = new StringBuilder();
        subscriptionId.ifPresent( s -> sb.append("The subscription id '").append(s).append("', ") );
        amendmentId.ifPresent( a -> sb.append("The amend id '").append(a).append("', ") );
        sb.append(String.format(error, type.map(ZuoraExternalServiceSoap.ZAmendmentTypes::name).orElse("")));
        return sb.toString();
    }

    private ZuoraSubscriptionResponse findActiveSubscriptionById(final String subscriptionId) throws ServiceException {
        // search subscriptions related on the invoices obtained
        final List<ZuoraSubscriptionResponse> subscriptions = zuoraExternalServiceSoap.getSubscriptionsInfoById(Arrays.asList(subscriptionId), Optional.of("Active"));
        if (subscriptions.size() > 1) {
            throw new ServiceException("Cannot register renewal payment on subscription.",
                    new IllegalStateException(String.format("Too many subscriptions found %s for id '%s'", subscriptions.size(), subscriptionId)));
        } else if(subscriptions.isEmpty()) {
            throw new NotFoundServiceException("Active subscription with id '%s' not found in Zuora", subscriptionId);
        }
        logger.info("Subscription found  by id '{}'", subscriptionId);
        return subscriptions.get(0);
    }

    @Override
    public List<ResultOperationServiceDto> registerRefundPaymentOnSubscription(final String refundId) throws ServiceException {

        Assert.notNull(refundId, "refundId identifier is mandatory");

        final ZuoraRefundResponse zuoraRefundResponse = paymentService.findRefundById(refundId, Optional.of(ZuoraExternalServiceSoap.RefundStatus.PROCESSED));

        RefundPaymentsRequest refundPaymentsRequests = mapper.map(zuoraRefundResponse, RefundPaymentsRequest.class);

        final List<SubscriptionInfoResponse> abaResponse = refundPaymentsAbbawebapps(Arrays.asList(refundPaymentsRequests));
        return abaResponse.stream()
                .map( r -> ResultOperationServiceDto.Builder.aResultOperationServiceDto()
                        .subscriptionId(r.getOriginalSubscriptionId())
                        .success(r.getSuccess())
                        .warnings(r.getWarnings())
                        .build())
                .collect(Collectors.toList());
    }

    /**
     * @see ISubscriptionService#registerRenewalSubscription(String, String)
     */
    @Override
    public List<ResultOperationServiceDto> registerRenewalSubscription(final String amendmentId, final String subscriptionOriginalId) throws ServiceException{

        //load amend details
        ZuoraAmendmentResponse amend = findAmendCompleted(Optional.empty(), Optional.of(amendmentId), Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal));

        //load current active subscription by original Id provided
        final List<ZuoraSubscriptionResponse> currentActiveSubscription = zuoraExternalServiceSoap.getSubscriptionsInfoByOriginalId(Arrays.asList(subscriptionOriginalId), Optional.of(SubscriptionStatus.ACTIVE));
        if(currentActiveSubscription.isEmpty()) {
            throw new NotFoundServiceException("Current active subscription not found for original id '%s' and amendment id '%s'", subscriptionOriginalId, amendmentId);
        } else if(currentActiveSubscription.size() > 1) {
            throw new ServiceException("Cannot register renewal subscription.",
                    new IllegalStateException(String.format("Too many subscriptions found %s for original id '%s'", currentActiveSubscription.size(), subscriptionOriginalId)));
        }

        //check that the previous version of subscription is the same as the amend
        if(!amend.getSubscriptionId().equalsIgnoreCase(currentActiveSubscription.get(0).getPreviousSubscriptionId())) {
            throw new ServiceException("Cannot register renewal subscription.",
                    new IllegalStateException(String.format("Previous subscription id '%s' is not the same as the given amend '%s'", currentActiveSubscription.get(0).getPreviousSubscriptionId(), amend.getSubscriptionId())));
        }

        // send to abawebapps
        try {
            final List<SubscriptionInfoResponse> abaResponse = abawebappsExternalService.renewSubscriptionsZuora(Arrays.asList(
                    RenewSubscriptionsRequest.Builder.aRenewSubscriptionsRequest()
                            .userExpirationDate(currentActiveSubscription.get(0).getTermEndDate())
                            .originalSubscriptionId(subscriptionOriginalId)
                            .build()));
            return abaResponse.stream()
                    .map(r -> ResultOperationServiceDto.Builder.aResultOperationServiceDto()
                            .subscriptionId(r.getOriginalSubscriptionId())
                            .success(r.getSuccess())
                            .warnings(r.getWarnings())
                            .amendmendId(amend.getId())
                            .build())
                    .collect(Collectors.toList());

        } catch (NotFoundExternalException e) {
            throw new ServiceException("Cannot send renewal subscription information to Aba.", e);
        }
    }

    /**
     * @see ISubscriptionService#registerCancellationSubscription(String, String)
     */
    @Override
    public List<ResultOperationServiceDto> registerCancellationSubscription(final String amendmentId, final String subscriptionOriginalId) throws ServiceException {

        //load amend details
        ZuoraAmendmentResponse amend = findAmendCompleted(Optional.empty(), Optional.of(amendmentId), Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Cancellation));

        //load current active subscription by original Id provided
        final List<ZuoraSubscriptionResponse> currentSubscription = zuoraExternalServiceSoap.getSubscriptionsInfoByOriginalId(Arrays.asList(subscriptionOriginalId), Optional.of(SubscriptionStatus.CANCELLED));
        if(currentSubscription.isEmpty()) {
            throw new NotFoundServiceException("Current subscription not found for original id '%s' and amendment id '%s'", subscriptionOriginalId, amendmentId);
        } else if(currentSubscription.size() > 1) {
            throw new ServiceException("Cannot register cancellation subscription.",
                    new IllegalStateException(String.format("Too many subscriptions found %s for original id '%s'", currentSubscription.size(), subscriptionOriginalId)));
        }

        // send to abawebapps
        try {
            final List<SubscriptionInfoResponse> abaResponse = abawebappsExternalService.cancelSubscriptionsZuora(Arrays.asList(
                    CancelSubscriptionsRequest.Builder.aCancelSubscriptionsRequest()
                            .originalSubscriptionId(subscriptionOriginalId)
                            .accountId(currentSubscription.get(0).getAccountId())
                            .cancelledDate(currentSubscription.get(0).getCancelledDate())
                            .build()));
            return abaResponse.stream()
                    .map(r -> ResultOperationServiceDto.Builder.aResultOperationServiceDto()
                            .subscriptionId(r.getOriginalSubscriptionId())
                            .success(r.getSuccess())
                            .warnings(r.getWarnings())
                            .amendmendId(amend.getId())
                            .build())
                    .collect(Collectors.toList());

        } catch (NotFoundExternalException e) {
            throw new ServiceException("Cannot send cancellation subscription information to Aba.", e);
        }
    }
}




