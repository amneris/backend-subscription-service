package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.controller.v1.dto.notification.NotificationEventCategories;
import com.abaenglish.subscription.service.ISubscriptionService;
import com.abaenglish.subscription.service.dto.notification.NotificationServiceDto;
import com.abaenglish.subscription.service.dto.notification.PaymentProcessedNotificationServiceDto;
import com.abaenglish.subscription.service.dto.subscription.ResultOperationServiceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PaymentProcessedNotificationProcessor extends NotificationProcessor<PaymentProcessedNotificationServiceDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentProcessedNotificationProcessor.class);

    @Autowired
    private ISubscriptionService subscriptionService;

    public PaymentProcessedNotificationProcessor() {
        super(NotificationEventCategories.PAYMENT_PROCESSED);
    }

    /**
     * @see NotificationProcessor#process(NotificationServiceDto)
     */
    @Override
    public PaymentProcessedNotificationServiceDto process(PaymentProcessedNotificationServiceDto dto) throws ServiceException{
        LOGGER.info("Processing notification '{}' with payment id '{}'",eventCategory.name(),  dto.getPaymentId());
        List<ResultOperationServiceDto> resultOperationServiceDtos =  subscriptionService.registerRenewalPaymentOnSubscription(dto.getPaymentId());
        if(resultOperationServiceDtos != null && !resultOperationServiceDtos.isEmpty()) {
            // expect only one result
            dto.setSuccess(resultOperationServiceDtos.get(0).getSuccess());
            dto.setErrors(resultOperationServiceDtos.get(0).getWarnings());
        }
        return dto;
    }
}
