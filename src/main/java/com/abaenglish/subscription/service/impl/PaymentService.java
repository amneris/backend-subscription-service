package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.zuora.soap.domain.subscription.*;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.exception.service.NotFoundServiceException;
import com.abaenglish.subscription.service.IPaymentService;
import com.abaenglish.subscription.service.dto.InvoicePaymentServiceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentService implements IPaymentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);

    @Autowired
    private ZuoraExternalServiceSoap zuoraExternalServiceSoap;

    @Override
    public InvoicePaymentServiceDto findInvoicePaymentByPaymentId(final String paymentId, final boolean detailsInvoice) throws ServiceException {
         /*
            search payment by id, and if it's not foun then stop the process
         */
        final Optional<Payment> payment = zuoraExternalServiceSoap.getPaymentProcessedById(paymentId);
        if(!payment.isPresent()) {
            throw new NotFoundServiceException("Payment with id '%s' not found in Zuora", paymentId);
        }
        LOGGER.info("Payment found for id '{}': [{}]", paymentId, payment);
        final InvoicePaymentServiceDto response = InvoicePaymentServiceDto.Builder.anInvoicePaymentServiceDto()
                .paymentDate(payment.get().getEffectiveDate())
                .build();

        /*
            search invoices related with the payment id provided
            validate that only one invoice payment found
         */
        final InvoiceItemsInfo invoiceItemsInfo = findInvoiceItemsByPaymentId(paymentId);
        LOGGER.info("Subscription related with payment {} found [{}]", paymentId, invoiceItemsInfo.getSubscriptionId());
        response.setInvoiceSubscriptionId(invoiceItemsInfo.getSubscriptionId());
        response.setInvoiceId(invoiceItemsInfo.getInvoiceId());

        /*
            Load extra information from the invoice if it is necessary
         */
        if(detailsInvoice) {
            // search from invoice number
            final InvoiceInfo invoicesInfo = zuoraExternalServiceSoap.getInvoiceInfo(invoiceItemsInfo.getInvoiceId());
            if (invoicesInfo == null) {
                throw new NotFoundServiceException("InvoiceInfo with id '%s' not found in Zuora for payment '%s'", invoiceItemsInfo.getInvoiceId(), paymentId);
            }
            response.setInvoiceNumber(invoicesInfo.getInvoiceNumber());
            LOGGER.info("InvoiceInfo found for invoiceId '{}': [{}]", response.getInvoiceId(), invoicesInfo);
        }
        return response;
    }

    private InvoiceItemsInfo findInvoiceItemsByPaymentId(final String paymentId) throws ServiceException {
        final List<InvoicePayment> invoicePayments = zuoraExternalServiceSoap.getInvoicePayment(paymentId);
        LOGGER.info("{} InvoicePayment found for paymentId '{}'", invoicePayments.size(), paymentId);
        if(invoicePayments.size() > 1) {
            throw new ServiceException("Invalid invoice results.",
                    new IllegalStateException(String.format("Too many InvoicePayment found %s for payment id '%s'", invoicePayments.size(), paymentId)));
        } else if (invoicePayments.isEmpty()){
            throw new NotFoundServiceException(String.format("InvoicePayment with payment id '%s' not found in Zuora", paymentId));
        }

        final List<InvoiceItemsInfo> invoiceItemsInfo = zuoraExternalServiceSoap.getInvoiceItemsInfo(invoicePayments.get(0).getInvoiceId());
        if(invoiceItemsInfo.isEmpty()) {
            throw new NotFoundServiceException("InvoiceItemsInfo with invoiceId '%s' not found in Zuora", invoicePayments.get(0).getInvoiceId());
        }
        LOGGER.info("{} InvoiceItemsInfo found for invoiceId '{}'", invoiceItemsInfo.size(), invoicePayments.get(0).getInvoiceId());
        return invoiceItemsInfo.get(0);
    }

    @Override
    public ZuoraRefundResponse findRefundById(final String refundId, final Optional<ZuoraExternalServiceSoap.RefundStatus> status) throws ServiceException {

        final ZuoraRefundResponse zuoraRefundResponse = zuoraExternalServiceSoap.getRefundInfoById(refundId, status);

        if (zuoraRefundResponse == null ) {
            throw new NotFoundServiceException("Refund with id '%s' not found in Zuora", refundId);
        }
        LOGGER.info("Refund found  by id '{}'", refundId);
        return  zuoraRefundResponse;
    }
}
