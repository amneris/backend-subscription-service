package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.controller.v1.dto.notification.NotificationEventCategories;
import com.abaenglish.subscription.service.ISubscriptionService;
import com.abaenglish.subscription.service.dto.notification.RefundNotificationServiceDto;
import com.abaenglish.subscription.service.dto.subscription.ResultOperationServiceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RefundNotificationProcessor extends NotificationProcessor<RefundNotificationServiceDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RefundNotificationProcessor.class);

    @Autowired
    private ISubscriptionService subscriptionService;

    public RefundNotificationProcessor() {
        super(NotificationEventCategories.REFUND_PROCESSED);
    }

    @Override
    public RefundNotificationServiceDto process(RefundNotificationServiceDto dto) throws ServiceException {
        LOGGER.info("Processing notification '{}' with refund id '{}'",eventCategory.name(),  dto.getRefundId());

        List<ResultOperationServiceDto> resultOperationServiceDtos = subscriptionService.registerRefundPaymentOnSubscription( dto.getRefundId());

        if(resultOperationServiceDtos != null && !resultOperationServiceDtos.isEmpty()) {
            dto.setSuccess(resultOperationServiceDtos.get(0).getSuccess());
            dto.setErrors(resultOperationServiceDtos.get(0).getWarnings());
        }

        return dto;
    }
}
