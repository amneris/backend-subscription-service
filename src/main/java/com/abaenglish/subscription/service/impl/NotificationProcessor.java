package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.controller.v1.dto.notification.NotificationEventCategories;
import com.abaenglish.subscription.service.dto.notification.NotificationServiceDto;

/**
 * Abstract class to inherit when you want to process a notification.
 * All implementations refer to a zuora notification through the type of <T extends NotificationServiceDto>
 * Each notification are defined by de {@link NotificationEventCategories}
 *
 * @param <T>
 */
public abstract class NotificationProcessor<T extends NotificationServiceDto> {

    protected T type;
    protected NotificationEventCategories eventCategory;

    public NotificationProcessor(NotificationEventCategories eventCategory) {
        this.eventCategory = eventCategory;
    }

    /**
     * Process the notification received
     *
     * @param dto
     * @return
     * @throws ServiceException
     */
    public abstract T process(T dto) throws ServiceException;

}
