package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.service.INotificationFactory;
import com.abaenglish.subscription.service.dto.notification.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NotificationFactory implements INotificationFactory {

    @Autowired
    PaymentDeclinedNotificationProcessor paymentDeclinedNotificationProcessor;

    @Autowired
    PaymentProcessedNotificationProcessor paymentProcessedNotificationProcessor;

    @Autowired
    RefundNotificationProcessor refundNotificationProcessor;

    @Autowired
    AmendProcessedNotificationProcessor amendProcessedNotificationProcessor;

    /**
     * Depending on the getEventCategory() select the corresponding processor
     *
     * @param dto
     * @return
     * @throws ServiceException
     */
    @Override
    public NotificationServiceDto process(NotificationServiceDto dto) throws ServiceException {
        switch (dto.getEventCategory()) {
            case PAYMENT_FAILED:
                return paymentDeclinedNotificationProcessor.process((PaymentDeclinedNotificationServiceDto) dto);

            case PAYMENT_PROCESSED:
                return paymentProcessedNotificationProcessor.process((PaymentProcessedNotificationServiceDto) dto);

            case REFUND_PROCESSED:
                return refundNotificationProcessor.process((RefundNotificationServiceDto) dto);

            case AMENDMENT_PROCESSED:
                return amendProcessedNotificationProcessor.process((AmendProcessedNotificationServiceDto) dto);

            default:
                throw new ServiceException(String.format("Cannot process event '%s'", dto.getEventCategory().name()), new IllegalStateException());
        }
    }
}
