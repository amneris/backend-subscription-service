package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceItemsInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoicePayment;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.subscription.controller.v1.dto.notification.NotificationEventCategories;
import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.exception.service.NotFoundServiceException;
import com.abaenglish.subscription.service.ISubscriptionService;
import com.abaenglish.subscription.service.dto.notification.NotificationServiceDto;
import com.abaenglish.subscription.service.dto.notification.PaymentDeclinedNotificationServiceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PaymentDeclinedNotificationProcessor extends NotificationProcessor<PaymentDeclinedNotificationServiceDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentDeclinedNotificationProcessor.class);
    public static final String CANCEL_SUBSCRIPTION_NAME = "Cancel subscription from payment declined";

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private ISubscriptionService subscriptionService;

    public PaymentDeclinedNotificationProcessor() {
        super(NotificationEventCategories.PAYMENT_FAILED);
    }

    /**
     * @see NotificationProcessor#process(NotificationServiceDto)
     */
    @Override
    public PaymentDeclinedNotificationServiceDto process(PaymentDeclinedNotificationServiceDto dto) throws ServiceException {
        LOGGER.info("Processing notification '{}' with payment id '{}'",eventCategory.name(),  dto.getPaymentId());
        boolean processed = processPaymentDeclined(dto.getPaymentId());
        dto.setSuccess(processed);
        return dto;
    }

    /**
     * Given a payment id from zuora, cancel the subscription related on Zuora
     *
     * @param paymentId
     * @return
     * @throws ServiceException
     */
    boolean processPaymentDeclined(final String paymentId) throws ServiceException {

        Assert.notNull(paymentId, "Payment id required");

        final InvoicePayment invoicePayment = getZuoraInvoiceFromPayment(paymentId);
        /*
            search invoice items to obtain subscription id
         */
        final List<InvoiceItemsInfo> invoiceItemsInfos = zuoraExternalService.getInvoiceItemsInfo(invoicePayment.getInvoiceId());
        final Set<String> subscriptionsIds = invoiceItemsInfos.stream()
                .map(InvoiceItemsInfo::getSubscriptionId)
                .collect(Collectors.toSet());
        LOGGER.info("'{}' subscriptions found related with invoice id '{}'", subscriptionsIds, invoicePayment.getInvoiceId());

        if(subscriptionsIds.isEmpty()) {
            throw new NotFoundServiceException(ErrorMessages.SUBSCRIPTION_NOT_FOUND);
        }

        /*
            Find invoice related to obtain due date
         */
        final InvoiceInfo invoiceInfo = zuoraExternalService.getInvoiceInfo(invoicePayment.getInvoiceId());

        /*
            Cancel all subscriptions related found
         */
        final Date cancelEffectiveDate = invoiceInfo.getDueDate();
        for (String subscriptionId : subscriptionsIds) {
            subscriptionService.cancelSubscriptionZuora(subscriptionId, new Date(), cancelEffectiveDate, CANCEL_SUBSCRIPTION_NAME);
            LOGGER.info("'{}' subscription cancelled with effective date {}", subscriptionId, cancelEffectiveDate);
        }
        return true;
    }

    /**
     * Given a payment zuora identifier, search and return the {@link InvoicePayment} related.
     * If there is more than one invoice related, throws a {@link ServiceException} with error {@link ErrorMessages#ZUORA_TOO_MUCH_INVOICES}
     *
     * @param paymentId
     *          valid payment zuora identifier
     * @return
     * @throws ServiceException
     */
    InvoicePayment getZuoraInvoiceFromPayment(final String paymentId) throws ServiceException {
        LOGGER.debug("Search invoice related with payment '{}'", paymentId);
        final List<InvoicePayment> invoicePayments = zuoraExternalService.getInvoicePayment(paymentId);
        if (invoicePayments.isEmpty() || invoicePayments.size() > 1) {
            throw new ServiceException(ErrorMessages.ZUORA_TOO_MUCH_INVOICES.getError());
        }

        LOGGER.debug("Invoice '{}' found related with payment '{}'", invoicePayments.get(0).getInvoiceId(), paymentId);
        return invoicePayments.get(0);
    }
}
