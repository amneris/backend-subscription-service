package com.abaenglish.subscription.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.subscription.controller.v1.dto.notification.NotificationEventCategories;
import com.abaenglish.subscription.domain.ZuoraNotification;
import com.abaenglish.subscription.domain.ZuoraNotificationStatus;
import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.exception.service.ConflictServiceException;
import com.abaenglish.subscription.exception.service.IllegalArgumentServiceException;
import com.abaenglish.subscription.exception.service.NotFoundServiceException;
import com.abaenglish.subscription.repository.ZuoraNotificationRepository;
import com.abaenglish.subscription.repository.predicate.ZuoraNotificationPredicate;
import com.abaenglish.subscription.service.INotificationFactory;
import com.abaenglish.subscription.service.INotificationService;
import com.abaenglish.subscription.service.dto.notification.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class NotificationService implements INotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

    @Autowired
    private ZuoraNotificationRepository zuoraNotificationRepository;

    @Autowired
    private INotificationFactory notificationFactory;

    /**
     * @see INotificationService#processZuoraNotification(ZuoraNotificationServiceRequest)
     */
    @Override
    public Optional<NotificationServiceDto> processZuoraNotification(final ZuoraNotificationServiceRequest serviceNotificationRequest) throws ServiceException {

        // process event if it's necessary
        Optional<NotificationServiceDto> notificationServiceDto = registerZuoraNotification(serviceNotificationRequest);

        if(notificationServiceDto.isPresent()){
            notificationFactory.process(notificationServiceDto.get());
        } else {
            LOGGER.warn("Cannot process event category '{}'", serviceNotificationRequest.getEventCategory());
            throw new IllegalArgumentServiceException(ErrorMessages.INVALID_NOTIFICATION_EVENT_CATEGORY);
        }
        return notificationServiceDto;
    }

    /**
     * Creates on the repository a new {@link ZuoraNotification} based on {@link ZuoraNotificationServiceRequest} given.
     * Check if this notification was processed before, then throws {@link ConflictServiceException}
     *
     * @param notification
     *          {@link ZuoraNotificationServiceRequest} received from zuora
     * @param zuoraReferenceId
     *          zuora identifier that depends on event category
     * @return
     *      notification created
     *
     * @throws ConflictServiceException
     *          throwed when the notification was processed before
     */
    ZuoraNotification createZuoraNotification(ZuoraNotificationServiceRequest notification, final String zuoraReferenceId) throws ServiceException {

        /*
            Check that not exist previous notification processed
         */
        ZuoraNotification zuoraNotification = zuoraNotificationRepository.findOne(ZuoraNotificationPredicate.existsNotification(notification.getEventCategory(), zuoraReferenceId));

        if(zuoraNotification != null && zuoraNotification.getId() != null) {
            LOGGER.warn("Notification already processed: {}", zuoraNotification.toString());
            throw new ConflictServiceException(ErrorMessages.NOTIFICATION_ALREADY_PROCESSED);
        }
        zuoraNotification = ZuoraNotification.Builder.aZuoraNotification()
                .eventCategory(notification.getEventCategory())
                .payload(notification)
                .zuoraReferenceId(zuoraReferenceId)
                .status(ZuoraNotificationStatus.RECEIVED)
                .build();
        LOGGER.debug("Save notification on the repository: ", zuoraNotification.toString());
        return zuoraNotificationRepository.save(zuoraNotification);
    }

    /**
     * @see NotificationService#updateZuoraNotificationStatus(Long, ZuoraNotificationStatus, Optional)
     */
    @Override
    public Optional<ZuoraNotification> updateZuoraNotificationStatus(final Long notificationId, final ZuoraNotificationStatus status, final Optional<String> details) throws NotFoundServiceException {
        Assert.notNull(notificationId, "Notification identifier is mandatory");
        Assert.notNull(status, "Notification status is mandatory");

        Optional<ZuoraNotification> notification = zuoraNotificationRepository.findOne(notificationId);
        if(notification.isPresent()) {
            if(details.isPresent()) {
                notification.get().setError(details.get());
            }
            notification.get().setStatus(status);
            notification.get().setUpdateDate(LocalDateTime.now());
            LOGGER.debug("Update notification on the repository: ", notification.toString());
            zuoraNotificationRepository.save(notification.get());
        } else {
            throw new NotFoundServiceException(ErrorMessages.NOTIFICATION_NOT_FOUND);
        }
        return notification;
    }

    /**
     * Given a {@link com.abaenglish.subscription.controller.v1.dto.notification.ZuoraNotificationRequest} check if the eventCategory related is valid.
     * If it is valid, then register the notification
     *
     * @param serviceNotificationRequest
     *          Raw notification from zuora
     * @return
     *          DTO built by eventCategory, if not dto is returned, then is not a notification that could be processed
     * @throws ServiceException
     */
    private Optional<NotificationServiceDto> registerZuoraNotification(final ZuoraNotificationServiceRequest serviceNotificationRequest) throws ServiceException {

        final Optional<NotificationEventCategories> eventCategory = NotificationEventCategories.parseEventCategory(serviceNotificationRequest.getEventCategory());
        Optional<NotificationServiceDto> notificationServiceDto = Optional.empty();
        if (eventCategory.isPresent()) {
            // create notification in database
            ZuoraNotification zuoraNotificationPersisted;

            switch (eventCategory.get()) {
                case PAYMENT_FAILED:
                    zuoraNotificationPersisted = createZuoraNotification(serviceNotificationRequest, serviceNotificationRequest.getPaymentId());
                    notificationServiceDto = Optional.of(new PaymentDeclinedNotificationServiceDto.Builder()
                            .notificationId(zuoraNotificationPersisted.getId())
                            .paymentId(serviceNotificationRequest.getPaymentId())
                            .eventCategory(eventCategory.get())
                            .build());
                    break;
                case PAYMENT_PROCESSED:
                    zuoraNotificationPersisted = createZuoraNotification(serviceNotificationRequest, serviceNotificationRequest.getPaymentId());
                    notificationServiceDto = Optional.of(new PaymentProcessedNotificationServiceDto.Builder()
                            .notificationId(zuoraNotificationPersisted.getId())
                            .paymentId(serviceNotificationRequest.getPaymentId())
                            .eventCategory(eventCategory.get())
                            .build());
                    break;
                case REFUND_PROCESSED:
                    zuoraNotificationPersisted = createZuoraNotification(serviceNotificationRequest, serviceNotificationRequest.getRefundId());
                    notificationServiceDto = Optional.of(new RefundNotificationServiceDto.Builder()
                            .notificationId(zuoraNotificationPersisted.getId())
                            .refundId(serviceNotificationRequest.getRefundId())
                            .eventCategory(eventCategory.get())
                            .build());
                    break;
                case AMENDMENT_PROCESSED:

                    zuoraNotificationPersisted = createZuoraNotification(serviceNotificationRequest, serviceNotificationRequest.getSubscriptionVersionAmendmentId());

                    final AmendProcessedNotificationServiceDto.Builder builder = new AmendProcessedNotificationServiceDto.Builder()
                            .notificationId(zuoraNotificationPersisted.getId())
                            .amendId(serviceNotificationRequest.getSubscriptionVersionAmendmentId())
                            .subscriptionOriginalId(serviceNotificationRequest.getSubscriptionOriginalId())
                            .eventCategory(eventCategory.get());

                    final Optional<AmendmentType> amendmentType = AmendmentType.parseAmendmentType(serviceNotificationRequest.getSubscriptionVersionAmendmentType());
                    amendmentType.ifPresent( t -> builder.amendType(t) ); // set if exists

                    notificationServiceDto = Optional.of(builder.build());

                    break;
            }
        }
        // throws an error if no
        return notificationServiceDto;
    }
}
