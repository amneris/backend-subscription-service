package com.abaenglish.subscription.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.zuora.soap.domain.subscription.ZuoraRefundResponse;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.service.dto.InvoicePaymentServiceDto;

import java.util.Optional;

public interface IPaymentService {
    InvoicePaymentServiceDto findInvoicePaymentByPaymentId(final String paymentId, final boolean detailsInfo) throws ServiceException;
    ZuoraRefundResponse findRefundById(final String refundId, final Optional<ZuoraExternalServiceSoap.RefundStatus> status) throws ServiceException;
}
