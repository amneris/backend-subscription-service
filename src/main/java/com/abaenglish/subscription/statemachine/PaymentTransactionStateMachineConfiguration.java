package com.abaenglish.subscription.statemachine;

import com.abaenglish.subscription.service.impl.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler;
import org.springframework.statemachine.state.State;

@Configuration
public class PaymentTransactionStateMachineConfiguration {

    @Configuration
    @EnableStateMachine
    public static class PaymetTransactionStateMachine extends StateMachineConfigurerAdapter<String, String> {
        private static final Logger LOGGER = LoggerFactory.getLogger(PaymetTransactionStateMachine.class);


        @Autowired
        private TransactionService transactionService;

        @Override
        public void configure(StateMachineConfigurationConfigurer<String, String> config)
                throws Exception {
            config
                    .withConfiguration()
                    .listener(listener());
        }

        @Override
        public void configure(StateMachineStateConfigurer<String, String> states) throws Exception {
            states
                    .withStates()
                    .initial(States.OPEN.name())
                    .state(States.PROCESS_ZUORA.name(), actionProcessZuora(), null) // synchronous entry action
                    .state(States.PROCESS_ABA.name(), actionProcessABA(), null) // synchronous entry action
                    .end(States.CLOSED.name())
                    .end(States.ERROR.name())
            ;
        }

        @Override
        public void configure(StateMachineTransitionConfigurer<String, String> transitions) throws Exception {
            transitions
                    .withExternal()
                    .source(States.OPEN.name()).target(States.PROCESS_ZUORA.name()).event(Events.START_PROCESS.name())
                    .and()
                    .withExternal()
                    .source(States.PROCESS_ZUORA.name()).target(States.PROCESS_ABA.name()).event(Events.CONFIRMED_ZUORA.name())
                    .and()
                    .withExternal()
                    .source(States.PROCESS_ABA.name()).target(States.CLOSED.name()).event(Events.CONFIRMED_ABA.name())
                    .and()
                    // manage error status
                    .withExternal().source(States.PROCESS_ZUORA.name()).target(States.ERROR.name()).event(Events.ERROR.name()).and()
                    .withExternal().source(States.PROCESS_ABA.name()).target(States.ERROR.name()).event(Events.ERROR.name())
            ;
        }

        @Bean
        public StateMachineListener<String, String> listener() {
            return new StateMachineListenerAdapter() {
                @Override
                public void stateChanged(State from, State to) {
                    super.stateChanged(from, to);
                    LOGGER.info("State change from {} to {}", from.getId(), to.getId());
                }

                @Override
                public void stateEntered(State state) {
                    super.stateEntered(state);
                    LOGGER.info("State entered to {}", state.getId());
                }

                @Override
                public void stateExited(State state) {
                    super.stateExited(state);
                    LOGGER.info("State exited to {}", state.getId());
                }

                @Override
                public void stateMachineError(StateMachine stateMachine, Exception exception) {
                    super.stateMachineError(stateMachine, exception);
                    LOGGER.error("id[{}], stateMachineError", stateMachine.getId(), exception);
                }
            };
        }

        private void setErrorState(final String transactionId, final Throwable e) {
            try {
                transactionService.change(transactionId, Events.ERROR.name());
                LOGGER.error(String.format("Something went wrong executing this action on state machine, public id '%s'", transactionId), e);
            } catch (Exception ex) {
                LOGGER.error(String.format("Cannot set error state on transaction '%s'",transactionId), ex);
                throw new RuntimeException(ex);
            }
        }

        @Bean
        public Action<String, String> actionProcessZuora() {
            return new Action<String, String>() {
                @Override
                public void execute(StateContext<String, String> context) {
                        final String publicId = getPublicId(context.getMessage());
                    try {
                        LOGGER.info("actionProcessZuora with public id: {}", publicId);
                        transactionService.processZuora(publicId);
                    } catch (Exception e) {
                        setErrorState(publicId, e);
                    }
                }
            };
        }

        @Bean
        public Action<String, String> actionProcessABA() {
            return new Action<String, String>() {
                @Override
                public void execute(StateContext<String, String> context) {
                        final String publicId = getPublicId(context.getMessage());
                    try {
                        LOGGER.info("actionProcessABA with public id: {}", publicId);
                        transactionService.processABA(publicId);
                    } catch (Exception e) {
                        setErrorState(publicId, e);
                    }
                }
            };
        }

        private String getPublicId(final Message message) {
            return message.getHeaders().get(TransactionService.MESSAGE_HEADER_PUBLIC_ID_HEADER, String.class);
        }

    }

    @Configuration
    static class PersistHandlerConfig {
        @Autowired
        private StateMachine<String, String> stateMachine;

        @Bean
        public PersistStateMachineHandler persistStateMachineHandler() {
            return new PersistStateMachineHandler(stateMachine);
        }
    }

}
