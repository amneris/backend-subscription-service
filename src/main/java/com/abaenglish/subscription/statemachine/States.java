package com.abaenglish.subscription.statemachine;

public enum States {
    OPEN, CLOSED, PROCESS_ZUORA, PROCESS_ABA, ERROR;
}
