package com.abaenglish.subscription.statemachine;

public enum Events {
    START_PROCESS, CONFIRMED_ZUORA, CONFIRMED_ABA, ERROR;
}
