package com.abaenglish.subscription.repository.converter;

import com.abaenglish.subscription.service.dto.transaction.PayloadServiceRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import java.io.IOException;

//@Converter(autoApply = true)
public class JpaPayloadConverter implements AttributeConverter<PayloadServiceRequest, String> {

    // ObjectMapper is thread safe
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convertToDatabaseColumn(PayloadServiceRequest attribute) {
        String jsonString = "";
        try {
            log.debug("Start convertToDatabaseColumn");

            // convert list of POJO to json
            jsonString = objectMapper.writeValueAsString(attribute);
            log.debug("convertToDatabaseColumn" + jsonString);

        } catch (JsonProcessingException ex) {
            log.error("Something went wrong: " + ex.getMessage() , ex);
        }
        return jsonString;
    }

    @Override
    public PayloadServiceRequest convertToEntityAttribute(String dbData) {

        PayloadServiceRequest payloadServiceRequest = new PayloadServiceRequest();
        try {
            log.debug("Start convertToEntityAttribute");

            // convert json to list of POJO
            payloadServiceRequest = objectMapper.readValue(dbData, PayloadServiceRequest.class);
            log.debug("JsonDocumentsConverter.convertToDatabaseColumn" + payloadServiceRequest);

        } catch (IOException ex) {
            log.error("Something went wrong: " + ex.getMessage() , ex);
        }
        return payloadServiceRequest;

    }
}
