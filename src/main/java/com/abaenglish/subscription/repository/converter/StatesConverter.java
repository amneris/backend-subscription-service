package com.abaenglish.subscription.repository.converter;

import com.abaenglish.subscription.statemachine.States;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatesConverter implements AttributeConverter<States, String> {

    @Override
    public String convertToDatabaseColumn(States attribute) {
        return attribute.name();
    }

    @Override
    public States convertToEntityAttribute(String dbData) {
        return States.valueOf(dbData);
    }
}
