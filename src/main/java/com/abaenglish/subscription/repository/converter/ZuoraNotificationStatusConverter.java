package com.abaenglish.subscription.repository.converter;

import com.abaenglish.subscription.domain.ZuoraNotificationStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ZuoraNotificationStatusConverter implements AttributeConverter<ZuoraNotificationStatus, String> {

    @Override
    public String convertToDatabaseColumn(ZuoraNotificationStatus attribute) {
        return attribute.name();
    }

    @Override
    public ZuoraNotificationStatus convertToEntityAttribute(String dbData) {
        return ZuoraNotificationStatus.valueOf(dbData);
    }
}
