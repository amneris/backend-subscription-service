package com.abaenglish.subscription.repository.converter;

import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import java.io.IOException;

public class ZuoraNotificationRequestConverter implements AttributeConverter<ZuoraNotificationServiceRequest, String> {

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger LOGGER = LoggerFactory.getLogger(ZuoraNotificationRequestConverter.class);

    @Override
    public String convertToDatabaseColumn(ZuoraNotificationServiceRequest attribute) {
        String jsonString = "";
        try {
            LOGGER.debug("Start convertToDatabaseColumn");

            // convert list of POJO to json
            jsonString = objectMapper.writeValueAsString(attribute);
            LOGGER.debug("convertToDatabaseColumn {}", jsonString);

        } catch (JsonProcessingException ex) {
            LOGGER.error("Something went wrong", ex);
        }
        return jsonString;
    }

    @Override
    public ZuoraNotificationServiceRequest convertToEntityAttribute(String dbData) {
        ZuoraNotificationServiceRequest object = new ZuoraNotificationServiceRequest();
        try {
            LOGGER.debug("Start convertToEntityAttribute");

            // convert json to list of POJO
            object = objectMapper.readValue(dbData, ZuoraNotificationServiceRequest.class);
            LOGGER.debug("JsonDocumentsConverter.convertToDatabaseColumn {}", object);

        } catch (IOException ex) {
            LOGGER.error("Something went wrong", ex);
        }
        return object;
    }
}
