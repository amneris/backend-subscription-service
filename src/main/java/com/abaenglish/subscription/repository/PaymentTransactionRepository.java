package com.abaenglish.subscription.repository;

import com.abaenglish.boot.data.repository.BaseRepository;
import com.abaenglish.subscription.domain.PaymentTransaction;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface PaymentTransactionRepository extends BaseRepository<PaymentTransaction, Long>, QueryDslPredicateExecutor<PaymentTransaction> {
}
