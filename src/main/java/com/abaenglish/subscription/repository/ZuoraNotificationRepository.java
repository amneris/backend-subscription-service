package com.abaenglish.subscription.repository;

import com.abaenglish.boot.data.repository.BaseRepository;
import com.abaenglish.subscription.domain.ZuoraNotification;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface ZuoraNotificationRepository extends BaseRepository<ZuoraNotification, Long>, QueryDslPredicateExecutor<ZuoraNotification> {
}
