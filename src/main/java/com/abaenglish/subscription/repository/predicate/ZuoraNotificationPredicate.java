package com.abaenglish.subscription.repository.predicate;

import com.abaenglish.subscription.domain.QZuoraNotification;
import com.querydsl.core.types.Predicate;

public class ZuoraNotificationPredicate {
    private ZuoraNotificationPredicate() {
        throw new IllegalAccessError("Utility class");
    }

    /**
     * Return true when the notification is processed yet
     *  - {@link com.abaenglish.subscription.domain.ZuoraNotification#eventCategory} is equals to given eventCategory
     *  - {@link com.abaenglish.subscription.domain.ZuoraNotification#zuoraReferenceId} is equals to given zuoraReferenceId
     *
     * @param eventCategory
     * @param zuoraReferenceId
     * @return
     */
    public static Predicate existsNotification(final String eventCategory, final String zuoraReferenceId) {
        return QZuoraNotification.zuoraNotification.eventCategory.eq(eventCategory)
                .and(QZuoraNotification.zuoraNotification.zuoraReferenceId.eq(zuoraReferenceId));
    }
}
