package com.abaenglish.subscription.repository.predicate;

import com.abaenglish.subscription.domain.QPaymentTransaction;
import com.querydsl.core.types.Predicate;

public class PaymentTransactionPredicate {
    private PaymentTransactionPredicate() {
        throw new IllegalAccessError("Utility class");
    }

    public static Predicate findByPublicId(String publicId) { return QPaymentTransaction.paymentTransaction.publicId.eq(publicId); }
}
