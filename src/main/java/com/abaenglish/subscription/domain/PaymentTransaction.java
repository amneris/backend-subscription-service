package com.abaenglish.subscription.domain;

import com.abaenglish.boot.domain.DomainObject;
import com.abaenglish.subscription.repository.converter.JpaPayloadConverter;
import com.abaenglish.subscription.service.dto.transaction.PayloadServiceRequest;
import com.abaenglish.subscription.statemachine.States;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "payment_transaction")
public class PaymentTransaction extends DomainObject {

    private static final long serialVersionUID = -4928097352855915663L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "state", insertable = true, updatable = true, nullable = false)
    private States state;
    @Column(name = "public_id", insertable = true, updatable = true, nullable = false)
    private String publicId;
    @Column(name = "user_id", insertable = true, updatable = true, nullable = false)
    private Long userId;
    @Column(name = "type", insertable = true, updatable = true, nullable = false)
    private String type;
    @Column(name = "period", insertable = true, updatable = true, nullable = true)
    private Integer period;

    @Column(name = "subscription_id_zuora", insertable = true, updatable = true, nullable = true)
    private String subscriptionIdZuora;
    @Column(name = "subscription_number_zuora", insertable = true, updatable = true, nullable = true)
    private String subscriptionNumberZuora;

    @Column(name = "account_id_zuora", insertable = true, updatable = true, nullable = true)
    private String accountIdZuora;
    @Column(name = "account_number_zuora", insertable = true, updatable = true, nullable = true)
    private String accountNumberZuora;

    @Column(name = "invoice_id_zuora", insertable = true, updatable = true, nullable = true)
    private String invoiceIdZuora;

    @Column(name = "amount_original", insertable = true, updatable = true, nullable = true)
    private BigDecimal price;

    @Column(name = "creation_date", insertable = true, updatable = true, nullable = false)
    private LocalDateTime creationDate;
    @Convert(converter = JpaPayloadConverter.class)
    @Column(name = "payload", insertable = true, updatable = true, nullable = true)
    private PayloadServiceRequest payload;

    @Column(name = "paypal_token", insertable = true, updatable = true, nullable = true)
    private String paypalToken;

    @Column(name = "gateway_name", insertable = true, updatable = true, nullable = true)
    private String gatewayName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public States getState() {
        return state;
    }

    public void setState(States state) {
        this.state = state;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getSubscriptionIdZuora() {
        return subscriptionIdZuora;
    }

    public void setSubscriptionIdZuora(String subscriptionIdZuora) {
        this.subscriptionIdZuora = subscriptionIdZuora;
    }

    public String getSubscriptionNumberZuora() {
        return subscriptionNumberZuora;
    }

    public void setSubscriptionNumberZuora(String subscriptionNumberZuora) {
        this.subscriptionNumberZuora = subscriptionNumberZuora;
    }

    public String getAccountIdZuora() {
        return accountIdZuora;
    }

    public void setAccountIdZuora(String accountIdZuora) {
        this.accountIdZuora = accountIdZuora;
    }

    public String getAccountNumberZuora() {
        return accountNumberZuora;
    }

    public void setAccountNumberZuora(String accountNumberZuora) {
        this.accountNumberZuora = accountNumberZuora;
    }

    public String getInvoiceIdZuora() {
        return invoiceIdZuora;
    }

    public void setInvoiceIdZuora(String invoiceIdZuora) {
        this.invoiceIdZuora = invoiceIdZuora;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public PayloadServiceRequest getPayload() {
        return payload;
    }

    public void setPayload(PayloadServiceRequest payloadServiceRequest) {
        this.payload = payloadServiceRequest;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getPaypalToken() {
        return paypalToken;
    }

    public void setPaypalToken(String paypalToken) {
        this.paypalToken = paypalToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentTransaction that = (PaymentTransaction) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static final class Builder {
        private Long id;
        private States state;
        private String publicId;
        private Long userId;
        private String type;
        private Integer period;
        private String subscriptionIdZuora;
        private String subscriptionNumberZuora;
        private String accountIdZuora;
        private String accountNumberZuora;
        private String invoiceIdZuora;
        private BigDecimal price;
        private LocalDateTime creationDate;
        private PayloadServiceRequest payload;
        private String paypalToken;
        private String gatewayName;

        private Builder() {
        }

        public static Builder aPaymentTransaction() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder state(States state) {
            this.state = state;
            return this;
        }

        public Builder publicId(String publicId) {
            this.publicId = publicId;
            return this;
        }

        public Builder publicId() {
            this.publicId = UUID.randomUUID().toString();
            return this;
        }

        public Builder paypalToken(String paypalToken) {
            this.paypalToken = paypalToken;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder period(Integer period) {
            this.period = period;
            return this;
        }

        public Builder subscriptionIdZuora(String subscriptionIdZuora) {
            this.subscriptionIdZuora = subscriptionIdZuora;
            return this;
        }

        public Builder subscriptionNumberZuora(String subscriptionNumberZuora) {
            this.subscriptionNumberZuora = subscriptionNumberZuora;
            return this;
        }

        public Builder accountIdZuora(String accountIdZuora) {
            this.accountIdZuora = accountIdZuora;
            return this;
        }

        public Builder accountNumberZuora(String accountNumberZuora) {
            this.accountNumberZuora = accountNumberZuora;
            return this;
        }

        public Builder invoiceIdZuora(String invoiceIdZuora) {
            this.invoiceIdZuora = invoiceIdZuora;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder creationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public Builder payload(PayloadServiceRequest payloadServiceRequest) {
            this.payload = payloadServiceRequest;
            return this;
        }

        public Builder gatewayName(String gatewayName) {
            this.gatewayName = gatewayName;
            return this;
        }

        public PaymentTransaction build() {
            PaymentTransaction paymentTransaction = new PaymentTransaction();
            paymentTransaction.setId(id);
            paymentTransaction.setState(state);
            paymentTransaction.setPublicId(publicId);
            paymentTransaction.setUserId(userId);
            paymentTransaction.setType(type);
            paymentTransaction.setPeriod(period);
            paymentTransaction.setSubscriptionIdZuora(subscriptionIdZuora);
            paymentTransaction.setSubscriptionNumberZuora(subscriptionNumberZuora);
            paymentTransaction.setAccountIdZuora(accountIdZuora);
            paymentTransaction.setAccountNumberZuora(accountNumberZuora);
            paymentTransaction.setInvoiceIdZuora(invoiceIdZuora);
            paymentTransaction.setPrice(price);
            paymentTransaction.setCreationDate(creationDate);
            paymentTransaction.setPayload(payload);
            paymentTransaction.setPaypalToken(paypalToken);
            paymentTransaction.setGatewayName(gatewayName);
            return paymentTransaction;
        }
    }

    @Override
    public String toString() {
        return "PaymentTransaction{" +
                "id=" + id +
                ", state=" + state +
                ", publicId='" + publicId + '\'' +
                ", userId=" + userId +
                ", type='" + type + '\'' +
                ", period=" + period +
                ", subscriptionIdZuora='" + subscriptionIdZuora + '\'' +
                ", subscriptionNumberZuora='" + subscriptionNumberZuora + '\'' +
                ", accountIdZuora='" + accountIdZuora + '\'' +
                ", accountNumberZuora='" + accountNumberZuora + '\'' +
                ", invoiceIdZuora='" + invoiceIdZuora + '\'' +
                ", price=" + price +
                ", creationDate=" + creationDate +
                ", payload=" + payload +
                ", gatewayName='" + gatewayName + '\'' +
                ", paypalToken='" + paypalToken + '\'' +
                '}';
    }
}
