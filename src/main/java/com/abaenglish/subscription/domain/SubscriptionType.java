package com.abaenglish.subscription.domain;

public enum SubscriptionType {
    ACH, BankTransfer, Cash, Check, CreditCard, CreditCardReferenceTransaction, DebitCard, Other, PayPal, WireTransfer;
    public static final String _ACH = "ACH";
    public static final String _BankTransfer = "BankTransfer";
    public static final String _Cash = "Cash";
    public static final String _Check = "Check";
    public static final String _CreditCard = "CreditCard";
    public static final String _CreditCardReferenceTransaction = "CreditCardReferenceTransaction";
    public static final String _DebitCard = "DebitCard";
    public static final String _Other = "Other";
    public static final String _PayPal = "PayPal";
    public static final String _WireTransfer = "WireTransfer";
}
