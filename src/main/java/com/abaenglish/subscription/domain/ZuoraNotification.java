package com.abaenglish.subscription.domain;

import com.abaenglish.boot.domain.DomainObject;
import com.abaenglish.subscription.repository.converter.ZuoraNotificationRequestConverter;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "zuora_notification", uniqueConstraints = @UniqueConstraint(columnNames = {"event_category", "zuora_reference_id"}))
public class ZuoraNotification  extends DomainObject {

    private static final long serialVersionUID = 3514691061471856376L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "event_category", updatable = false, nullable = false)
    private String eventCategory;

    @Column(name = "status", nullable = false)
    private ZuoraNotificationStatus status;

    @Convert(converter = ZuoraNotificationRequestConverter.class)
    @Column(name = "payload", updatable = false, nullable = false, length = 1000)
    private ZuoraNotificationServiceRequest payload;

    @Column(name = "error")
    private String error;

    @Column(name = "creation_date", updatable = false, nullable = false)
    private LocalDateTime creationDate;

    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "zuora_reference_id", nullable = false, length = 100, updatable = false)
    private String zuoraReferenceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public ZuoraNotificationStatus getStatus() {
        return status;
    }

    public void setStatus(ZuoraNotificationStatus status) {
        this.status = status;
    }

    public ZuoraNotificationServiceRequest getPayload() {
        return payload;
    }

    public void setPayload(ZuoraNotificationServiceRequest payload) {
        this.payload = payload;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public String getZuoraReferenceId() {
        return zuoraReferenceId;
    }

    public void setZuoraReferenceId(String zuoraReferenceId) {
        this.zuoraReferenceId = zuoraReferenceId;
    }

    public static final class Builder {
        private Long id;
        private String eventCategory;
        private ZuoraNotificationStatus status;
        private ZuoraNotificationServiceRequest payload;
        private String error;
        private LocalDateTime creationDate;
        private LocalDateTime updateDate;
        private String zuoraReferenceId;

        private Builder() {
        }

        public static Builder aZuoraNotification() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder eventCategory(String eventCategory) {
            this.eventCategory = eventCategory;
            return this;
        }

        public Builder status(ZuoraNotificationStatus status) {
            this.status = status;
            return this;
        }

        public Builder payload(ZuoraNotificationServiceRequest payload) {
            this.payload = payload;
            return this;
        }

        public Builder error(String error) {
            this.error = error;
            return this;
        }

        public Builder zuoraReferenceId(String zuoraReferenceId) {
            this.zuoraReferenceId = zuoraReferenceId;
            return this;
        }

        public Builder creationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public Builder updateDate(LocalDateTime updateDate) {
            this.updateDate = updateDate;
            return this;
        }

        public ZuoraNotification build() {
            ZuoraNotification zuoraNotification = new ZuoraNotification();
            zuoraNotification.setId(id);
            zuoraNotification.setEventCategory(eventCategory);
            zuoraNotification.setStatus(status);
            zuoraNotification.setPayload(payload);
            zuoraNotification.setError(error);
            zuoraNotification.setCreationDate(creationDate == null ? LocalDateTime.now() : creationDate);
            zuoraNotification.setUpdateDate(updateDate == null ? LocalDateTime.now() : updateDate);
            zuoraNotification.setZuoraReferenceId(zuoraReferenceId);
            return zuoraNotification;
        }
    }

    @Override
    public String toString() {
        return "ZuoraNotification{" +
                "id=" + id +
                ", eventCategory='" + eventCategory + '\'' +
                ", status=" + status +
                ", payload=" + payload +
                ", error='" + error + '\'' +
                ", creationDate=" + creationDate +
                ", updateDate=" + updateDate +
                ", zuoraReferenceId='" + zuoraReferenceId + '\'' +
                "} " + super.toString();
    }
}
