package com.abaenglish.subscription.domain;

public enum ZuoraNotificationStatus {

    RECEIVED, PROCESSED, ERROR
}
