package com.abaenglish.subscription.controller.v1.dto.subscription;

import java.math.BigDecimal;

public class PaypalTokenRequest {

    private String urlSuccess;
    private String urlCancel;

    public String getUrlSuccess() {
        return urlSuccess;
    }

    public void setUrlSuccess(String urlSuccess) {
        this.urlSuccess = urlSuccess;
    }

    public String getUrlCancel() {
        return urlCancel;
    }

    public void setUrlCancel(String urlCancel) {
        this.urlCancel = urlCancel;
    }

}
