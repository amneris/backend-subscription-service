package com.abaenglish.subscription.controller.v1.dto.notification.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.subscription.controller.v1.dto.notification.ZuoraNotificationRequest;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import ma.glasnost.orika.MappingContext;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.stream.Stream;

@Component
public class ZuoraNotificationRequestToZuoraNotificationServiceRequestMapper extends BaseCustomMapper<ZuoraNotificationRequest, ZuoraNotificationServiceRequest> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZuoraNotificationRequestToZuoraNotificationServiceRequestMapper.class);

    public ZuoraNotificationRequestToZuoraNotificationServiceRequestMapper() {
        super();
    }

    @Override
    public void mapAtoB(ZuoraNotificationRequest a, ZuoraNotificationServiceRequest b, MappingContext context) {
        a.getParameters().stream()
                .forEach( param -> setValue(b, param.getName(), param.getValue()) );
    }

    /**
     * Given a key field given, map given value to field attribute into object
     *
     * @param obj
     *          object to set value given
     * @param key
     *          field to search in object given
     * @param value
     *          value to set into the object given
     */
    private void setValue(ZuoraNotificationServiceRequest obj, String key, String value) {
        try {
            final Optional<Field> field = Stream.of(obj.getClass().getDeclaredFields())
                    .filter( f -> f.getName().equalsIgnoreCase(key))
                    .findFirst();
            if(field.isPresent()) {
                Field f = field.get();
                f.setAccessible(true);
                f.set(obj, value);
            }
        } catch (Exception e) { LOGGER.info("This field cannot be parsed '{}', by {}", key, ExceptionUtils.getRootCauseMessage(e)); }
    }
}
