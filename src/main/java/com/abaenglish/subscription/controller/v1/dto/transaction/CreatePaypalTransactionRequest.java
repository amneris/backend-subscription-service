package com.abaenglish.subscription.controller.v1.dto.transaction;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class CreatePaypalTransactionRequest extends CreateDefaultTransactionRequest {

    @ApiModelProperty(value = "Paypal token given previously", required = true)
    @NotEmpty
    private String paypalToken;

    public String getPaypalToken() {
        return paypalToken;
    }

    public void setPaypalToken(String paypalToken) {
        this.paypalToken = paypalToken;
    }

    public static class Builder extends CreateDefaultTransactionRequest.Builder<CreatePaypalTransactionRequest, CreatePaypalTransactionRequest.Builder> {

        public Builder paypalToken(String token ) {
            type.setPaypalToken(token);
            return b;
        }
        protected CreatePaypalTransactionRequest createObj() { return new CreatePaypalTransactionRequest(); }
        protected Builder getThis() { return this; }
    }
}
