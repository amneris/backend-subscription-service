package com.abaenglish.subscription.controller.v1.dto.subscription;

import com.abaenglish.external.zuora.soap.domain.subscription.ZuoraSubscriptionResponse;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionsList {

    private List<ZuoraSubscriptionResponse> subscriptions = new ArrayList<>();

    public SubscriptionsList(List<ZuoraSubscriptionResponse> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<ZuoraSubscriptionResponse> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<ZuoraSubscriptionResponse> subscriptions) {
        this.subscriptions = subscriptions;
    }
}
