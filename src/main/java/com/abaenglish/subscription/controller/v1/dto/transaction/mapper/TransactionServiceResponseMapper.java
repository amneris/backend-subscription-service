package com.abaenglish.subscription.controller.v1.dto.transaction.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.subscription.controller.v1.dto.transaction.TransactionResponse;
import com.abaenglish.subscription.service.dto.transaction.TransactionServiceResponse;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class TransactionServiceResponseMapper extends BaseCustomMapper<TransactionServiceResponse, TransactionResponse> {
    public TransactionServiceResponseMapper() {
        super();
        addField("publicId", "id");
        addField("subscriptionNumberZuora", "subscriptionId");
        addField("finalPrice", "invoice.finalPrice");
    }

    @Override
    public void mapAtoB(TransactionServiceResponse transactionServiceResponseA, TransactionResponse transactionResponseB, MappingContext context) {
        super.mapAtoB(transactionServiceResponseA, transactionResponseB, context);

        if(transactionServiceResponseA.getInvoiceItems() != null && !transactionServiceResponseA.getInvoiceItems().isEmpty()) {

            transactionServiceResponseA.getInvoiceItems().stream()
                    .forEach( item -> {
                        if(item.getTotal().compareTo(BigDecimal.ZERO) >= 0) {
                            transactionResponseB.getInvoice().setBasePrice(
                                    transactionResponseB.getInvoice().getBasePrice() == null ?
                                            item.getTotal() : transactionResponseB.getInvoice().getBasePrice().add(item.getTotal()));
                        } else {
                            transactionResponseB.getInvoice().setDiscountPrice(
                                    transactionResponseB.getInvoice().getDiscountPrice() == null ?
                                            item.getTotal() : transactionResponseB.getInvoice().getDiscountPrice().add(item.getTotal()));
                        }
                    } );
        }
    }
}
