package com.abaenglish.subscription.controller.v1.dto.subscription.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionResponse;
import com.abaenglish.subscription.service.dto.subscription.CreateSubscriptionServiceResponse;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class CreateSubscriptionResponseMapper extends BaseCustomMapper<CreateSubscriptionServiceResponse, CreateSubscriptionResponse> {
    public CreateSubscriptionResponseMapper() {
        super();
        addField("userZuora.user.name", "name");
        addField("userZuora.user.surname", "surname");
        addField("userZuora.user.email", "email");
        addField("userZuora.user.currency", "currency");
        addField("invoiceAmount", "price");
        addField("confirmationNumber", "confirmationNumber");
        addField("paymentId", "paymentId");
        addField("partnerId", "partnerId");
    }

    @Override
    public void mapAtoB(CreateSubscriptionServiceResponse createSubscriptionServiceResponse, CreateSubscriptionResponse createSubscriptionResponse, MappingContext context) {
        super.mapAtoB(createSubscriptionServiceResponse, createSubscriptionResponse, context);
        createSubscriptionResponse.setAccountNumber(createSubscriptionServiceResponse.getSubscribeResults()[0].getAccountNumber() == null ? createSubscriptionServiceResponse.getUserZuora().getZuora().getZuoraAccountNumber() : createSubscriptionServiceResponse.getSubscribeResults()[0].getAccountNumber());
    }
}
