package com.abaenglish.subscription.controller.v1.dto.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel(value="ZuoraNotificationRequest", description="Request of notifications from zuora")
public class ZuoraNotificationRequest implements Serializable {

    private static final long serialVersionUID = 2922125148714308043L;

    @JacksonXmlProperty(localName = "parameter")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<ZuoraNotificationParameterRequest> parameters;

    public List<ZuoraNotificationParameterRequest> getParameters() {
        return parameters;
    }

    public void setParameters(List<ZuoraNotificationParameterRequest> parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return "ZuoraNotificationRequest{" +
                "parameters=" + parameters +
                '}';
    }

    public static final class Builder {
        private List<ZuoraNotificationParameterRequest> parameters;

        private Builder() {
        }

        public static Builder aZuoraNotificationRequest() {
            return new Builder();
        }

        public Builder parameters(List<ZuoraNotificationParameterRequest> parameters) {
            this.parameters = parameters;
            return this;
        }

        public ZuoraNotificationRequest build() {
            ZuoraNotificationRequest zuoraNotificationRequest = new ZuoraNotificationRequest();
            zuoraNotificationRequest.setParameters(parameters);
            return zuoraNotificationRequest;
        }
    }
}
