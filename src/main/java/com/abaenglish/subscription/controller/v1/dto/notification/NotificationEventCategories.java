package com.abaenglish.subscription.controller.v1.dto.notification;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Wrapper of Zuora event categories that our processes work
 */
public enum NotificationEventCategories {

    PAYMENT_FAILED("PaymentDeclined"),PAYMENT_PROCESSED("PaymentProcessed"),REFUND_PROCESSED("PaymentRefundProcessed"),AMENDMENT_PROCESSED("AmendmentProcessed");

    private String eventCategory;

    /*
        Avoid clone list from values().
        Use valuesAsSet() instead of values()
     */
    private static final Set<NotificationEventCategories> VALUE_LIST = Stream.of(values()).collect(Collectors.toSet());

    NotificationEventCategories(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public static Optional<NotificationEventCategories> parseEventCategory(final String event) {
        return VALUE_LIST.stream()
                .filter( e -> e.eventCategory.equalsIgnoreCase(event) )
                .findFirst();
    }
}
