package com.abaenglish.subscription.controller.v1.dto.notification;

import com.abaenglish.subscription.exception.SubscriptionRuntimeException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@JacksonXmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ZuoraNotificationResponse implements Serializable {

    private static final long serialVersionUID = -4700812184080717244L;

    @JsonIgnore
    protected Long notificationId;
    @JsonIgnore
    protected Boolean success;
    @JsonIgnore
    protected String errors;

    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    @JacksonXmlProperty
    public String getResume() throws IllegalAccessException {
        return Stream.of(this.getClass().getDeclaredFields())
                .map(this::mapField)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(", "));
    }

    private String mapField(final Field f) {
        try {
            if(!"serialVersionUID".equalsIgnoreCase(f.getName())
                    && f.get(this) != null && f.get(this) != "") {
                f.setAccessible(true);
                return f.getName() + ": " + f.get(this);
            }
        } catch (IllegalAccessException e) {
            throw new SubscriptionRuntimeException(e);
        }
        return null;
    }

    public static final class Builder {
        protected Long notificationId;
        protected Boolean success;
        protected String errors;

        private Builder() {
        }

        public static Builder aZuoraNotificationResponse() {
            return new Builder();
        }

        public Builder notificationId(Long notificationId) {
            this.notificationId = notificationId;
            return this;
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public Builder errors(String errors) {
            this.errors = errors;
            return this;
        }

        public ZuoraNotificationResponse build() {
            ZuoraNotificationResponse zuoraNotificationResponse = new ZuoraNotificationResponse();
            zuoraNotificationResponse.setNotificationId(notificationId);
            zuoraNotificationResponse.setSuccess(success);
            zuoraNotificationResponse.setErrors(errors);
            return zuoraNotificationResponse;
        }
    }
}
