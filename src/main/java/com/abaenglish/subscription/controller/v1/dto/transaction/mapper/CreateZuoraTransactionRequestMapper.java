package com.abaenglish.subscription.controller.v1.dto.transaction.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.subscription.controller.v1.dto.transaction.CreateZuoraTransactionRequest;
import com.abaenglish.subscription.service.dto.transaction.PayloadServiceRequest;
import org.springframework.stereotype.Component;

@Component
public class CreateZuoraTransactionRequestMapper extends BaseCustomMapper<CreateZuoraTransactionRequest, PayloadServiceRequest> {
    public CreateZuoraTransactionRequestMapper() {
        super();
        addField("zuoraRefId", "paymentId");
    }
}
