package com.abaenglish.subscription.controller.v1.dto.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="ZuoraNotificationParameterRequest", description="Request of parameter received in the notification from zuora")
public class ZuoraNotificationParameterRequest implements Serializable {

    private static final long serialVersionUID = -7803047213432004180L;

    @ApiModelProperty(value = "Parameter value")
    @JacksonXmlText
    private String value;

    @ApiModelProperty(value = "Attribute value")
    @JacksonXmlProperty(isAttribute = true)
    private String name;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static final class Builder {
        private String value;
        private String name;

        private Builder() {
        }

        public static Builder aZuoraNotificationParameterRequest() {
            return new Builder();
        }

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public ZuoraNotificationParameterRequest build() {
            ZuoraNotificationParameterRequest zuoraNotificationParameterRequest = new ZuoraNotificationParameterRequest();
            zuoraNotificationParameterRequest.setValue(value);
            zuoraNotificationParameterRequest.setName(name);
            return zuoraNotificationParameterRequest;
        }
    }

    @Override
    public String toString() {
        return "ZuoraNotificationParameterRequest{" +
                "value='" + value + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
