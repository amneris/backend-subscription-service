package com.abaenglish.subscription.controller.v1.dto.subscription;

import java.math.BigDecimal;

public class CreateSubscriptionResponse {

    private String name;
    private String surname;
    private String email;
    private String accountNumber;
    private BigDecimal price;
    private Integer period;
    private String currency;
    private String confirmationNumber;
    private String paymentId;
    private String partnerId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber) {
        this.confirmationNumber = confirmationNumber;
    }

    public String getPaymentId() { return paymentId; }

    public String getPartnerId() { return partnerId; }

    public void setPaymentId(String paymentId) { this.paymentId = paymentId; }

    public void setPartnerId(String partnerId) { this.partnerId = partnerId; }

}
