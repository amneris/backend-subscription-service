package com.abaenglish.subscription.controller.v1.dto.subscription;

import com.abaenglish.subscription.domain.SubscriptionType;

import java.util.List;

public class CreateSubscriptionRequest {

    private Long userId;
    private SubscriptionType type;
    private String paymentId;
    private List<String> ratePlanId;
    private Integer period;
    private String gatewayName;
    private String paypalToken;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public SubscriptionType getType() {
        return type;
    }

    public void setType(SubscriptionType type) {
        this.type = type;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) { this.paymentId = paymentId; }

    public List<String> getRatePlanId() {
        return ratePlanId;
    }

    public void setRatePlanId(List<String> ratePlanId) {
        this.ratePlanId = ratePlanId;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getPaypalToken() {
        return paypalToken;
    }

    public void setPaypalToken(String paypalToken) {
        this.paypalToken = paypalToken;
    }

    public static final class Builder {
        private Long userId;
        private SubscriptionType type;
        private String paymentId;
        private List<String> ratePlanId;
        private Integer period;
        private String gatewayName;
        private String paypalToken;

        private Builder() {
        }

        public static Builder aCreateSubscriptionRequest() {
            return new Builder();
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder type(SubscriptionType type) {
            this.type = type;
            return this;
        }

        public Builder paymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder ratePlanId(List<String> ratePlanId) {
            this.ratePlanId = ratePlanId;
            return this;
        }

        public Builder period(Integer period) {
            this.period = period;
            return this;
        }

        public Builder gatewayName(String gatewayName) {
            this.gatewayName = gatewayName;
            return this;
        }

        public Builder paypalToken(String paypalToken) {
            this.paypalToken= paypalToken;
            return this;
        }

        public CreateSubscriptionRequest build() {
            CreateSubscriptionRequest createSubscriptionRequest = new CreateSubscriptionRequest();
            createSubscriptionRequest.setUserId(userId);
            createSubscriptionRequest.setType(type);
            createSubscriptionRequest.setPaymentId(paymentId);
            createSubscriptionRequest.setRatePlanId(ratePlanId);
            createSubscriptionRequest.setPeriod(period);
            createSubscriptionRequest.setGatewayName(gatewayName);
            createSubscriptionRequest.setPaypalToken(paypalToken);
            return createSubscriptionRequest;
        }
    }
}
