package com.abaenglish.subscription.controller.v1.dto.transaction;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

@ApiModel(value="RestoreTransactionRequest", description="Request attributes to restore a payment transaction")
public class RestoreTransactionRequest implements Serializable{

    private static final long serialVersionUID = 992653642052250374L;

    @ApiModelProperty(
            value = "Payment transaction state to force before to send",
            allowableValues = "OPEN,PROCESS_ZUORA,PROCESS_ABA,VERIFY_PAYPAL",
            required = true
    )
    @NotEmpty
    private String state;

    @ApiModelProperty(
            value = "Event to send to state machine",
            allowableValues = "START_PROCESS, CONFIRMED_ZUORA, CONFIRMED_ABA, PAYPAL_VERIFIED",
            required = true
    )
    @NotEmpty
    private String event;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public static final class Builder {
        private String state;
        private String event;

        private Builder() {
        }

        public static Builder aRestoreTransactionRequest() {
            return new Builder();
        }

        public Builder state(String state) {
            this.state = state;
            return this;
        }

        public Builder event(String event) {
            this.event = event;
            return this;
        }

        public RestoreTransactionRequest build() {
            RestoreTransactionRequest restoreTransactionRequest = new RestoreTransactionRequest();
            restoreTransactionRequest.setState(state);
            restoreTransactionRequest.setEvent(event);
            return restoreTransactionRequest;
        }
    }
}
