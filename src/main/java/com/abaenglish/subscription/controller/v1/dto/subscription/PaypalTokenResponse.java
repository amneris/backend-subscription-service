package com.abaenglish.subscription.controller.v1.dto.subscription;


public class PaypalTokenResponse {

    private String paypalToken;

    public String getPaypalToken() {
        return paypalToken;
    }

    public void setPaypalToken(String paypalToken) {
        this.paypalToken = paypalToken;
    }
}
