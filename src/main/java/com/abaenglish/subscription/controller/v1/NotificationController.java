package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.ServerErrorApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.subscription.controller.v1.dto.notification.ZuoraNotificationRequest;
import com.abaenglish.subscription.controller.v1.dto.notification.ZuoraNotificationResponse;
import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.service.INotificationService;
import com.abaenglish.subscription.service.dto.notification.NotificationServiceDto;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Api(value = "notifications", description = "Operations about notifications.")
@RestController
@RequestMapping(value = "api/v1", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class NotificationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

    @Autowired
    private OrikaBeanMapper mapper;

    @Autowired
    private INotificationService notificationService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST, value = "/payment-notifications/zuora", headers = "Accept=application/xml,text/xml", produces = {"text/xml", "application/xml"})
    @ApiOperation(value = "Receive notifications from zuora", notes = "Receive notifications from events produced in Zuora platform")
    public ZuoraNotificationResponse zuoraPaymentNotification(@RequestBody ZuoraNotificationRequest request) throws ApiException {
        return processNotification(request);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST, value = "/subscription-notifications/zuora", headers = "Accept=application/xml,text/xml", produces = {"text/xml", "application/xml"})
    @ApiOperation(value = "Receive notifications from zuora", notes = "Receive notifications from events produced in Zuora platform")
    public ZuoraNotificationResponse zuoraSubscriptionNotification(@RequestBody ZuoraNotificationRequest request) throws ApiException {
        return processNotification(request);
    }

    /**
     * All notifications have the same treatment in the controller.
     * If a ServiceException is throwed, then return a ZuoraNotificationResponse with error details.
     * If another {@link Exception} is throwed, then return error
     *
     * @param zuoraNotificationRequest
     * @return
     * @throws ApiException
     */
    private ZuoraNotificationResponse processNotification(final ZuoraNotificationRequest zuoraNotificationRequest) throws ApiException {
        try {
            LOGGER.info("Received notification from zuora: [{}]", zuoraNotificationRequest.toString());
            final Optional<NotificationServiceDto> serviceResponse = notificationService.processZuoraNotification(mapper.map(zuoraNotificationRequest, ZuoraNotificationServiceRequest.class));
            if(serviceResponse.isPresent()) {
                return mapper.map(serviceResponse.get(), ZuoraNotificationResponse.class);
            } else {
                return new ZuoraNotificationResponse();
            }
        } catch (ServiceException e) {
            LOGGER.warn("There was a controlled error processing the notification",e);
            return ZuoraNotificationResponse.Builder.aZuoraNotificationResponse()
                    .success(evalIfItIsAnError(e.getCodeMessage()))
                    .errors(e.getMessage())
                    .build();
        } catch (Exception e) {
            LOGGER.error("Something went wrong processing the notification.", e);
            throw new ServerErrorApiException(e.getMessage());
        }
    }

    /**
     * Given a {@link CodeMessage} evaluate if it is an error for send to Zuora or not.
     *
     * Errors that are not errors for zuora:
     *  - {@link ErrorMessages#PAYMENT_PROCESSED_WRONG_ORIGIN}
     *  - {@link ErrorMessages#INVALID_NOTIFICATION_EVENT_CATEGORY}
     *  - {@link ErrorMessages#NOTIFICATION_ALREADY_PROCESSED}
     *
     * @param codeMessage
     * @return
     */
    private boolean evalIfItIsAnError(final CodeMessage codeMessage) {
        if(codeMessage != null) {
            return codeMessage.equals(ErrorMessages.PAYMENT_PROCESSED_WRONG_ORIGIN.getError())
                    || codeMessage.equals(ErrorMessages.INVALID_NOTIFICATION_EVENT_CATEGORY.getError())
                    || codeMessage.equals(ErrorMessages.NOTIFICATION_ALREADY_PROCESSED.getError());
        }
        return false;
    }
}
