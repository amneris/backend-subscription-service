package com.abaenglish.subscription.controller.v1.dto.transaction;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@ApiModel(value = "InvoiceResponse", description = "Information about invoice")
public class InvoiceApiResponse implements Serializable {

    private static final long serialVersionUID = 9047214986080175193L;

    @ApiModelProperty(value = "The price that the user has paid")
    private BigDecimal finalPrice;

    @ApiModelProperty(value = "Original base price")
    private BigDecimal basePrice;

    @ApiModelProperty(value = "Price discount applied")
    private BigDecimal discountPrice;

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public static final class Builder {
        private BigDecimal finalPrice;
        private BigDecimal basePrice;
        private BigDecimal discountPrice;

        private Builder() {
        }

        public static Builder anInvoiceApiResponse() {
            return new Builder();
        }

        public Builder finalPrice(BigDecimal finalPrice) {
            this.finalPrice = finalPrice;
            return this;
        }

        public Builder basePrice(BigDecimal basePrice) {
            this.basePrice = basePrice;
            return this;
        }

        public Builder discountPrice(BigDecimal discountPrice) {
            this.discountPrice = discountPrice;
            return this;
        }

        public InvoiceApiResponse build() {
            InvoiceApiResponse invoiceApiResponse = new InvoiceApiResponse();
            invoiceApiResponse.setFinalPrice(finalPrice);
            invoiceApiResponse.setBasePrice(basePrice);
            invoiceApiResponse.setDiscountPrice(discountPrice);
            return invoiceApiResponse;
        }
    }
}
