package com.abaenglish.subscription.controller.v1.dto.transaction;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@ApiModel(value="InvoiceItem", description="Information about invoice item")
public class InvoiceItemApiResponse implements Serializable{

    private static final long serialVersionUID = 158989773974797877L;

    @ApiModelProperty(value = "Quantity of items")
    private Integer quantity;
    @ApiModelProperty(value = "Total amount of the item")
    private BigDecimal total;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public static final class Builder {
        private Integer quantity;
        private BigDecimal total;

        private Builder() {
        }

        public static Builder anInvoiceItemApiResponse() {
            return new Builder();
        }

        public Builder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder total(BigDecimal total) {
            this.total = total;
            return this;
        }

        public InvoiceItemApiResponse build() {
            InvoiceItemApiResponse invoiceItemsApiResponse = new InvoiceItemApiResponse();
            invoiceItemsApiResponse.setQuantity(quantity);
            invoiceItemsApiResponse.setTotal(total);
            return invoiceItemsApiResponse;
        }
    }
}
