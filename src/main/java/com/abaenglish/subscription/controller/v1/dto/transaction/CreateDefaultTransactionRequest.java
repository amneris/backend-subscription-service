package com.abaenglish.subscription.controller.v1.dto.transaction;

import com.abaenglish.subscription.domain.SubscriptionType;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel(value="CreateDefaultTransactionRequest", description="Request to create a transaction")
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type", visible = true
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreatePaypalTransactionRequest.class, name = SubscriptionType._PayPal),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._ACH),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._BankTransfer),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._Cash),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._Check),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._CreditCard),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._CreditCardReferenceTransaction),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._DebitCard),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._Other),
        @JsonSubTypes.Type(value = CreateZuoraTransactionRequest.class, name = SubscriptionType._WireTransfer)
})
public abstract class CreateDefaultTransactionRequest {

    @ApiModelProperty(value = "User owner of the transactions", required = true)
    @NotNull
    private Long userId;

    @ApiModelProperty(value = "Subscription period", required = true)
    @NotNull
    private Integer period;

    @ApiModelProperty(value = "Subscription rate plans", required = true, dataType = "list")
    @NotNull
    private List<String> ratePlans;

    @ApiModelProperty(value = "Subscription type", required = true,
            allowableValues = "ACH, BankTransfer, Cash, Check, CreditCard, CreditCardReferenceTransaction, DebitCard, Other, PayPal, WireTransfer")
    @NotNull
    private SubscriptionType type;

    @ApiModelProperty(value = "Payment gateway", required = true)
    @NotNull
    private String gatewayName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public List<String> getRatePlans() {
        return ratePlans;
    }

    public void setRatePlans(List<String> ratePlans) {
        this.ratePlans = ratePlans;
    }

    public SubscriptionType getType() {
        return type;
    }

    public void setType(SubscriptionType type) {
        this.type = type;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    protected abstract static class Builder<T extends CreateDefaultTransactionRequest, B extends Builder<T, B>> {
        protected T type;
        protected B b;

        public Builder() {
            type = createObj();
            b = getThis();
        }

        public B userId(Long userId) {
            this.type.setUserId(userId);
            return b;
        }

        public B period(Integer period) {
            this.type.setPeriod(period);
            return this.b;
        }

        public B ratePlans(List<String> ratePlans) {
            this.type.setRatePlans(ratePlans);
            return this.b;
        }

        public B type(SubscriptionType type) {
            this.type.setType(type);
            return this.b;
        }

        public B gatewayName(String gatewayName) {
            this.type.setGatewayName(gatewayName);
            return this.b;
        }

        public T build() { return type; }
        protected abstract T createObj();
        protected abstract B getThis();
    }
}
