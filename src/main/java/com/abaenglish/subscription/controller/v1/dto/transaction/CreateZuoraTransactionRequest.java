package com.abaenglish.subscription.controller.v1.dto.transaction;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class CreateZuoraTransactionRequest extends CreateDefaultTransactionRequest {

    @ApiModelProperty(value = "Zuora payment reference id given previously", required = true)
    @NotEmpty
    private String zuoraRefId;

    public String getZuoraRefId() {
        return zuoraRefId;
    }

    public void setZuoraRefId(String zuoraRefId) {
        this.zuoraRefId = zuoraRefId;
    }

    public static class Builder extends CreateDefaultTransactionRequest.Builder<CreateZuoraTransactionRequest, CreateZuoraTransactionRequest.Builder> {

        public Builder zuoraRefId( String zuoraRefId ) {
            type.setZuoraRefId(zuoraRefId);
            return b;
        }
        protected CreateZuoraTransactionRequest createObj() { return new CreateZuoraTransactionRequest(); }
        protected Builder getThis() { return this; }
    }

}
