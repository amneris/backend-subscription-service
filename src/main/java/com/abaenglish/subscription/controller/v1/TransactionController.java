package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.exception.*;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.subscription.controller.v1.dto.transaction.CreateDefaultTransactionRequest;
import com.abaenglish.subscription.controller.v1.dto.transaction.RestoreTransactionRequest;
import com.abaenglish.subscription.controller.v1.dto.transaction.TransactionResponse;
import com.abaenglish.subscription.exception.service.ConflictServiceException;
import com.abaenglish.subscription.exception.service.IllegalArgumentServiceException;
import com.abaenglish.subscription.exception.service.NotFoundServiceException;
import com.abaenglish.subscription.service.ITransactionService;
import com.abaenglish.subscription.service.dto.transaction.PayloadServiceRequest;
import com.abaenglish.subscription.service.dto.transaction.TransactionExpand;
import com.abaenglish.subscription.statemachine.Events;
import com.abaenglish.subscription.statemachine.States;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.EnumSet;
import java.util.Set;

@Api(value = "transactions", description = "Operations about Transactions.")
@RestController
@RequestMapping(value = "api/v1/transactions", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class TransactionController {

    @Autowired
    private ITransactionService transactionService;
    @Autowired
    private OrikaBeanMapper mapper;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create Transaction", notes = "Create and persist one subscription transaction",
            response = TransactionResponse.class,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionResponse createTransaction(@Validated @RequestBody CreateDefaultTransactionRequest request) throws ApiException {
        final PayloadServiceRequest payloadServiceRequest = mapper.map(request, PayloadServiceRequest.class);
        try {
            return mapper.map(transactionService.openTransaction(payloadServiceRequest), TransactionResponse.class);
        } catch (NotFoundServiceException e) {
            throw new NotFoundApiException(e);
        } catch (ConflictServiceException e) {
            throw new ConflictApiException(e);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{publicId}")
    @ApiOperation(value = "Get one Transaction", notes = "Obtain one transaction by id", response = TransactionResponse.class)
    @ApiImplicitParams({@ApiImplicitParam(name = "expand", value = "Expand options to obtain extra information", dataType = "list", paramType = "query", allowableValues = "invoice,dueDate", allowMultiple = true)})
    public TransactionResponse getTransaction(@PathVariable String publicId, @RequestParam(required = false, value = "expand") Set<String> expand) throws ApiException {
        try {
            // Translate expand parameter
            final EnumSet<TransactionExpand> expandEnumSet = TransactionExpand.getTransactionExpandSet(expand);
            return mapper.map(transactionService.getPaymentTransactionExpandedByPublicId(publicId, expandEnumSet), TransactionResponse.class);
        } catch (NotFoundServiceException e) {
            throw new NotFoundApiException(e);
        } catch (IllegalArgumentServiceException e) {
            throw new BadRequestApiException(e);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{publicId}")
    @ApiOperation(value = "Restore state of Transaction",
            notes = "Resore state of Transaction from event given",
            response = TransactionResponse.class,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionResponse restoreTransaction(
            @PathVariable String publicId,
            @Validated @RequestBody RestoreTransactionRequest request) throws ApiException {
        try {
            return mapper.map(transactionService.restoreTransaction(publicId, Events.valueOf(request.getEvent()), States.valueOf(request.getState())), TransactionResponse.class);
        } catch (NotFoundServiceException e) {
            throw new NotFoundApiException(e);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }
    }
}
