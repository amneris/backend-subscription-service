package com.abaenglish.subscription.controller.v1.dto.transaction.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.subscription.controller.v1.dto.transaction.TransactionResponse;
import com.abaenglish.subscription.domain.PaymentTransaction;
import org.springframework.stereotype.Component;

@Component
public class TransactionResponseMapper extends BaseCustomMapper<PaymentTransaction, TransactionResponse> {
    public TransactionResponseMapper() {
        super();
        addField("publicId", "id");
        addField("subscriptionNumberZuora", "subscriptionId");
    }
}
