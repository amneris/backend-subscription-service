package com.abaenglish.subscription.controller.v1.dto.transaction;

import com.abaenglish.subscription.statemachine.States;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="TransactionResponse", description="Information about transaction")
public class TransactionResponse implements Serializable{

    private static final long serialVersionUID = 7617331219939164082L;

    @ApiModelProperty(value = "Public identifier")
    private String id;
    @ApiModelProperty(value = "State of the transaction", allowableValues = "OPEN, CLOSED, PROCESS_ZUORA, PROCESS_ABA, VERIFY_PAYPAL, PAYPAL_INVALID, ERROR")
    private States state;
    @ApiModelProperty(value = "User identifier")
    private Long userId;
    @ApiModelProperty(value = "Subscription type", allowableValues = "ACH, BankTransfer, Cash, Check, CreditCard, CreditCardReferenceTransaction, DebitCard, Other, PayPal, WireTransfer")
    private String type;
    @ApiModelProperty(value = "Period of the subscription")
    private Integer period;
    @ApiModelProperty(value = "Subscription identifier")
    private String subscriptionId;

 	@JsonInclude(JsonInclude.Include.NON_NULL)
    private String paypalToken;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Due date")
    private String dueDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "Detail of invoice related with the transaction")
    private InvoiceApiResponse invoice;


    public String getId() {
        return id;
    }

    public void setId(String uuid) {
        this.id = uuid;
    }

    public States getState() {
        return state;
    }

    public void setState(States states) {
        this.state = states;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getPaypalToken() {
        return paypalToken;
    }

    public void setPaypalToken(String paypalToken) {
        this.paypalToken = paypalToken;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public InvoiceApiResponse getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceApiResponse invoice) {
        this.invoice = invoice;
    }

    public static final class Builder {
        private String id;
        private States state;
        private Long userId;
        private String type;
        private Integer period;
        private String subscriptionId;
        private String paypalToken;

        private Builder() {
        }

        public static Builder aTransactionResponse() {
            return new Builder();
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder state(States state) {
            this.state = state;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder period(Integer period) {
            this.period = period;
            return this;
        }

        public Builder subscriptionId(String subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public Builder tokenPaypal(String tokenPaypal) {
            this.paypalToken = tokenPaypal;
            return this;
        }

        public TransactionResponse build() {
            TransactionResponse transactionResponse = new TransactionResponse();
            transactionResponse.setId(id);
            transactionResponse.setState(state);
            transactionResponse.setUserId(userId);
            transactionResponse.setType(type);
            transactionResponse.setPeriod(period);
            transactionResponse.setSubscriptionId(subscriptionId);
            transactionResponse.setPaypalToken(paypalToken);
            return transactionResponse;
        }
    }
}
