package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.ServerErrorApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.external.paypal.domain.PaypalExternalTokenRequest;
import com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionRequest;
import com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionResponse;
import com.abaenglish.subscription.controller.v1.dto.subscription.PaypalTokenRequest;
import com.abaenglish.subscription.controller.v1.dto.subscription.PaypalTokenResponse;
import com.abaenglish.subscription.service.ISubscriptionService;
import com.abaenglish.subscription.service.dto.subscription.CreateSubscriptionServiceResponse;
import com.abaenglish.subscription.service.dto.subscription.SubscriptionServiceRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "subscriptions", description = "Operations about Subscriptions.")
@RestController
@RequestMapping(value = "api/v1/subscriptions", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SubscriptionController {

    @Autowired
    private ISubscriptionService subscriptionService;
    @Autowired
    private OrikaBeanMapper mapper;

    @Deprecated // use open transaction
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create Subscription", notes = "This can only be done with well-formed JSON")
    public CreateSubscriptionResponse createSubscription(@RequestBody CreateSubscriptionRequest subscription) throws ApiException {
        try {
            SubscriptionServiceRequest newSubscription = mapper.map(subscription, SubscriptionServiceRequest.class);

            CreateSubscriptionServiceResponse serviceResponse = subscriptionService.createSubscription(newSubscription);

            return mapper.map(serviceResponse, CreateSubscriptionResponse.class);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/paypal/token")
    @ApiOperation(value = "Create PayPal paypalToken", notes = "Create PayPal paypalToken for after obtain BAID")
    public PaypalTokenResponse createTokenPayPal(@RequestBody PaypalTokenRequest request) throws ApiException {

        try {
            PaypalTokenResponse tokenResponse = new PaypalTokenResponse();
            tokenResponse.setPaypalToken(subscriptionService.createToken(mapper.map(request, PaypalExternalTokenRequest.class)));
            return tokenResponse;
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }
    }
}
