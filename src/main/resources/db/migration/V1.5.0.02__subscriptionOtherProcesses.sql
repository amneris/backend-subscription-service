CREATE TABLE subscription_cancelled_trace (
  id              bigint(20) NOT NULL AUTO_INCREMENT,
  subscription_id varchar(100) NOT NULL,
  success         tinyint(1),
  reason          varchar(255),
  created_date    TIMESTAMP NOT NULL DEFAULT now(),
  updated_date    TIMESTAMP DEFAULT now(),
  PRIMARY KEY (id),
  UNIQUE KEY subscriptionId (subscription_id)
);