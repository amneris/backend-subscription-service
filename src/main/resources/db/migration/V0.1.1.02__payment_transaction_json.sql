ALTER TABLE payment_transaction ADD COLUMN payload VARCHAR(255);
ALTER TABLE payment_transaction CHANGE subscription_id_zuora subscription_id_zuora VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE payment_transaction ADD COLUMN invoice_id_zuora VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER subscription_id_zuora;
ALTER TABLE payment_transaction ADD COLUMN subscription_number_zuora VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER subscription_id_zuora;
ALTER TABLE payment_transaction ADD COLUMN account_id_zuora VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER subscription_number_zuora;
ALTER TABLE payment_transaction ADD COLUMN account_number_zuora VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER account_id_zuora;
ALTER TABLE payment_transaction ADD COLUMN amount_original DECIMAL(18,9) NULL DEFAULT '0' AFTER invoice_id_zuora;
