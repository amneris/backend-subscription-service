CREATE TABLE payment_hostedpages (
  id            BIGINT PRIMARY KEY AUTO_INCREMENT,
  pageId        VARCHAR(100) NOT NULL,
  pageName      VARCHAR(100) NOT NULL,
  pageType      VARCHAR(100) NOT NULL,
  pageVersion   INTEGER NOT NULL,
  active        BOOLEAN DEFAULT TRUE
);

insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (1,  '2c92c0f955a0b5bb0155a162c1aa255b', 'DEV - Adyen',                 'Credit Card',  2,  TRUE);
--insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (2,  '2c92c0f955a0b5b80155a16436b32969', 'DEV - Credit Card HPM 2.0',   'Credit Card',  2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (3,  '2c92c0f955a0b5bb0155a1663e653374', 'DEV - SEPA Direct debit',     'SEPA',         2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (4,  '2c92c0f855c9f4620155ca4bcb116f09', 'DEV - Stripe',                'Credit Card',  2,  TRUE);
--insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (5,  '2c92c0f855a0a9660155a1675c2b79e7', 'DEV - Zilla HPM 2.0 - SEPA',  'SEPA',         2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (6,  '{"merchanid":"22V3M4VKL5GTW"}',    'DEV - PayPal',                'PayPal',       2,  TRUE);

insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (7,  '2c92c0f9558201b00155924bb7392058',  'LOCAL - Adyen',                'Credit Card',  2,  TRUE);
--insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (8,  '2c92c0f854a35e960154a965550b6ece',  'LOCAL - Credit Card HPM 2.0',  'Credit Card',  2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (9,  '2c92c0f95595724b0155960773197a73',  'LOCAL - SEPA Direct debit',    'SEPA',         2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (10, '2c92c0f855c9f4b20155ca4db93d77b7',  'LOCAL - Stripe',               'Credit Card',  2,  TRUE);
--insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (11, '2c92c0f854a35e960154a9668350793a',  'LOCAL - Zilla HPM 2.0 - SEPA', 'SEPA',         2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (12, '{"merchanid":"22V3M4VKL5GTW"}',     'LOCAL - PayPal',               'PayPal',       2,  TRUE);

insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (13,  '2c92c0f955ca02920155de1e5f142cf2',  'QA - Adyen',                'Credit Card',  2,  TRUE);
--insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (14,  '2c92c0f955ca02910155de6c66ea755b',  'QA - Credit Card HPM 2.0',  'Credit Card',  2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (15,  '2c92c0f955ca02910155de78cd901a21',  'QA - SEPA Direct debit',    'SEPA',         2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (16,  '2c92c0f855c9f4620155de7e2bbc75ad',  'QA - Stripe',               'Credit Card',  2,  TRUE);
--insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (17,  '2c92c0f955ca02920155de822dff5b4e',  'QA - Zilla HPM 2.0 - SEPA', 'SEPA',         2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (18,  '{"merchanid":"22V3M4VKL5GTW"}',     'QA - PayPal',               'PayPal',       2,  TRUE);

insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (19,  '2c92a0ff567e360b01568d3479ba5224',  'PRO - Adyen',                'Credit Card',  2,  TRUE);
-- insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (20,  '2c92c0f955ca02910155de6c66ea755b',  'PRO - Credit Card HPM 2.0',  'Credit Card',  2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (21,  '2c92a0fe56b17c5f0156b2092c9a6242',  'PRO - SEPA Direct debit',    'SEPA',         2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (22,  '2c92a0fe567e364d01568d282de566d9',  'PRO - Stripe',               'Credit Card',  2,  TRUE);
-- insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (23,  '2c92c0f955ca02920155de822dff5b4e',  'PRO - Zilla HPM 2.0 - SEPA', 'SEPA',         2,  TRUE);
insert into payment_hostedpages(id, pageId, pageName, pageType, pageVersion,  active) values (24,  '{"merchanid":"38FJ47NHMEL38"}',     'PRO - PayPal',               'PayPal',       2,  TRUE);
