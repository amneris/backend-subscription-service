CREATE TABLE payment_transaction (
  id          		BIGINT PRIMARY KEY AUTO_INCREMENT,
  public_id       VARCHAR(100) NOT NULL,
  state 	        VARCHAR(25) NOT NULL,
  user_id         BIGINT NOT NULL,
  type            VARCHAR(25) NOT NULL,
  period 	        INTEGER,
  subscription_id_zuora VARCHAR(25),
  creation_date 	timestamp NOT NULL DEFAULT now()
);



ALTER TABLE ipn_paypal ADD COLUMN payment_transaction_id BIGINT;