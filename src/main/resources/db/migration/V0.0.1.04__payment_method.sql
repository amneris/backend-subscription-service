CREATE TABLE payment_method (
  id          BIGINT PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(100) NOT NULL,
  active      BOOLEAN
);

insert into payment_method(id, name, active) values (1, 'ACH', TRUE );
insert into payment_method(id, name, active) values (2, 'Cash', TRUE );
insert into payment_method(id, name, active) values (3, 'Check', TRUE );
insert into payment_method(id, name, active) values (4, 'CreditCard', TRUE );
insert into payment_method(id, name, active) values (5, 'PayPal', TRUE );
insert into payment_method(id, name, active) values (6, 'WireTransfer', TRUE );
insert into payment_method(id, name, active) values (7, 'DebitCard', TRUE );
insert into payment_method(id, name, active) values (8, 'CreditCardReferenceTransaction', TRUE );
insert into payment_method(id, name, active) values (9, 'BankTransfer', TRUE );
insert into payment_method(id, name, active) values (10, 'Other', TRUE );

