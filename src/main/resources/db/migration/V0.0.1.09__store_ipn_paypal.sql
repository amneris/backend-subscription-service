CREATE TABLE ipn_paypal (
  id          		BIGINT PRIMARY KEY AUTO_INCREMENT,
  ipn        		text NOT NULL,
  creation_date 	timestamp NOT NULL DEFAULT now(),
  payment_status 	VARCHAR(25) NOT NULL,
  txn_id 	VARCHAR(25) NOT NULL,
  txn_type 	VARCHAR(25) NOT NULL,
  subscr_id VARCHAR(25) NOT NULL,
  user_id BIGINT
);