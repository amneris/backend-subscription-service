CREATE TABLE subscription_process (
  id            BIGINT PRIMARY KEY AUTO_INCREMENT,
  name          VARCHAR(100) NOT NULL,
  last_process  DATE,
  UNIQUE KEY `name` (`name`)
);

INSERT INTO subscription_process(name,last_process) VALUES ('renew', '2016-01-01');
INSERT INTO subscription_process(name,last_process) VALUES ('cancel', '2016-01-01');
INSERT INTO subscription_process(name,last_process) VALUES ('refund', '2016-01-01');