ALTER TABLE payment_transaction
ADD gateway_name varchar(64) DEFAULT NULL,
ADD paypal_token varchar(64) DEFAULT NULL;