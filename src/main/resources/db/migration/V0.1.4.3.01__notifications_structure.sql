CREATE TABLE zuora_notification (
  id              BIGINT PRIMARY KEY AUTO_INCREMENT,
  event_category  VARCHAR(100) NOT NULL,
  status          VARCHAR(10) NOT NULL,
  payload         VARCHAR(1000) NOT NULL,
  error           VARCHAR(255) ,
  creation_date 	timestamp NOT NULL DEFAULT now(),
  update_date 	  timestamp DEFAULT now()
);
