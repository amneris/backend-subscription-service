INSERT INTO payment_gateway (id, name, gateway, active)
VALUES
	(8, 'Test Abaenglish', 'Test Gateway', 1),
	(9, 'Stripe Payment Gateway', 'Stripe v1', 1),
	(10, 'Paypal Express Checkout', 'PayPal Express Checkout', 1),
	(11, 'Adyen Payment Gateway', 'Adyen', 1);


update `payment_form` set gateway_id = 9 where gateway_id = 2 and environmenturl = 'premium.abaenglish.com';

update `payment_form` set gateway_id = 10 where gateway_id = 7 and environmenturl = 'premium.abaenglish.com';

update `payment_form` set gateway_id = 11 where gateway_id = 6 and environmenturl = 'premium.abaenglish.com';