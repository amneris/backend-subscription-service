ALTER TABLE zuora_notification ADD zuora_reference_id varchar(100) NOT NULL;
ALTER TABLE zuora_notification ADD UNIQUE unique_event_ref_id(event_category, zuora_reference_id);
DROP TABLE subscription_cancelled_trace;
DROP TABLE subscription_process;