package com.abaenglish.subscription.domain;

import com.abaenglish.subscription.objectmother.PaymentTransactionObjectMother;
import com.abaenglish.subscription.statemachine.States;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PaymentTransactionTest {

    protected static final Long PAYMENT_TRANSACTION_1_ID = 1L;
    protected static final LocalDateTime PAYMENT_TRANSACTION_1_DATE = LocalDateTime.now();
    protected static final Integer PAYMENT_TRANSACTION_1_PERIOD = 12;
    protected static final String PAYMENT_TRANSACTION_1_PUBLIC_ID = "12345";
    protected static final States PAYMENT_TRANSACTION_1_STATE = States.OPEN;
    protected static final String PAYMENT_TRANSACTION_1_SUBSCRIPTIONID = "S12345";
    protected static final String PAYMENT_TRANSACTION_1_INVOICEID = "I12345";
    protected static final String PAYMENT_TRANSACTION_1_TYPE = SubscriptionType.CreditCard.name();
    protected static final Long PAYMENT_TRANSACTION_1_USERID = 12345L;

    protected static final Long PAYMENT_TRANSACTION_2_ID = 2L;
    protected static final LocalDateTime PAYMENT_TRANSACTION_2_DATE = LocalDateTime.now();
    protected static final Integer PAYMENT_TRANSACTION_2_PERIOD = 24;
    protected static final String PAYMENT_TRANSACTION_2_PUBLIC_ID = "54321";
    protected static final States PAYMENT_TRANSACTION_2_STATE = States.CLOSED;
    protected static final String PAYMENT_TRANSACTION_2_SUBSCRIPTIONID = "S54321";
    protected static final String PAYMENT_TRANSACTION_2_INVOICEID = "I12345";
    protected static final String PAYMENT_TRANSACTION_2_TYPE = SubscriptionType.PayPal.name();
    protected static final Long PAYMENT_TRANSACTION_2_USERID = 54321L;

    @Test
    public void equals() {
        PaymentTransaction pt = PaymentTransactionObjectMother.paymentTransaction();

        assertThat(pt.getId()).isEqualTo(PAYMENT_TRANSACTION_1_ID);
        assertThat(pt.getCreationDate()).isEqualToIgnoringHours(PAYMENT_TRANSACTION_1_DATE);
        assertThat(pt.getPeriod()).isEqualTo(PAYMENT_TRANSACTION_1_PERIOD);
        assertThat(pt.getPublicId()).isEqualTo(PAYMENT_TRANSACTION_1_PUBLIC_ID);
        assertThat(pt.getState()).isEqualTo(PAYMENT_TRANSACTION_1_STATE);
        assertThat(pt.getSubscriptionIdZuora()).isEqualTo(PAYMENT_TRANSACTION_1_SUBSCRIPTIONID);
        assertThat(pt.getInvoiceIdZuora()).isEqualTo(PAYMENT_TRANSACTION_1_INVOICEID);
        assertThat(pt.getType()).isEqualTo(PAYMENT_TRANSACTION_1_TYPE);
        assertThat(pt.getUserId()).isEqualTo(PAYMENT_TRANSACTION_1_USERID);

        PaymentTransaction itemClone = PaymentTransactionObjectMother.paymentTransaction();
        assertThat(itemClone.getId()).isEqualTo(PAYMENT_TRANSACTION_1_ID);
        assertThat(itemClone.getCreationDate()).isEqualToIgnoringHours(PAYMENT_TRANSACTION_1_DATE);
        assertThat(itemClone.getPeriod()).isEqualTo(PAYMENT_TRANSACTION_1_PERIOD);
        assertThat(itemClone.getPublicId()).isEqualTo(PAYMENT_TRANSACTION_1_PUBLIC_ID);
        assertThat(itemClone.getState()).isEqualTo(PAYMENT_TRANSACTION_1_STATE);
        assertThat(itemClone.getSubscriptionIdZuora()).isEqualTo(PAYMENT_TRANSACTION_1_SUBSCRIPTIONID);
        assertThat(itemClone.getInvoiceIdZuora()).isEqualTo(PAYMENT_TRANSACTION_1_INVOICEID);
        assertThat(itemClone.getType()).isEqualTo(PAYMENT_TRANSACTION_1_TYPE);
        assertThat(itemClone.getUserId()).isEqualTo(PAYMENT_TRANSACTION_1_USERID);

        PaymentTransaction itemOther = PaymentTransactionObjectMother.paymentTransactionClosed();
        assertThat(itemOther.getId()).isEqualTo(PAYMENT_TRANSACTION_2_ID);
        assertThat(itemOther.getCreationDate()).isEqualToIgnoringHours(PAYMENT_TRANSACTION_2_DATE);
        assertThat(itemOther.getPeriod()).isEqualTo(PAYMENT_TRANSACTION_2_PERIOD);
        assertThat(itemOther.getPublicId()).isEqualTo(PAYMENT_TRANSACTION_2_PUBLIC_ID);
        assertThat(itemOther.getState()).isEqualTo(PAYMENT_TRANSACTION_2_STATE);
        assertThat(itemOther.getSubscriptionIdZuora()).isEqualTo(PAYMENT_TRANSACTION_2_SUBSCRIPTIONID);
        assertThat(itemOther.getInvoiceIdZuora()).isEqualTo(PAYMENT_TRANSACTION_2_INVOICEID);
        assertThat(itemOther.getType()).isEqualTo(PAYMENT_TRANSACTION_2_TYPE);
        assertThat(itemOther.getUserId()).isEqualTo(PAYMENT_TRANSACTION_2_USERID);

        assertFalse(pt.equals(itemOther));
        assertFalse(itemClone.equals(itemOther));
    }

    @Test
    public void testHashCode() {
        PaymentTransaction item = PaymentTransactionObjectMother.paymentTransaction();
        PaymentTransaction itemClone = PaymentTransactionObjectMother.paymentTransaction();
        PaymentTransaction itemOther = PaymentTransactionObjectMother.paymentTransactionClosed();

        assertEquals(item.hashCode(), itemClone.hashCode());
        assertNotEquals(item.hashCode(), itemOther.hashCode());
        assertNotEquals(itemClone.hashCode(), itemOther.hashCode());
    }

}