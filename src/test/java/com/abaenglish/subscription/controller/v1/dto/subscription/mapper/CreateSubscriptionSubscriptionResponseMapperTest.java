package com.abaenglish.subscription.controller.v1.dto.subscription.mapper;

import com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionResponse;
import com.abaenglish.subscription.objectmother.SubscriptionObjectMother;
import com.abaenglish.subscription.service.dto.subscription.CreateSubscriptionServiceResponse;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

public class CreateSubscriptionSubscriptionResponseMapperTest {

    private MapperFacade mapper;

    @Before
    public void setUp() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        CreateSubscriptionResponseMapper createSubscriptionResponseMapper = new CreateSubscriptionResponseMapper();

        ClassMapBuilder<?, ?> classMap = mapperFactory.classMap(CreateSubscriptionServiceResponse.class, CreateSubscriptionResponse.class).customize(createSubscriptionResponseMapper);
        createSubscriptionResponseMapper.getFields().forEach((k, v) -> classMap.field((String) k, (String) v));
        classMap.byDefault().register();

        mapper = mapperFactory.getMapperFacade();
    }

    @Test
    public void createSubscriptionWithAccount() {
        CreateSubscriptionServiceResponse createSubscriptionServiceResponse = SubscriptionObjectMother.createSubscriptionServiceResponse();

        CreateSubscriptionResponse createSubscriptionResponse = mapper.map(createSubscriptionServiceResponse, CreateSubscriptionResponse.class);

        assertNotNull(createSubscriptionResponse);
        assertThat(createSubscriptionResponse.getEmail()).isEqualTo(createSubscriptionServiceResponse.getUserZuora().getUser().getEmail());
        assertThat(createSubscriptionResponse.getAccountNumber()).isEqualTo(createSubscriptionServiceResponse.getUserZuora().getZuora().getZuoraAccountNumber());
    }

    @Test
    public void createSubscriptionWithoutAccount() {
        CreateSubscriptionServiceResponse createSubscriptionServiceResponse = SubscriptionObjectMother.createAccountSubscriptionServiceResponse();

        CreateSubscriptionResponse createSubscriptionResponse = mapper.map(createSubscriptionServiceResponse, CreateSubscriptionResponse.class);

        assertNotNull(createSubscriptionResponse);
        assertThat(createSubscriptionResponse.getEmail()).isEqualTo(createSubscriptionServiceResponse.getUserZuora().getUser().getEmail());
        assertThat(createSubscriptionResponse.getAccountNumber()).isEqualTo(createSubscriptionServiceResponse.getSubscribeResults()[0].getAccountNumber());
    }

}
