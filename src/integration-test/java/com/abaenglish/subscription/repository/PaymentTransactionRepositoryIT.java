package com.abaenglish.subscription.repository;

import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.controller.v1.TestConfiguration;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.repository.predicate.PaymentTransactionPredicate;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static com.abaenglish.subscription.objectmother.PaymentTransactionObjectMother.zuoraConfirmedTransactionCreditCard;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@DatabaseSetup(value = "../base/transactions.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../base/transactions-empty.xml")
public class PaymentTransactionRepositoryIT {

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Test
    public void findByPublicId() {
        PaymentTransaction paymentTransaction = paymentTransactionRepository.findOne(PaymentTransactionPredicate.findByPublicId(zuoraConfirmedTransactionCreditCard().getPublicId()));
        assertNotNull(paymentTransaction);
        assertThat(paymentTransaction, Matchers.equalTo(zuoraConfirmedTransactionCreditCard()));
    }
}
