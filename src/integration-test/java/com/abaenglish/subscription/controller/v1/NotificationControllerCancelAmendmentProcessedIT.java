package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.data.repository.dataset.CustomReplacementDataSetLoader;
import com.abaenglish.external.abawebapps.domain.CancelSubscriptionsRequest;
import com.abaenglish.external.abawebapps.domain.SubscriptionInfoResponse;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.service.impl.AbawebappsExternalService;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.abaenglish.external.zuora.soap.service.impl.SubscriptionStatus;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.abaenglish.subscription.objectmother.NotificationObjectMother.*;
import static com.abaenglish.subscription.objectmother.ZuoraObjectMother.cancelAmendPayed;
import static com.abaenglish.subscription.objectmother.ZuoraObjectMother.cancelledSubscriptions;
import static io.restassured.RestAssured.given;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = CustomReplacementDataSetLoader.class)
@DatabaseSetup(value = "../../base/zuora-notifications.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../../base/zuora-notifications-empty.xml")
public class NotificationControllerCancelAmendmentProcessedIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private AbawebappsExternalService abawebappsExternalService;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        reset(zuoraExternalService, abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/cancel-amendment-processed/new_notification_cancel_amendment_processed.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationCancelAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(cancelAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.CANCELLED))))
                .thenReturn(cancelledSubscriptions());

        when(abawebappsExternalService.cancelSubscriptionsZuora(anyList())).thenReturn(Arrays.asList(
                SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                        .originalSubscriptionId(serviceRequest.getSubscriptionOriginalId())
                        .success(true)
                        .build()));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.empty()));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedCancelSubscriptionsRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).cancelSubscriptionsZuora(expectedCancelSubscriptionsRequestAux.capture());
        List<CancelSubscriptionsRequest> expectedCancelSubscriptionsRequest = (List<CancelSubscriptionsRequest>) expectedCancelSubscriptionsRequestAux.getValue();
        Assert.assertEquals(serviceRequest.getSubscriptionOriginalId(), expectedCancelSubscriptionsRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(cancelledSubscriptions().get(0).getCancelledDate(), expectedCancelSubscriptionsRequest.get(0).getCancelledDate());
    }

    @Test
    @ExpectedDatabase(value = "expected/cancel-amendment-processed/new_notification_cancel_amendment_processed_amendNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AmendNotFound() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationCancelAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(Arrays.asList()); //empty list

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("The amend id '2c92c08659b048b70159caa0d8520de7', has no 'Cancellation' amend related")));

        verify(zuoraExternalService, never()).getSubscriptionsInfoByOriginalId(any(), any());
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/cancel-amendment-processed/new_notification_cancel_amendment_processed_errorTooManyAmends.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_TooManyAmends() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationCancelAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(Arrays.asList(cancelAmendPayed().get(0), cancelAmendPayed().get(0)));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Invalid amend results.")));

        verify(zuoraExternalService, never()).getSubscriptionsInfoByOriginalId(any(), any());
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/cancel-amendment-processed/new_notification_cancel_amendment_processed_errorDraftAmend.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AmendNotCompleted() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationCancelAmendmentProcessedServiceRequest();

        final ZuoraAmendmentResponse draftAmend = cancelAmendPayed().get(0);
        draftAmend.setStatus("Draft");
        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(Arrays.asList(draftAmend));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Invalid amend results.")));

        verify(zuoraExternalService, never()).getSubscriptionsInfoByOriginalId(any(), any());
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/cancel-amendment-processed/new_notification_cancel_amendment_processed_errorSubscriptionNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_SubscriptionNotFound() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationCancelAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(cancelAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.CANCELLED))))
                .thenReturn(Arrays.asList());
     // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Current subscription not found for original id '2c92c0f9588ff00b0158923c53e9252e' and amendment id '2c92c08659b048b70159caa0d8520de7'")));

        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/cancel-amendment-processed/new_notification_cancel_amendment_processed_errorTooManySubscriptions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_TooManySubscriptions() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationCancelAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(cancelAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.CANCELLED))))
                .thenReturn(Arrays.asList(cancelledSubscriptions().get(0), cancelledSubscriptions().get(0)));
        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot register cancellation subscription.")));

        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/cancel-amendment-processed/new_notification_cancel_amendment_processed_errorUnexpectedFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaFails() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationCancelAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(cancelAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.CANCELLED))))
                .thenReturn(cancelledSubscriptions());

        NotFoundExternalException ex = new NotFoundExternalException(new Exception("Unexpected error"));
        when(abawebappsExternalService.cancelSubscriptionsZuora(anyList())).thenThrow(ex);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot send cancellation subscription information to Aba")));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedCancelSubscriptionsRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).cancelSubscriptionsZuora(expectedCancelSubscriptionsRequestAux.capture());
        List<CancelSubscriptionsRequest> expectedCancelSubscriptionsRequest = (List<CancelSubscriptionsRequest>) expectedCancelSubscriptionsRequestAux.getValue();
        Assert.assertEquals(serviceRequest.getSubscriptionOriginalId(), expectedCancelSubscriptionsRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(cancelledSubscriptions().get(0).getCancelledDate(), expectedCancelSubscriptionsRequest.get(0).getCancelledDate());
    }

    @Test
    @ExpectedDatabase(value = "expected/cancel-amendment-processed/new_notification_cancel_amendment_processed_errorsFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaReturnErrors() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationCancelAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(cancelAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.CANCELLED))))
                .thenReturn(cancelledSubscriptions());

        when(abawebappsExternalService.cancelSubscriptionsZuora(anyList())).thenReturn(Arrays.asList(
                SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                        .originalSubscriptionId(serviceRequest.getSubscriptionOriginalId())
                        .success(false)
                        .warnings("Errors on Aba")
                        .build()));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Errors on Aba")));

         /*
            check the arguments sent to aba are valid
         */

        ArgumentCaptor<List> expectedCancelSubscriptionsRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).cancelSubscriptionsZuora(expectedCancelSubscriptionsRequestAux.capture());
        List<CancelSubscriptionsRequest> expectedCancelSubscriptionsRequest = (List<CancelSubscriptionsRequest>) expectedCancelSubscriptionsRequestAux.getValue();
        Assert.assertEquals(serviceRequest.getSubscriptionOriginalId(), expectedCancelSubscriptionsRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(cancelledSubscriptions().get(0).getCancelledDate(), expectedCancelSubscriptionsRequest.get(0).getCancelledDate());
    }
}
