package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.data.repository.dataset.CustomReplacementDataSetLoader;
import com.abaenglish.external.abawebapps.domain.RefundPaymentsRequest;
import com.abaenglish.external.abawebapps.domain.SubscriptionInfoResponse;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.abaenglish.subscription.objectmother.NotificationObjectMother.*;
import static com.abaenglish.subscription.objectmother.ZuoraObjectMother.refundResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasXPath;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = CustomReplacementDataSetLoader.class)
@DatabaseSetup(value = "../../base/zuora-notifications.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../../base/zuora-notifications-empty.xml")
public class NotificationControllerRefundIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private IAbawebappsExternalService abawebappsExternalService;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        reset(zuoraExternalService, abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/refund-processed/new_notification_refund_processed.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AllPerfect() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRefundProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getRefundInfoById(eq(serviceRequest.getRefundId()), eq(Optional.of(ZuoraExternalServiceSoap.RefundStatus.PROCESSED))))
                .thenReturn(refundResponse());

        when(abawebappsExternalService.refundSubscriptionsZuora(anyList())).thenReturn(Arrays.asList(
                SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                        .originalSubscriptionId(serviceRequest.getSubscriptionOriginalId())
                        .success(true)
                        .build()));


        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.empty()));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedRefundPaymentsRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).refundSubscriptionsZuora(expectedRefundPaymentsRequestAux.capture());
        List<RefundPaymentsRequest> expectedRefundPaymentsRequest = (List<RefundPaymentsRequest>) expectedRefundPaymentsRequestAux.getValue();
        Assert.assertEquals(serviceRequest.getRefundId(), expectedRefundPaymentsRequest.get(0).getId());
        Assert.assertEquals(serviceRequest.getAccountId(), expectedRefundPaymentsRequest.get(0).getAccountId());
        Assert.assertEquals(refundResponse().getRefundDate(), expectedRefundPaymentsRequest.get(0).getRefundDate());
        Assert.assertEquals(refundResponse().getAmount(), expectedRefundPaymentsRequest.get(0).getAmount());
    }


    @Test
    @ExpectedDatabase(value = "expected/payment-refund/new_notification_refund_errorUnexpectedFromZuora.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_ZuoraFails() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRefundProcessedServiceRequest();

        // mock notification service external calls
        RuntimeException ex = new RuntimeException("Unexpected error");
        when(zuoraExternalService.getRefundInfoById(any(), any())).thenThrow(ex);

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder()
                .expectStatusCode(500)
                .expectContentType(ContentType.XML)
                .expectBody(hasXPath("/Map/message[text()='" + ex.getMessage() + "']"));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(responseSpecBuilder.build());

        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-refund/new_notification_refund_errorNotFoundRefund.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_notFoundRefund() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRefundProcessedServiceRequest();

        // mock notification service external calls
        RuntimeException ex = new RuntimeException("Unexpected error");
        when(zuoraExternalService.getRefundInfoById(eq(serviceRequest.getRefundId()), eq(Optional.of(ZuoraExternalServiceSoap.RefundStatus.PROCESSED))))
                .thenReturn(null);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Refund with id '2c92c0f859d9576f0159f4e8bb912896' not found in Zuora")));

        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-refund/new_notification_refund_errorUnexpectedFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaFails() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRefundProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getRefundInfoById(eq(serviceRequest.getRefundId()), eq(Optional.of(ZuoraExternalServiceSoap.RefundStatus.PROCESSED))))
                .thenReturn(refundResponse());
        RuntimeException ex = new RuntimeException("Unexpected error from aba");
        when(abawebappsExternalService.refundSubscriptionsZuora(any())).thenThrow(ex);

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder()
                .expectStatusCode(500)
                .expectContentType(ContentType.XML)
                .expectBody(hasXPath("/Map/message[text()='" + ex.getMessage() + "']"));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(responseSpecBuilder.build());

    }

    @Test
    @ExpectedDatabase(value = "expected/payment-refund/new_notification_refund_errorUserNotFoundFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaNotFoundUser() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRefundProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getRefundInfoById(eq(serviceRequest.getRefundId()), eq(Optional.of(ZuoraExternalServiceSoap.RefundStatus.PROCESSED))))
                .thenReturn(refundResponse());
        NotFoundExternalException ex = new NotFoundExternalException(new Exception("User not found"));
        when(abawebappsExternalService.refundSubscriptionsZuora(any())).thenThrow(ex);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("User not found")));

    }

    @Test
    @ExpectedDatabase(value = "expected/payment-refund/new_notification_refund_errorFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaReturnErrors() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRefundProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getRefundInfoById(eq(serviceRequest.getRefundId()), eq(Optional.of(ZuoraExternalServiceSoap.RefundStatus.PROCESSED))))
                .thenReturn(refundResponse());
        when(abawebappsExternalService.refundSubscriptionsZuora(any()))
                .thenReturn(Arrays.asList(SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                        .success(false)
                        .warnings("Error processing refund")
                        .build()));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Error processing refund")));

    }
}
