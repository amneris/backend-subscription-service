package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.data.repository.dataset.CustomReplacementDataSetLoader;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.exception.ErrorMessages;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Optional;

import static com.abaenglish.subscription.objectmother.NotificationObjectMother.buildRequestXML;
import static com.abaenglish.subscription.objectmother.NotificationObjectMother.buildResponseSpecificationXml;
import static io.restassured.RestAssured.given;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = CustomReplacementDataSetLoader.class)
@DatabaseSetup(value = "../../base/zuora-notifications.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../../base/zuora-notifications-empty.xml")
public class NotificationControllerInvalidIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private IAbawebappsExternalService abawebappsExternalService;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        Mockito.reset(zuoraExternalService, abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "../../base/zuora-notifications.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receivePaymentNotification() throws Exception {

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                        .eventCategory("invalidEventCategory")
                        .build())).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.of(ErrorMessages.INVALID_NOTIFICATION_EVENT_CATEGORY.getError().getMessage())));
        Mockito.verifyZeroInteractions(zuoraExternalService,abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "../../base/zuora-notifications.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveSubscriptionNotification() throws Exception {

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                        .eventCategory("invalidEventCategory")
                        .build())).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.of(ErrorMessages.INVALID_NOTIFICATION_EVENT_CATEGORY.getError().getMessage())));
        Mockito.verifyZeroInteractions(zuoraExternalService,abawebappsExternalService);
    }
}
