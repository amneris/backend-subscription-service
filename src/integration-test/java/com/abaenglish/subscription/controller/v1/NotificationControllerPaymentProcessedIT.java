package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.data.repository.dataset.CustomReplacementDataSetLoader;
import com.abaenglish.external.abawebapps.domain.RenewPaymentRequest;
import com.abaenglish.external.abawebapps.domain.SubscriptionInfoResponse;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.abaenglish.subscription.objectmother.NotificationObjectMother.*;
import static com.abaenglish.subscription.objectmother.ZuoraObjectMother.*;
import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = CustomReplacementDataSetLoader.class)
@DatabaseSetup(value = "../../base/zuora-notifications.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../../base/zuora-notifications-empty.xml")
public class NotificationControllerPaymentProcessedIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private IAbawebappsExternalService abawebappsExternalService;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        reset(zuoraExternalService, abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromRenewalInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromRenewalInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(activeSubscriptionsRenewed());
        when(zuoraExternalService.getAmendmentByIdFilters(eq(Optional.of(activeSubscriptionsRenewed().get(0).getPreviousSubscriptionId())), eq(Optional.empty()), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal))))
                .thenReturn(renewalAmendPayed());
        when(abawebappsExternalService.renewPaymentsZuora(anyList())).thenReturn(Arrays.asList(
                SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                        .originalSubscriptionId(subscriptionId)
                        .success(true)
                        .build()));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.empty()));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedRenewPaymentRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).renewPaymentsZuora(expectedRenewPaymentRequestAux.capture());
        List<RenewPaymentRequest> expectedRenewPaymentRequest = (List<RenewPaymentRequest>) expectedRenewPaymentRequestAux.getValue();
        Assert.assertEquals(invoiceProcessedFromRenewalInfo().getId(), expectedRenewPaymentRequest.get(0).getInvoiceId());
        Assert.assertEquals(invoiceProcessedFromRenewalInfo().getInvoiceNumber(), expectedRenewPaymentRequest.get(0).getInvoiceNumber());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getTermEndDate(), expectedRenewPaymentRequest.get(0).getNextRenewalDate());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getOriginalId(), expectedRenewPaymentRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(paymentProcessedFromRenewal().getEffectiveDate(), expectedRenewPaymentRequest.get(0).getPaymentDate());
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaReturnErrors() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromRenewalInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromRenewalInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(activeSubscriptionsRenewed());
        when(zuoraExternalService.getAmendmentByIdFilters(eq(Optional.of(activeSubscriptionsRenewed().get(0).getPreviousSubscriptionId())), eq(Optional.empty()), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal))))
                .thenReturn(renewalAmendPayed());

        when(abawebappsExternalService.renewPaymentsZuora(anyList())).thenReturn(Arrays.asList(
                SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                        .originalSubscriptionId(subscriptionId)
                        .success(false)
                        .warnings("Errors")
                        .build()));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Errors")));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedRenewPaymentRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).renewPaymentsZuora(expectedRenewPaymentRequestAux.capture());
        List<RenewPaymentRequest> expectedRenewPaymentRequest = (List<RenewPaymentRequest>) expectedRenewPaymentRequestAux.getValue();
        Assert.assertEquals(invoiceProcessedFromRenewalInfo().getId(), expectedRenewPaymentRequest.get(0).getInvoiceId());
        Assert.assertEquals(invoiceProcessedFromRenewalInfo().getInvoiceNumber(), expectedRenewPaymentRequest.get(0).getInvoiceNumber());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getTermEndDate(), expectedRenewPaymentRequest.get(0).getNextRenewalDate());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getOriginalId(), expectedRenewPaymentRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(paymentProcessedFromRenewal().getEffectiveDate(), expectedRenewPaymentRequest.get(0).getPaymentDate());
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorPaymentNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_paymentNotFound() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.empty());

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Payment with id '2c92c09559b0489c0159cb955aed2fc8' not found in Zuora")));

        /*
            check there aren't more calls
         */
        verify(zuoraExternalService, never()).getInvoicePayment(any());
        verify(zuoraExternalService, never()).getInvoiceItemsInfo(any());
        verify(zuoraExternalService, never()).getInvoiceInfo(any());
        verify(zuoraExternalService, never()).getSubscriptionsInfoById(anyList(), any());
        verify(zuoraExternalService, never()).getAmendmentByIdFilters(any(), any(), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal)));
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorInvoicePaymentNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_invoicePaymentNotFound() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(Arrays.asList()); //empty list

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("InvoicePayment with payment id '2c92c09559b0489c0159cb955aed2fc8' not found in Zuora")));

         /*
            check there aren't more calls
         */
        verify(zuoraExternalService, never()).getInvoiceItemsInfo(any());
        verify(zuoraExternalService, never()).getInvoiceInfo(any());
        verify(zuoraExternalService, never()).getSubscriptionsInfoById(anyList(), any());
        verify(zuoraExternalService, never()).getAmendmentByIdFilters(any(), any(), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal)));
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorTooManyInvoicePayments.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_invoicePaymentTooManyResults() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(Arrays.asList(invoicePaymentsProcessedFromRenewal().get(0), invoicePaymentsProcessedFromRenewal().get(0)));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Invalid invoice results")));

         /*
            check there aren't more calls
         */
        verify(zuoraExternalService, never()).getInvoiceItemsInfo(any());
        verify(zuoraExternalService, never()).getInvoiceInfo(any());
        verify(zuoraExternalService, never()).getSubscriptionsInfoById(anyList(), any());
        verify(zuoraExternalService, never()).getAmendmentByIdFilters(any(), any(), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal)));
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorInvoiceNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_invoiceNotFound() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(Arrays.asList()); //empty list

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("InvoiceItemsInfo with invoiceId '2c92c09459b02c5e0159cb9470b957e2' not found in Zuora")));

         /*
            check there aren't more calls
         */
        verify(zuoraExternalService, never()).getInvoiceInfo(any());
        verify(zuoraExternalService, never()).getSubscriptionsInfoById(anyList(), any());
        verify(zuoraExternalService, never()).getAmendmentByIdFilters(any(), any(), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal)));
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorInvoiceInfoNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_invoiceInfoNotFound() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(null);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("InvoiceInfo with id '2c92c09459b02c5e0159cb9470b957e2' not found in Zuora for payment '2c92c09559b0489c0159cb955aed2fc8'")));

         /*
            check there aren't more calls
         */
        verify(zuoraExternalService, never()).getSubscriptionsInfoById(anyList(), any());
        verify(zuoraExternalService, never()).getAmendmentByIdFilters(any(), any(), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal)));
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorSubscriptionNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_subscriptionNotFound() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromRenewalInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromRenewalInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(Arrays.asList());
        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Active subscription with id '2c92c08659b048b70159caa0d87d0e10' not found in Zuora")));

         /*
            check there aren't more calls
         */
        verify(zuoraExternalService, never()).getAmendmentByIdFilters(any(), any(), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal)));
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorTooManySubscriptions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_subscriptionTooMany() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromRenewalInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromRenewalInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(Arrays.asList(activeSubscriptionsRenewed().get(0), activeSubscriptionsRenewed().get(0)));
        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot register renewal payment on subscription.")));

         /*
            check there aren't more calls
         */
        verify(zuoraExternalService, never()).getAmendmentByIdFilters(any(), any(), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal)));
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorAmendNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_amend() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromRenewalInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromRenewalInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(activeSubscriptionsRenewed());
        when(zuoraExternalService.getAmendmentByIdFilters(eq(Optional.of(activeSubscriptionsRenewed().get(0).getPreviousSubscriptionId())), eq(Optional.empty()), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal))))
                .thenReturn(Arrays.asList());//empty list

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("The subscription id '2c92c08559261bf601592ab2b665658e', has no 'Renewal' amend related")));

         /*
            check the arguments sent to aba are valid
         */
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorAmendStatusInvalid.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_amendInvalidStatus() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromRenewalInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromRenewalInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(activeSubscriptionsRenewed());
        ZuoraAmendmentResponse amendInvalidStatus = renewalAmendPayed().get(0);
        amendInvalidStatus.setStatus("Processing");
        when(zuoraExternalService.getAmendmentByIdFilters(eq(Optional.of(activeSubscriptionsRenewed().get(0).getPreviousSubscriptionId())), eq(Optional.empty()), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal))))
                .thenReturn(Arrays.asList(amendInvalidStatus));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Invalid amend results.")));

         /*
            check the arguments sent to aba are valid
         */
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorTooManyAmends.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_amendTooMany() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromRenewalInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromRenewalInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(activeSubscriptionsRenewed());
        when(zuoraExternalService.getAmendmentByIdFilters(eq(Optional.of(activeSubscriptionsRenewed().get(0).getPreviousSubscriptionId())), eq(Optional.empty()), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal))))
                .thenReturn(Arrays.asList(renewalAmendPayed().get(0), renewalAmendPayed().get(0)));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Invalid amend results.")));

         /*
            check the arguments sent to aba are valid
         */
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_errorUnexpectedFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaFails() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromRenewal().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromRenewalInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromRenewal()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromRenewal());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromRenewalInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromRenewalInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(activeSubscriptionsRenewed());
        when(zuoraExternalService.getAmendmentByIdFilters(eq(Optional.of(activeSubscriptionsRenewed().get(0).getPreviousSubscriptionId())), eq(Optional.empty()), eq(Optional.of(ZuoraExternalServiceSoap.ZAmendmentTypes.Renewal))))
                .thenReturn(renewalAmendPayed());

        NotFoundExternalException ex = new NotFoundExternalException(new Exception("Unexpected error"));
        when(abawebappsExternalService.renewPaymentsZuora(anyList())).thenThrow(ex);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot send billing information about the renewal to Aba")));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedRenewPaymentRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).renewPaymentsZuora(expectedRenewPaymentRequestAux.capture());
        List<RenewPaymentRequest> expectedRenewPaymentRequest = (List<RenewPaymentRequest>) expectedRenewPaymentRequestAux.getValue();
        Assert.assertEquals(invoiceProcessedFromRenewalInfo().getId(), expectedRenewPaymentRequest.get(0).getInvoiceId());
        Assert.assertEquals(invoiceProcessedFromRenewalInfo().getInvoiceNumber(), expectedRenewPaymentRequest.get(0).getInvoiceNumber());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getTermEndDate(), expectedRenewPaymentRequest.get(0).getNextRenewalDate());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getOriginalId(), expectedRenewPaymentRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(paymentProcessedFromRenewal().getEffectiveDate(), expectedRenewPaymentRequest.get(0).getPaymentDate());
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-processed/new_notification_payment_processed_paymentWrongOrigin.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentNotFromRenewal() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedFromNewSubscriptionReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = invoicePaymentsProcessedFromNewSubscription().get(0).getInvoiceId();
        final String subscriptionId = invoiceItemsProcessedFromNewSubscriptionInfo().get(0).getSubscriptionId();

        // mock notification service external calls
        when(zuoraExternalService.getPaymentProcessedById(eq(paymentId))).thenReturn(Optional.of(paymentProcessedFromNewSubscription()));
        when(zuoraExternalService.getInvoicePayment(eq(paymentId))).thenReturn(invoicePaymentsProcessedFromNewSubscription());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(invoiceId))).thenReturn(invoiceItemsProcessedFromNewSubscriptionInfo());
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(invoiceProcessedFromNewSubscriptionInfo());
        when(zuoraExternalService.getSubscriptionsInfoById(eq(Arrays.asList(subscriptionId)), eq(Optional.of("Active")))).thenReturn(activeSubscriptionsCreated());

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.of("The payment id is not from a renewal subscription.")));;

         /*
            check the arguments sent to aba are valid
         */
        verify(zuoraExternalService, never()).getAmendmentByIdFilters(any(),any(), any());
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "../../base/zuora-notifications.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentProcessedAlreadyRegistered() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentProcessedAlreadyExistsServiceRequest();

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.of("This notification was already processed.")));;

         /*
            check the arguments sent to aba are valid
         */
        verifyZeroInteractions(abawebappsExternalService, zuoraExternalService);
    }
}
