package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.data.repository.dataset.CustomReplacementDataSetLoader;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceItemsInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoicePayment;
import com.abaenglish.external.zuora.soap.domain.subscription.ZuoraSubscriptionResponse;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.dto.CancelSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.zuora.api.axis2.UnexpectedErrorFault;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.*;

import static com.abaenglish.subscription.objectmother.NotificationObjectMother.*;
import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.failCancelAmendResults;
import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.successCancelAmendResults;
import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = CustomReplacementDataSetLoader.class)
@DatabaseSetup(value = "../../base/zuora-notifications.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../../base/zuora-notifications-empty.xml")
public class NotificationControllerPaymentDeclinedIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        reset(zuoraExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-declined/new_notification_payment_declined.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentDeclined() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentDeclinedReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = "2c92c0f95876a6bd0158775b62602b15";
        final String subscriptionId = "2c92c0f95876a6bd0158775b62602b16";

        // mock notification service external calls
        final List<InvoicePayment> invoicePayments = Arrays.asList(
                InvoicePayment.Builder.anInvoicePayment().paymentId(paymentId).invoiceId(invoiceId).build());
        when(zuoraExternalService
                .getInvoicePayment(eq(paymentId)))
                .thenReturn(invoicePayments);
        final List<InvoiceItemsInfo> invoiceItemsInfos = Arrays.asList(InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                .invoiceId(invoiceId)
                .subscriptionId(subscriptionId)
                .build());
        when(zuoraExternalService
                .getInvoiceItemsInfo(eq(invoiceId)))
                .thenReturn(invoiceItemsInfos);

        // mock subscription service external calls
        when(zuoraExternalService
                .getSubscriptionInfoById(eq(subscriptionId), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE)))
                .thenReturn(Arrays.asList(
                        ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse()
                                .accountId(serviceRequest.getAccountId())
                                .termStartDate("2016-11-01")
                                .termEndDate("2016-12-01")
                                .id(subscriptionId)
                                .build()));

        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().dueDate(new Date()).build());

        when(zuoraExternalService
                .cancelSubscription(any(CancelSubscriptionRequest.class)))
                .thenReturn(successCancelAmendResults());

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.empty()));
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-declined/new_notification_payment_declined_errorSubscriptionNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentDeclined_CancelSubscriptionNotFoundSubscription() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentDeclinedReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = "2c92c0f95876a6bd0158775b62602b15";
        final String subscriptionId = "2c92c0f95876a6bd0158775b62602b16";

        reset(zuoraExternalService);
        // mock notification service external calls
        final List<InvoicePayment> invoicePayments = Arrays.asList(
                InvoicePayment.Builder.anInvoicePayment().paymentId(paymentId).invoiceId(invoiceId).build());
        when(zuoraExternalService
                .getInvoicePayment(eq(paymentId)))
                .thenReturn(invoicePayments);
        final List<InvoiceItemsInfo> invoiceItemsInfos = Arrays.asList(InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                .invoiceId(invoiceId)
                .subscriptionId(subscriptionId)
                .build());
        when(zuoraExternalService
                .getInvoiceItemsInfo(eq(invoiceId)))
                .thenReturn(invoiceItemsInfos);

        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().dueDate(new Date()).build());
        // mock subscription service external calls
        when(zuoraExternalService.getSubscriptionInfoById(eq(subscriptionId), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenReturn(new ArrayList(0));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Subscription not found")));

        verify(zuoraExternalService, never()).cancelSubscription(any(CancelSubscriptionRequest.class));
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-declined/new_notification_payment_declined_errorTooManySubscriptions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentDeclined_CancelSubscriptionIsNotUnique() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentDeclinedReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = "2c92c0f95876a6bd0158775b62602b15";
        final String subscriptionId = "2c92c0f95876a6bd0158775b62602b16";

        reset(zuoraExternalService);
        // mock notification service external calls
        final List<InvoicePayment> invoicePayments = Arrays.asList(
                InvoicePayment.Builder.anInvoicePayment().paymentId(paymentId).invoiceId(invoiceId).build());
        when(zuoraExternalService
                .getInvoicePayment(eq(paymentId)))
                .thenReturn(invoicePayments);
        final List<InvoiceItemsInfo> invoiceItemsInfos = Arrays.asList(InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                .invoiceId(invoiceId)
                .subscriptionId(subscriptionId)
                .build());
        when(zuoraExternalService
                .getInvoiceItemsInfo(eq(invoiceId)))
                .thenReturn(invoiceItemsInfos);
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().dueDate(new Date()).build());
        // mock subscription service external calls
        final ZuoraSubscriptionResponse subscription = ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id(subscriptionId).build();
        final List<ZuoraSubscriptionResponse> duplicatedSubscriptions = Arrays.asList(subscription, subscription);
        when(zuoraExternalService.getSubscriptionInfoById(eq(subscriptionId), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenReturn(duplicatedSubscriptions);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Too many subscriptions found for this id")));

        verify(zuoraExternalService, never()).cancelSubscription(any(CancelSubscriptionRequest.class));
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-declined/new_notification_payment_declined_errorExternal.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentDeclined_CancelSubscriptionZuoraThrowException() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentDeclinedReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = "2c92c0f95876a6bd0158775b62602b15";
        final String subscriptionId = "2c92c0f95876a6bd0158775b62602b16";

        reset(zuoraExternalService);
        // mock notification service external calls
        final List<InvoicePayment> invoicePayments = Arrays.asList(
                InvoicePayment.Builder.anInvoicePayment().paymentId(paymentId).invoiceId(invoiceId).build());
        when(zuoraExternalService
                .getInvoicePayment(eq(paymentId)))
                .thenReturn(invoicePayments);
        final List<InvoiceItemsInfo> invoiceItemsInfos = Arrays.asList(InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                .invoiceId(invoiceId)
                .subscriptionId(subscriptionId)
                .build());
        when(zuoraExternalService
                .getInvoiceItemsInfo(eq(invoiceId)))
                .thenReturn(invoiceItemsInfos);
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().dueDate(new Date()).build());
        // mock subscription service external calls
        when(zuoraExternalService.getSubscriptionInfoById(eq(subscriptionId), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE)))
                .thenReturn(Arrays.asList(ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id(subscriptionId).build()));
        when(zuoraExternalService.cancelSubscription(any(CancelSubscriptionRequest.class)))
                .thenThrow(UnexpectedErrorFault.class);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot cancel the subscription")));
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-declined/new_notification_payment_declined_zuoraErrors.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentDeclined_cancelSubscriptionZuoraReturnErrors() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentDeclinedReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = "2c92c0f95876a6bd0158775b62602b15";
        final String subscriptionId = "2c92c0f95876a6bd0158775b62602b16";

        reset(zuoraExternalService);

        // mock notification service external calls
        final List<InvoicePayment> invoicePayments = Arrays.asList(
                InvoicePayment.Builder.anInvoicePayment().paymentId(paymentId).invoiceId(invoiceId).build());
        when(zuoraExternalService
                .getInvoicePayment(eq(paymentId)))
                .thenReturn(invoicePayments);
        final List<InvoiceItemsInfo> invoiceItemsInfos = Arrays.asList(InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                .invoiceId(invoiceId)
                .subscriptionId(subscriptionId)
                .build());
        when(zuoraExternalService
                .getInvoiceItemsInfo(eq(invoiceId)))
                .thenReturn(invoiceItemsInfos);
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().dueDate(new Date()).build());
        // mock subscription service external calls
        when(zuoraExternalService.getSubscriptionInfoById(eq(subscriptionId), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE)))
                .thenReturn(Arrays.asList(ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id(subscriptionId).build()));
        when(zuoraExternalService.cancelSubscription(any(CancelSubscriptionRequest.class)))
                .thenReturn(failCancelAmendResults());

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot cancel the subscription: xxx-INVALID_FIELD-Invalid field")));
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-declined/new_notification_payment_declined_errorSubscriptionNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentDeclined_InvoiceWithMultipleSubscriptions() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentDeclinedReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = "2c92c0f95876a6bd0158775b62602b15";
        final String subscriptionId = "2c92c0f95876a6bd0158775b62602b16";

        reset(zuoraExternalService);
        // mock notification service external calls
        final List<InvoicePayment> invoicePayments = Arrays.asList(
                InvoicePayment.Builder.anInvoicePayment().paymentId(paymentId).invoiceId(invoiceId).build());
        when(zuoraExternalService
                .getInvoicePayment(eq(paymentId)))
                .thenReturn(invoicePayments);
        final List<InvoiceItemsInfo> invoiceItemsInfos = Arrays.asList(
                InvoiceItemsInfo.Builder.anInvoiceItemsInfo().invoiceId(invoiceId).subscriptionId(subscriptionId).build(),
                InvoiceItemsInfo.Builder.anInvoiceItemsInfo().invoiceId(invoiceId).subscriptionId(subscriptionId).build());
        when(zuoraExternalService
                .getInvoiceItemsInfo(eq(invoiceId)))
                .thenReturn(invoiceItemsInfos);
        when(zuoraExternalService.getInvoiceInfo(eq(invoiceId))).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().dueDate(new Date()).build());

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Subscription not found")));
        verify(zuoraExternalService, never()).cancelSubscription(any(CancelSubscriptionRequest.class));
    }

    @Test
    @ExpectedDatabase(value = "expected/payment-declined/new_notification_payment_declined_errorTooManyInvoices.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentDeclined_TooMuchInvoices() throws Exception {
        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentDeclinedReceivedServiceRequest();
        final String paymentId = serviceRequest.getPaymentId();
        final String invoiceId = "2c92c0f95876a6bd0158775b62602b15";

        reset(zuoraExternalService);
        // mock notification service external calls
        final List<InvoicePayment> invoicePayments = Arrays.asList(
                InvoicePayment.Builder.anInvoicePayment().paymentId(paymentId).invoiceId(invoiceId).build(),
                InvoicePayment.Builder.anInvoicePayment().paymentId(paymentId).invoiceId(invoiceId).build());
        when(zuoraExternalService
                .getInvoicePayment(eq(paymentId)))
                .thenReturn(invoicePayments);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Too much invoices found for payment")));
        verify(zuoraExternalService, never()).cancelSubscription(any(CancelSubscriptionRequest.class));
    }

    @Test
    @ExpectedDatabase(value = "../../base/zuora-notifications.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotificationPaymentDeclinedAlreadyRegistered() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationPaymentDeclinedAlreadyProcessedServiceRequest();

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/payment-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.of("This notification was already processed.")));;

         /*
            check the arguments sent to aba are valid
         */
        verifyZeroInteractions(zuoraExternalService);
    }
}
