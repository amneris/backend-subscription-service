package com.abaenglish.subscription.controller.v1;

import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.external.abawebapps.service.impl.AbawebappsExternalService;
import com.abaenglish.external.paypal.service.IPaypalExternalService;
import com.abaenglish.external.paypal.service.impl.PaypalExternalService;
import com.abaenglish.external.selligent.service.ISelligentSegmentService;
import com.abaenglish.external.selligent.service.impl.SelligentExternalService;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

public class TestConfiguration {

    @Bean
    @Primary
    public IZuoraExternalServiceSoap zuoraExternalService() {
        return Mockito.mock(ZuoraExternalServiceSoap.class);
    }

    @Bean
    @Primary
    public IAbawebappsExternalService abawebappsExternalService() { return Mockito.mock(AbawebappsExternalService.class); }

    @Bean
    @Primary
    public ISelligentSegmentService selligentExternalService() {
        return Mockito.mock(SelligentExternalService.class);
    }

    @Bean
    @Primary
    public IPaypalExternalService paypalService() {
        return Mockito.mock(PaypalExternalService.class);
    }

    @Bean
    @Primary
    public IUserServiceFeign userServiceFeign() {
        return Mockito.mock(IUserServiceFeign.class);
    }

    @Bean
    @Primary
    public RabbitTemplate rabbitTemplate() { return Mockito.mock(RabbitTemplate.class); }

    @Bean(name = "paymentDeclinedListenerContainerFactory")
    @Primary
    RabbitListenerContainerFactory paymentDeclinedListenerContainerFactory() {
        return null;
    }

    @Bean(name = "rabbitRetryListenerContainerFactory")
    @Primary
    RabbitListenerContainerFactory rabbitRetryListenerContainerFactory() { return null; }
}
