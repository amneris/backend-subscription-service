package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.abawebapps.domain.InsertUserRequest;
import com.abaenglish.external.paypal.domain.PaypalExternalCustomerDetails;
import com.abaenglish.external.paypal.service.IPaypalExternalService;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceItemsInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.ZuoraSubscriptionResponse;
import com.abaenglish.external.zuora.soap.service.dto.CreateSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.controller.v1.dto.transaction.CreatePaypalTransactionRequest;
import com.abaenglish.subscription.controller.v1.dto.transaction.CreateZuoraTransactionRequest;
import com.abaenglish.subscription.controller.v1.dto.transaction.RestoreTransactionRequest;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.queue.PaymentTransactionEventType;
import com.abaenglish.subscription.queue.event.AbaConfirmedEvent;
import com.abaenglish.subscription.queue.event.StartTransactionEvent;
import com.abaenglish.subscription.queue.event.ZuoraConfirmedEvent;
import com.abaenglish.subscription.service.dto.transaction.TransactionExpand;
import com.abaenglish.subscription.statemachine.Events;
import com.abaenglish.subscription.statemachine.States;
import com.abaenglish.user.controller.v1.dto.response.InsertUserApiResponse;
import com.abaenglish.user.controller.v1.dto.response.UserZuoraApiResponse;
import com.abaenglish.user.controller.v1.dto.response.ZuoraApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;
import com.zuora.api.axis2.UnexpectedErrorFault;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.JsonConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import static com.abaenglish.subscription.objectmother.PaymentTransactionObjectMother.*;
import static com.abaenglish.subscription.objectmother.PaymentTransactionObjectMother.abaConfirmedTransactionCreditCard;
import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.subscribeResults;
import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.userWithoutZuoraAccount;
import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@DatabaseSetup(value = "../../base/transactions.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../../base/transactions-empty.xml")
public class TransactionControllerIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IUserServiceFeign userServiceFeign;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private IPaypalExternalService paypalService;

    private MessageConverter messageConverter = new SimpleMessageConverter();

    @Before
    public void setup() {

        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;

        // clean previous calls
        reset(rabbitTemplate, zuoraExternalService, userServiceFeign);

        //mock converter for rabbit template
        when(rabbitTemplate.getMessageConverter()).thenReturn(messageConverter);
    }

    @Test
    @ExpectedDatabase(value = "expected/new_paypal_payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void createPaypalTransaction() {

        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());

        CreatePaypalTransactionRequest request = createPaypalTransactionRequest();

        ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("type", is(request.getType().name()))
                .expectBody("state", is("OPEN"))
                .expectBody("id", Matchers.notNullValue())
                .expectBody("userId", is(request.getUserId().intValue()))
                .expectBody("period", is(request.getPeriod()))
                .expectBody("subscriptionId", Matchers.nullValue())
                .expectBody("paypalToken", is(request.getPaypalToken()))
                .build();

        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(responseSpec);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.START.getExchange()),
                eq(PaymentTransactionEventType.START.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "expected/new_zuora_payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void createZuoraTransaction() {

        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());

        CreateZuoraTransactionRequest request = createZuoraTransactionRequest();

        ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("type", is(request.getType().name()))
                .expectBody("state", is("OPEN"))
                .expectBody("id", Matchers.notNullValue())
                .expectBody("userId", is(request.getUserId().intValue()))
                .expectBody("period", is(request.getPeriod()))
                .expectBody("subscriptionId", Matchers.nullValue())
                .build();

        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(responseSpec);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.START.getExchange()),
                eq(PaymentTransactionEventType.START.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void createTransactionForUserAlreadyExits() {

        // clean previous calls
        UserZuoraApiResponse userZuora = userWithoutZuoraAccount();
        userZuora.setZuora(ZuoraApiResponse.Builder.aZuoraApiResponse().build());
        when(userServiceFeign.getUser(anyLong())).thenReturn(userZuora);

        CreateZuoraTransactionRequest request = createZuoraTransactionRequest();

        ResponseSpecification responseSpec = new ResponseSpecBuilder().expectStatusCode(409).build();

        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(responseSpec);

        // check event publications
        verify(rabbitTemplate, never()).send(
                eq(PaymentTransactionEventType.START.getExchange()),
                eq(PaymentTransactionEventType.START.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void badRequestOnCreatePaypalTransaction() {

        // prepare bad request for period
        CreatePaypalTransactionRequest request = createPaypalTransactionRequest();
        request.setPeriod(null);

        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "period"));

        // prepare bad request for ratePlans
        request = createPaypalTransactionRequest();
        request.setRatePlans(null);
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "ratePlans"));

        // prepare bad request for user id
        request = createPaypalTransactionRequest();
        request.setUserId(null);
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "userId"));

        // prepare bad request for type
        request = createPaypalTransactionRequest();
        request.setType(null);
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "type"));

        // prepare bad request for type
        request = createPaypalTransactionRequest();
        request.setPaypalToken(null);
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "paypalToken"));

        // prepare bad request for empty form
        request = new CreatePaypalTransactionRequest();
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectFieldsErrors(400, Arrays.asList("period", "ratePlans", "type", "userId", "paypalToken")));

        // check event publications
        verify(rabbitTemplate, never()).send(
                eq(PaymentTransactionEventType.START.getExchange()),
                eq(PaymentTransactionEventType.START.getRoutingKey()),
                any(Message.class));

    }

    @Test
    @ExpectedDatabase(value = "../../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void badRequestOnCreateZuoraTransaction()  {

        // prepare bad request for period
        CreateZuoraTransactionRequest request = createZuoraTransactionRequest();
        request.setPeriod(null);

        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "period"));

        // prepare bad request for ratePlans
        request = createZuoraTransactionRequest();
        request.setRatePlans(null);
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "ratePlans"));

        // prepare bad request for user id
        request = createZuoraTransactionRequest();
        request.setUserId(null);
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "userId"));

        // prepare bad request for type
        request = createZuoraTransactionRequest();
        request.setType(null);
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "type"));

        // prepare bad request for zuoraRefId
        request = createZuoraTransactionRequest();
        request.setZuoraRefId("");
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectSingleFieldError(400, "zuoraRefId"));

        // prepare bad request for empty form
        request = new CreateZuoraTransactionRequest();
        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/transactions").
                then().
                spec(expectFieldsErrors(400, Arrays.asList("period", "ratePlans", "type", "userId", "zuoraRefId")));

        // check event publications
        verify(rabbitTemplate, never()).send(
                eq(PaymentTransactionEventType.START.getExchange()),
                eq(PaymentTransactionEventType.START.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void getOpenTransactionCreditCardWithoutExpand() {
        final ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("id", is(openTransactionCreditCard().getPublicId()))
                .expectBody("state", is(openTransactionCreditCard().getState().name()))
                .expectBody("userId", is(openTransactionCreditCard().getUserId().intValue()))
                .expectBody("type", is(openTransactionCreditCard().getType()))
                .expectBody("subscriptionId", is(openTransactionCreditCard().getSubscriptionIdZuora()))
                .expectBody("period", is(openTransactionCreditCard().getPeriod()))
                .expectBody("paypalToken", Matchers.nullValue())
                .build();

        given().
                contentType(ContentType.JSON).
                when().
                pathParam("userId", openTransactionCreditCard().getPublicId()).
                get("/api/v1/transactions/{userId}").
                then().
                spec(responseSpec);

    }

    @Test
    @ExpectedDatabase(value = "../../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void getAbaConfirmedTransactionCreditCardWithExpandWithDicount() {

        // mock external calls
        final List<ZuoraSubscriptionResponse> subscriptionInfoResponse = Arrays.asList(ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().termEndDate("2017-10-04").build());
        when(zuoraExternalService.getSubscriptionInfoById(eq(abaConfirmedTransactionCreditCard().getSubscriptionIdZuora()), any())).thenReturn(subscriptionInfoResponse);

        final List<InvoiceItemsInfo> invoiceItemsInfo = Arrays.asList(
                InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                        .chargeAmount(BigDecimal.valueOf(49.58))
                        .quantity(1)
                        .taxAmount(BigDecimal.valueOf(10.41)).build(),
                InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                        .chargeAmount(BigDecimal.valueOf(-9.92))
                        .quantity(1)
                        .taxAmount(BigDecimal.valueOf(-2.08)).build());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(abaConfirmedTransactionCreditCard().getInvoiceIdZuora()))).thenReturn(invoiceItemsInfo);
        when(zuoraExternalService.getInvoiceInfo(eq(abaConfirmedTransactionCreditCard().getInvoiceIdZuora()))).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().amount(BigDecimal.valueOf(47.99)).build());

        final ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("id", is(abaConfirmedTransactionCreditCard().getPublicId()))
                .expectBody("state", is(abaConfirmedTransactionCreditCard().getState().name()))
                .expectBody("userId", is(abaConfirmedTransactionCreditCard().getUserId().intValue()))
                .expectBody("type", is(abaConfirmedTransactionCreditCard().getType()))
                .expectBody("subscriptionId", is(abaConfirmedTransactionCreditCard().getSubscriptionNumberZuora()))
                .expectBody("period", is(abaConfirmedTransactionCreditCard().getPeriod()))
                .expectBody("dueDate", is(subscriptionInfoResponse.get(0).getTermEndDate()))
                .expectBody("invoice.finalPrice", Matchers.notNullValue())
                .expectBody("invoice.basePrice", Matchers.notNullValue())
                .expectBody("invoice.discountPrice", Matchers.notNullValue())
                .build();

        given().
                config(RestAssuredConfig.newConfig().jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))).
                contentType(ContentType.JSON).
                when().
                pathParam("publicId", abaConfirmedTransactionCreditCard().getPublicId()).
                params("expand", String.join(",",TransactionExpand.DUE_DATE.getParamName(), TransactionExpand.INVOICE.getParamName())).
                get("/api/v1/transactions/{publicId}").
                then().
                spec(responseSpec);

        // check expand options called
        verify(zuoraExternalService).getSubscriptionInfoById(eq(abaConfirmedTransactionCreditCard().getSubscriptionIdZuora()), any());
        verify(zuoraExternalService).getInvoiceItemsInfo(eq(abaConfirmedTransactionCreditCard().getInvoiceIdZuora()));
    }

    @Test
    @ExpectedDatabase(value = "../../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void getAbaConfirmedTransactionCreditCardWithExpandWithoutDiscount() {
        // mock external calls
        final List<ZuoraSubscriptionResponse> subscriptionInfoResponse = Arrays.asList(ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().termEndDate("2017-10-04").build());
        when(zuoraExternalService.getSubscriptionInfoById(eq(abaConfirmedTransactionCreditCard().getSubscriptionIdZuora()), any())).thenReturn(subscriptionInfoResponse);

        final List<InvoiceItemsInfo> invoiceItemsInfo = Arrays.asList(
                InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                        .chargeAmount(BigDecimal.valueOf(49))
                        .quantity(1)
                        .taxAmount(BigDecimal.valueOf(10)).build());
        when(zuoraExternalService.getInvoiceItemsInfo(eq(abaConfirmedTransactionCreditCard().getInvoiceIdZuora()))).thenReturn(invoiceItemsInfo);
        when(zuoraExternalService.getInvoiceInfo(eq(abaConfirmedTransactionCreditCard().getInvoiceIdZuora()))).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().amount(BigDecimal.valueOf(59)).build());

        final ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("id", is(abaConfirmedTransactionCreditCard().getPublicId()))
                .expectBody("state", is(abaConfirmedTransactionCreditCard().getState().name()))
                .expectBody("userId", is(abaConfirmedTransactionCreditCard().getUserId().intValue()))
                .expectBody("type", is(abaConfirmedTransactionCreditCard().getType()))
                .expectBody("subscriptionId", is(abaConfirmedTransactionCreditCard().getSubscriptionNumberZuora()))
                .expectBody("period", is(abaConfirmedTransactionCreditCard().getPeriod()))
                .expectBody("dueDate", is(subscriptionInfoResponse.get(0).getTermEndDate()))
                .expectBody("invoice.finalPrice", Matchers.notNullValue())
                .expectBody("invoice.basePrice", Matchers.notNullValue())
                .expectBody("invoice.discountPrice", Matchers.nullValue())
                .build();

        given().
                config(RestAssuredConfig.newConfig().jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))).
                contentType(ContentType.JSON).
                when().
                pathParam("publicId", abaConfirmedTransactionCreditCard().getPublicId()).
                params("expand", String.join(",",TransactionExpand.DUE_DATE.getParamName(), TransactionExpand.INVOICE.getParamName())).
                get("/api/v1/transactions/{publicId}").
                then().
                spec(responseSpec);

        // check expand options called
        verify(zuoraExternalService).getSubscriptionInfoById(eq(abaConfirmedTransactionCreditCard().getSubscriptionIdZuora()), any());
        verify(zuoraExternalService).getInvoiceItemsInfo(eq(abaConfirmedTransactionCreditCard().getInvoiceIdZuora()));
    }

    private ResponseSpecification expectFieldsErrors(final Integer statusCode, List<String> expectedFieldsError) {
        final ResponseSpecBuilder spec = new ResponseSpecBuilder().expectStatusCode(statusCode);
        if (expectedFieldsError != null) {
            expectedFieldsError.stream().forEach(field -> spec.expectBody("errors.field", Matchers.hasItem(field)));
        }
        return spec.build();
    }

    private ResponseSpecification expectSingleFieldError(final Integer statusCode, String expectedFieldError) {
        final ResponseSpecBuilder spec = new ResponseSpecBuilder().expectStatusCode(statusCode);
        if (expectedFieldError != null) {
            spec.expectBody("errors.field[0]", Matchers.equalTo(expectedFieldError));
        }
        return spec.build();
    }

    @Test
    @DatabaseSetup(value = "transaction-error.xml", type = DatabaseOperation.UPDATE)
    @ExpectedDatabase(value = "expected/restore-to-open-default-transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void restoreDefaultTransactionProccessZuoraToOpen() throws UnexpectedErrorFault, RemoteException {

        //mock external calls
        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.createSubscription(any(CreateSubscriptionRequest.class))).thenReturn(subscribeResults());

        PaymentTransaction paymentTransactionToRestore = zuoraConfirmedTransactionCreditCard();

        RestoreTransactionRequest request = RestoreTransactionRequest.Builder.aRestoreTransactionRequest()
                .event(Events.START_PROCESS.name())
                .state(States.OPEN.name())
                .build();

        ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("type", is(paymentTransactionToRestore.getType()))
                .expectBody("state", is(States.PROCESS_ZUORA.name()))
                .expectBody("id", is(paymentTransactionToRestore.getPublicId()))
                .expectBody("userId", is(paymentTransactionToRestore.getUserId().intValue()))
                .expectBody("period", is(paymentTransactionToRestore.getPeriod()))
                .expectBody("subscriptionId", is(subscribeResults()[0].getSubscriptionNumber()))
                .build();

        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                put("/api/v1/transactions/" + paymentTransactionToRestore.getPublicId()).
                then().
                spec(responseSpec);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @DatabaseSetup(value = "transaction-error.xml", type = DatabaseOperation.UPDATE)
    @ExpectedDatabase(value = "expected/restore-to-open-paypal-transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void restorePaypalTransactionVerifyPaypalToOpen() throws UnexpectedErrorFault, RemoteException, ServiceException {

        //mock external calls
        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.createSubscription(any(CreateSubscriptionRequest.class))).thenReturn(subscribeResults());
        when(paypalService.getExpressCheckoutDetails(anyString())).thenReturn(PaypalExternalCustomerDetails.Builder.aPaypalCustomerDetails().payerEmail(userWithoutZuoraAccount().getUser().getEmail()).build());
        when(paypalService.createBillingAgreement(anyString())).thenReturn("test-BAID");

        PaymentTransaction paymentTransactionToRestore = zuoraConfirmedTransactionPayPal();

        RestoreTransactionRequest request = RestoreTransactionRequest.Builder.aRestoreTransactionRequest()
                .event(Events.START_PROCESS.name())
                .state(States.OPEN.name())
                .build();

        ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("type", is(paymentTransactionToRestore.getType()))
                .expectBody("state", is(States.PROCESS_ZUORA.name()))
                .expectBody("id", is(paymentTransactionToRestore.getPublicId()))
                .expectBody("userId", is(paymentTransactionToRestore.getUserId().intValue()))
                .expectBody("period", is(paymentTransactionToRestore.getPeriod()))
                .expectBody("subscriptionId", is(paymentTransactionToRestore.getSubscriptionIdZuora()))
                .expectBody("paypalToken", is(paymentTransactionToRestore.getPaypalToken()))
                .build();

        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                put("/api/v1/transactions/" + paymentTransactionToRestore.getPublicId()).
                then().
                spec(responseSpec);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @DatabaseSetup(value = "transaction-error.xml", type = DatabaseOperation.UPDATE)
    @ExpectedDatabase(value = "expected/restore-to-processZuora-default-transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void restoreDefaultTransactionProccessAbaToProccesZuora() throws UnexpectedErrorFault, RemoteException {

        PaymentTransaction paymentTransactionToRestore = abaConfirmedTransactionCreditCard();

        //mock external calls
        when(userServiceFeign.getUser(eq(paymentTransactionToRestore.getUserId()))).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.getInvoiceInfo(anyString())).thenReturn(InvoiceInfo.Builder.anInvoiceInfo()
                .amount(paymentTransactionToRestore.getPrice())
                .invoiceNumber(paymentTransactionToRestore.getInvoiceIdZuora())
                .build());
        when(zuoraExternalService.getSubscriptionInfoById(anyString(), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenReturn(Arrays.asList(
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("1").accountId("1").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build(),
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("2").accountId("2").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build()));
        when(userServiceFeign.insertUser(any(InsertUserRequest.class))).thenReturn(InsertUserApiResponse.Builder.anInsertUserApiResponse().build());


        RestoreTransactionRequest request = RestoreTransactionRequest.Builder.aRestoreTransactionRequest()
                .event(Events.CONFIRMED_ZUORA.name())
                .state(States.PROCESS_ZUORA.name())
                .build();

        ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectBody("type", is(paymentTransactionToRestore.getType()))
                .expectBody("state", is(States.PROCESS_ABA.name()))
                .expectBody("id", is(paymentTransactionToRestore.getPublicId()))
                .expectBody("userId", is(paymentTransactionToRestore.getUserId().intValue()))
                .expectBody("period", is(paymentTransactionToRestore.getPeriod()))
                .expectBody("subscriptionId", is(paymentTransactionToRestore.getSubscriptionNumberZuora()))
                .build();

        given().
                contentType(ContentType.JSON).
                body(request).
                when().
                put("/api/v1/transactions/" + paymentTransactionToRestore.getPublicId()).
                then().
                spec(responseSpec);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }
}
