package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.data.repository.dataset.CustomReplacementDataSetLoader;
import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.GoneApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.abawebapps.domain.InsertUserRequest;
import com.abaenglish.external.paypal.domain.PaypalExternalCustomerDetails;
import com.abaenglish.external.paypal.service.IPaypalExternalService;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.dto.CreateSubscriptionRequest;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.zuora.api.axis2.UnexpectedErrorFault;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.JsonConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.specification.ResponseSpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.rmi.RemoteException;

import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = CustomReplacementDataSetLoader.class)
public class SubscriptionControllerIT {

    @Value("${local.server.port}")
    int port;
    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;
    @Autowired
    private IUserServiceFeign userServiceFeign;
    @Autowired
    private IPaypalExternalService paypalService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        resetMocks();
        when(rabbitTemplate.getMessageConverter()).thenReturn(new SimpleMessageConverter());
    }

    private void resetMocks() {
        reset(zuoraExternalService, userServiceFeign, paypalService, rabbitTemplate);
    }

    @Test
    public void createSubscriptionCreditCardNewAccount() throws GoneApiException, UnexpectedErrorFault, RemoteException, ServiceException {

        BigDecimal bigDecimal = new BigDecimal(777.777).setScale(3, RoundingMode.CEILING);

        when(userServiceFeign.getUser(265L)).thenReturn(userWithZuoraAccount());
        when(zuoraExternalService.createSubscription(Matchers.any(CreateSubscriptionRequest.class))).thenReturn(subscribeResults());
        when(zuoraExternalService.getInvoiceAmount(anyString())).thenReturn(bigDecimal);
        when(userServiceFeign.insertUser(Matchers.any(InsertUserRequest.class))).thenReturn(responseInsertUser());

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);
        builder.expectBody("name", is(userWithZuoraAccount().getUser().getName()));
        builder.expectBody("surname", is(userWithZuoraAccount().getUser().getSurname()));
        builder.expectBody("email", is(userWithZuoraAccount().getUser().getEmail()));
        builder.expectBody("currency", is(userWithZuoraAccount().getUser().getCurrency()));
        builder.expectBody("period", is(newSubscriptionCreditCardRequest().getPeriod()));
        builder.expectBody("accountNumber", is(subscribeResults()[0].getAccountNumber()));

        ResponseSpecification responseSpec = builder.build();

        com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionRequest request = newSubscriptionCreditCardRequest();

        given().
                config(RestAssuredConfig.config().jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))).
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/subscriptions").
                then().
                spec(responseSpec);
        verify(paypalService, never()).getExpressCheckoutDetails(anyString());
        verify(paypalService, never()).createBillingAgreement(anyString());
    }

    @Test
    public void createSubscriptionPaypalNewAccount() throws GoneApiException, UnexpectedErrorFault, RemoteException, ServiceException {

        BigDecimal bigDecimal = new BigDecimal(777.777).setScale(3, RoundingMode.CEILING);

        when(userServiceFeign.getUser(265L)).thenReturn(userWithZuoraAccount());
        when(zuoraExternalService.createSubscription(Matchers.any(CreateSubscriptionRequest.class))).thenReturn(subscribeResults());
        when(zuoraExternalService.getInvoiceAmount(anyString())).thenReturn(bigDecimal);
        when(userServiceFeign.insertUser(Matchers.any(InsertUserRequest.class))).thenReturn(responseInsertUser());
        when(paypalService.getExpressCheckoutDetails(anyString())).thenReturn(PaypalExternalCustomerDetails.Builder.aPaypalCustomerDetails().payerEmail(userWithZuoraAccount().getUser().getEmail()).build());
        when(paypalService.createBillingAgreement(anyString())).thenReturn("test-BAID");

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);
        builder.expectBody("name", is(userWithZuoraAccount().getUser().getName()));
        builder.expectBody("surname", is(userWithZuoraAccount().getUser().getSurname()));
        builder.expectBody("email", is(userWithZuoraAccount().getUser().getEmail()));
        builder.expectBody("currency", is(userWithZuoraAccount().getUser().getCurrency()));
        builder.expectBody("period", is(newSubscriptionCreditCardRequest().getPeriod()));
        builder.expectBody("accountNumber", is(subscribeResults()[0].getAccountNumber()));

        ResponseSpecification responseSpec = builder.build();

        com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionRequest request = newSubscriptionPaypalRequest();

        given().
                config(RestAssuredConfig.config().jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))).
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/subscriptions").
                then().
                spec(responseSpec);
    }

    @Test
    public void createBadRequestSubscription() throws  UnexpectedErrorFault, RemoteException, ApiException {

        when(userServiceFeign.getUser(265L)).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.getInvoiceAmount(anyString())).thenReturn(new BigDecimal(777.777).setScale(3, RoundingMode.CEILING));
        when(zuoraExternalService.createSubscription(Matchers.any(CreateSubscriptionRequest.class))).
                thenThrow(UnexpectedErrorFault.class);

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(400);
        builder.expectBody("status", is(400));
        builder.expectBody("abaCode", is("SUB0005"));

        ResponseSpecification responseSpec = builder.build();

        com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionRequest request = newSubscriptionCreditCardRequest();

        given().
                config(RestAssuredConfig.config().jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))).
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/subscriptions").
                then().
                spec(responseSpec);

    }

    @Test
    public void createBadResponseSubscription() throws UnexpectedErrorFault, RemoteException {

        when(userServiceFeign.getUser(265L)).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.createSubscription(Matchers.any(CreateSubscriptionRequest.class))).thenReturn(badSubscribeResults());
        when(zuoraExternalService.getInvoiceAmount(anyString())).thenReturn(new BigDecimal(777.777).setScale(3, RoundingMode.CEILING));
        when(userServiceFeign.insertUser(Matchers.any(InsertUserRequest.class))).thenReturn(responseInsertUser());

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(400);
        builder.expectBody("status", is(400));
        builder.expectBody("abaCode", is("SUB0006"));
        builder.expectBody("message", is("Internal Zuora error when try subscribe"));

        ResponseSpecification responseSpec = builder.build();

        com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionRequest request = newSubscriptionCreditCardRequest();

        given().
                config(RestAssuredConfig.config().jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))).
                contentType(ContentType.JSON).
                body(request).
                when().
                post("/api/v1/subscriptions").
                then().
                spec(responseSpec);

    }
}
