package com.abaenglish.subscription.controller.v1;

import com.abaenglish.boot.data.repository.dataset.CustomReplacementDataSetLoader;
import com.abaenglish.external.abawebapps.domain.RenewSubscriptionsRequest;
import com.abaenglish.external.abawebapps.domain.SubscriptionInfoResponse;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.service.impl.AbawebappsExternalService;
import com.abaenglish.external.zuora.soap.domain.subscription.ZuoraSubscriptionResponse;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;
import com.abaenglish.external.zuora.soap.service.impl.SubscriptionStatus;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.abaenglish.subscription.objectmother.NotificationObjectMother.*;
import static com.abaenglish.subscription.objectmother.ZuoraObjectMother.activeSubscriptionsRenewed;
import static com.abaenglish.subscription.objectmother.ZuoraObjectMother.renewalAmendPayed;
import static io.restassured.RestAssured.given;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = CustomReplacementDataSetLoader.class)
@DatabaseSetup(value = "../../base/zuora-notifications.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../../base/zuora-notifications-empty.xml")
public class NotificationControllerRenewalAmendmentProcessedIT {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private AbawebappsExternalService abawebappsExternalService;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        reset(zuoraExternalService, abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(renewalAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.ACTIVE))))
                .thenReturn(activeSubscriptionsRenewed());

        when(abawebappsExternalService.renewSubscriptionsZuora(anyList())).thenReturn(Arrays.asList(
                SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                        .originalSubscriptionId(serviceRequest.getSubscriptionOriginalId())
                        .success(true)
                        .build()));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(true, Optional.empty()));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedRenewSubscriptionsRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).renewSubscriptionsZuora(expectedRenewSubscriptionsRequestAux.capture());
        List<RenewSubscriptionsRequest> expectedRenewSubscriptionsRequest = (List<RenewSubscriptionsRequest>) expectedRenewSubscriptionsRequestAux.getValue();
        Assert.assertEquals(serviceRequest.getSubscriptionOriginalId(), expectedRenewSubscriptionsRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getTermEndDate(), expectedRenewSubscriptionsRequest.get(0).getUserExpirationDate());
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed_amendNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AmendNotFound() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(Arrays.asList()); //empty list

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("The amend id '2c92c08659b048b70159caa0d8520de7', has no 'Renewal' amend related")));

        verify(zuoraExternalService, never()).getSubscriptionsInfoByOriginalId(any(), any());
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed_errorTooManyAmends.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_TooManyAmends() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(Arrays.asList(renewalAmendPayed().get(0), renewalAmendPayed().get(0)));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Invalid amend results.")));

        verify(zuoraExternalService, never()).getSubscriptionsInfoByOriginalId(any(), any());
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed_errorDraftAmend.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AmendNotCompleted() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        final ZuoraAmendmentResponse draftAmend = renewalAmendPayed().get(0);
        draftAmend.setStatus("Draft");
        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(Arrays.asList(draftAmend));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Invalid amend results.")));

        verify(zuoraExternalService, never()).getSubscriptionsInfoByOriginalId(any(), any());
        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed_errorSubscriptionNotFound.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_SubscriptionNotFound() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(renewalAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.ACTIVE))))
                .thenReturn(Arrays.asList());
     // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Current active subscription not found for original id '2c92c0f9588ff00b0158923c53e9252e' and amendment id '2c92c08659b048b70159caa0d8520de7'")));

        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed_errorTooManySubscriptions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_TooManySubscriptions() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(renewalAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.ACTIVE))))
                .thenReturn(Arrays.asList(activeSubscriptionsRenewed().get(0), activeSubscriptionsRenewed().get(0)));
        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot register renewal subscription.")));

        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed_errorOldAmend.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_notLatestAmendOnSubscription() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(renewalAmendPayed());
        ZuoraSubscriptionResponse newestSubscription = activeSubscriptionsRenewed().get(0);
        newestSubscription.setPreviousSubscriptionId("xxx");
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.ACTIVE))))
                .thenReturn(Arrays.asList(newestSubscription));
        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot register renewal subscription.")));

        verifyZeroInteractions(abawebappsExternalService);
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed_errorUnexpectedFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaFails() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(renewalAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.ACTIVE))))
                .thenReturn(activeSubscriptionsRenewed());

        NotFoundExternalException ex = new NotFoundExternalException(new Exception("Unexpected error"));
        when(abawebappsExternalService.renewSubscriptionsZuora(anyList())).thenThrow(ex);

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Cannot send renewal subscription information to Aba")));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedRenewSubscriptionsRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).renewSubscriptionsZuora(expectedRenewSubscriptionsRequestAux.capture());
        List<RenewSubscriptionsRequest> expectedRenewSubscriptionsRequest = (List<RenewSubscriptionsRequest>) expectedRenewSubscriptionsRequestAux.getValue();
        Assert.assertEquals(serviceRequest.getSubscriptionOriginalId(), expectedRenewSubscriptionsRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getTermEndDate(), expectedRenewSubscriptionsRequest.get(0).getUserExpirationDate());
    }

    @Test
    @ExpectedDatabase(value = "expected/renewal-amendment-processed/new_notification_renewal_amendment_processed_errorsFromAba.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void receiveNotification_AbaReturnErrors() throws Exception {

        final ZuoraNotificationServiceRequest serviceRequest = zuoraNotificationRenealAmendmentProcessedServiceRequest();

        // mock notification service external calls
        when(zuoraExternalService.getAmendmentByIdFilters(
                eq(Optional.empty()),
                eq(Optional.of(serviceRequest.getSubscriptionVersionAmendmentId())),
                any()))
                .thenReturn(renewalAmendPayed());
        when(zuoraExternalService.getSubscriptionsInfoByOriginalId(eq(Arrays.asList(serviceRequest.getSubscriptionOriginalId())), eq(Optional.of(SubscriptionStatus.ACTIVE))))
                .thenReturn(activeSubscriptionsRenewed());

        when(abawebappsExternalService.renewSubscriptionsZuora(anyList())).thenReturn(Arrays.asList(
                SubscriptionInfoResponse.Builder.aSubscriptionInfoResponse()
                        .originalSubscriptionId(serviceRequest.getSubscriptionOriginalId())
                        .success(false)
                        .warnings("Errors on Aba")
                        .build()));

        // do request
        given().
                contentType(ContentType.XML).
                accept("application/xml").
                body(buildRequestXML(serviceRequest)).
                when().
                post("/api/v1/subscription-notifications/zuora").
                then().
                spec(buildResponseSpecificationXml(false, Optional.of("Errors on Aba")));

         /*
            check the arguments sent to aba are valid
         */
        ArgumentCaptor<List> expectedRenewSubscriptionsRequestAux = ArgumentCaptor.forClass(List.class);
        verify(abawebappsExternalService).renewSubscriptionsZuora(expectedRenewSubscriptionsRequestAux.capture());
        List<RenewSubscriptionsRequest> expectedRenewSubscriptionsRequest = (List<RenewSubscriptionsRequest>) expectedRenewSubscriptionsRequestAux.getValue();
        Assert.assertEquals(serviceRequest.getSubscriptionOriginalId(), expectedRenewSubscriptionsRequest.get(0).getOriginalSubscriptionId());
        Assert.assertEquals(activeSubscriptionsRenewed().get(0).getTermEndDate(), expectedRenewSubscriptionsRequest.get(0).getUserExpirationDate());
    }
}
