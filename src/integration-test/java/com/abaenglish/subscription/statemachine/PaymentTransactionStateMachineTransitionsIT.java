package com.abaenglish.subscription.statemachine;

import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.abawebapps.domain.InsertUserRequest;
import com.abaenglish.external.paypal.domain.PaypalExternalCustomerDetails;
import com.abaenglish.external.paypal.service.IPaypalExternalService;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceInfo;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.dto.CreateSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.controller.v1.TestConfiguration;
import com.abaenglish.subscription.service.impl.TransactionService;
import com.abaenglish.user.controller.v1.dto.response.InsertUserApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;
import com.zuora.api.axis2.UnexpectedErrorFault;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Arrays;

import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.subscribeResults;
import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.userWithoutZuoraAccount;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@DatabaseSetup(value = "../base/transactions.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../base/transactions-empty.xml")
public class PaymentTransactionStateMachineTransitionsIT {

    @Autowired
    private StateMachine<String, String> machine;

    @Autowired
    private TransactionService paymentTransactionService;

    @Autowired
    private IUserServiceFeign userServiceFeign;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private IPaypalExternalService paypalService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testNotStarted() throws Exception {
        assertThat(machine.getId(), isEmptyOrNullString());
    }

    @Test
    @ExpectedDatabase(value = "expected/payment_transaction-next_state.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testAllTransitionsAllowed() throws Exception {
        mockExternalServicesWithDummies();
        paymentTransactionService.change("1", "START_PROCESS");
        paymentTransactionService.change("2", "START_PROCESS");
        paymentTransactionService.change("3", "CONFIRMED_ZUORA");
        paymentTransactionService.change("4", "CONFIRMED_ABA");
        paymentTransactionService.change("6", "START_PROCESS");
        paymentTransactionService.change("7", "CONFIRMED_ZUORA");
    }

    @Test
    @ExpectedDatabase(value = "../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testTransitionsNotAllowed() throws Exception {
        paymentTransactionService.change("1", "CONFIRMED_ZUORA");
        paymentTransactionService.change("1", "CONFIRMED_ABA");

        paymentTransactionService.change("2", "CONFIRMED_ZUORA");
        paymentTransactionService.change("2", "CONFIRMED_ABA");

        paymentTransactionService.change("3", "START_PROCESS");
        paymentTransactionService.change("3", "CONFIRMED_ABA");

        paymentTransactionService.change("4", "START_PROCESS");
        paymentTransactionService.change("4", "CONFIRMED_ZUORA");

        paymentTransactionService.change("5", "START_PROCESS");
        paymentTransactionService.change("5", "CONFIRMED_ZUORA");
        paymentTransactionService.change("5", "CONFIRMED_ABA");
    }

    @Test
    @ExpectedDatabase(value = "expected/complete-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testCloseOpenTransaction() throws Exception {
        mockExternalServicesWithDummies();
        paymentTransactionService.change("1", "START_PROCESS");
        paymentTransactionService.change("1", "CONFIRMED_ZUORA");
        paymentTransactionService.change("1", "CONFIRMED_ABA");
    }


    @Test
    @ExpectedDatabase(value = "expected/error-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void testErrorsTransactions() throws Exception {
        // force the fail
        when(userServiceFeign.getUser(anyLong())).thenThrow(NotFoundApiException.class);
        when(rabbitTemplate.getMessageConverter()).thenReturn(new SimpleMessageConverter());
        paymentTransactionService.change("1", "START_PROCESS");
        paymentTransactionService.change("2", "START_PROCESS");
        paymentTransactionService.change("3", "CONFIRMED_ZUORA");
    }

    private void mockExternalServicesWithDummies() throws ServiceException, UnexpectedErrorFault, RemoteException {
        reset(userServiceFeign, zuoraExternalService, paypalService, rabbitTemplate);
        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.createSubscription(any(CreateSubscriptionRequest.class))).thenReturn(subscribeResults());
        when(zuoraExternalService.getInvoiceInfo(anyString())).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().amount(BigDecimal.ONE).build());
        when(zuoraExternalService.getSubscriptionInfoById(anyString(), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenReturn(Arrays.asList());
        when(userServiceFeign.insertUser(any(InsertUserRequest.class))).thenReturn(InsertUserApiResponse.Builder.anInsertUserApiResponse().build());
        when(paypalService.getExpressCheckoutDetails(anyString())).thenReturn(PaypalExternalCustomerDetails.Builder.aPaypalCustomerDetails().payerEmail(userWithoutZuoraAccount().getUser().getEmail()).build());
        when(paypalService.createBillingAgreement(anyString())).thenReturn("test-baid");
        when(rabbitTemplate.getMessageConverter()).thenReturn(new SimpleMessageConverter());
    }
}
