package com.abaenglish.subscription.consumer;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.exception.service.queue.RetryableServiceException;
import com.abaenglish.external.paypal.domain.PaypalExternalCustomerDetails;
import com.abaenglish.external.paypal.service.IPaypalExternalService;
import com.abaenglish.external.zuora.soap.service.dto.CreateSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.controller.v1.TestConfiguration;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.queue.PaymentTransactionEventType;
import com.abaenglish.subscription.queue.consumer.TransactionStartConsumer;
import com.abaenglish.subscription.queue.event.StartTransactionEvent;
import com.abaenglish.subscription.queue.event.ZuoraConfirmedEvent;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;
import com.zuora.api.axis2.UnexpectedErrorFault;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.rmi.RemoteException;

import static com.abaenglish.subscription.objectmother.PaymentTransactionObjectMother.openTransactionPaypal;
import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.subscribeResults;
import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.userWithoutZuoraAccount;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@DatabaseSetup(value = "../base/transactions.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../base/transactions-empty.xml")
public class TransactionStartConsumerIT {

    @Autowired
    private TransactionStartConsumer transactionStartConsumer;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private IUserServiceFeign userServiceFeign;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private IPaypalExternalService paypalService;

    private MessageConverter messageConverter = new SimpleMessageConverter();

    @Before
    public void beforeTest() {
        // clean previous calls
        reset(rabbitTemplate, userServiceFeign, zuoraExternalService, paypalService);

        when(rabbitTemplate.getMessageConverter()).thenReturn(messageConverter);
    }

    @Test
    @ExpectedDatabase(value = "expected/update-open-zuora-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenZuoraTransaction() throws ApiException, UnexpectedErrorFault, RemoteException, ServiceException {

        //mock external calls
        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.createSubscription(any(CreateSubscriptionRequest.class))).thenReturn(subscribeResults());

        // prepare event
        final StartTransactionEvent event = StartTransactionEvent.Builder.aStartTransactionEvent()
                .publicId("1")
                .build();

        // consume event
        transactionStartConsumer.onMessage(event);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "expected/update-open-paypal-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenPayPalTransaction() throws ApiException, UnexpectedErrorFault, RemoteException, ServiceException {

        final PaymentTransaction transaction = openTransactionPaypal();

        //mock external calls
        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.createSubscription(any(CreateSubscriptionRequest.class))).thenReturn(subscribeResults());
        when(paypalService.getExpressCheckoutDetails(anyString()))
                .thenReturn(PaypalExternalCustomerDetails.Builder.aPaypalCustomerDetails().payerEmail("test@gmail.com").build());
        when(paypalService.createBillingAgreement(eq(transaction.getPaypalToken()))).thenReturn("BAIDPAYPAL");

        // prepare event
        final StartTransactionEvent event = StartTransactionEvent.Builder.aStartTransactionEvent()
                .publicId(transaction.getPublicId())
                .build();

        // consume event
        transactionStartConsumer.onMessage(event);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void notAllowedStartTransaction() throws ApiException, UnexpectedErrorFault, RemoteException, ServiceException {

        // consume events with invalid previous status
        transactionStartConsumer.onMessage(StartTransactionEvent.Builder.aStartTransactionEvent().publicId("3").build());
        transactionStartConsumer.onMessage(StartTransactionEvent.Builder.aStartTransactionEvent().publicId("4").build());
        transactionStartConsumer.onMessage(StartTransactionEvent.Builder.aStartTransactionEvent().publicId("5").build());
        transactionStartConsumer.onMessage(StartTransactionEvent.Builder.aStartTransactionEvent().publicId("7").build());

        // check mocks never called
        verify(rabbitTemplate, never()).send(anyString(), anyString(), any(Message.class));
        verify(userServiceFeign, never()).getUser(anyLong());
        verify(zuoraExternalService, never()).createSubscription(any(CreateSubscriptionRequest.class));
        verify(paypalService, never()).getExpressCheckoutDetails(anyString());
        verify(paypalService, never()).createBillingAgreement(anyString());
    }

    @Test
    @ExpectedDatabase(value = "expected/update-open-zuora-fail-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenTransactionZuoraThrowsAnException() throws ApiException, UnexpectedErrorFault, RemoteException, ServiceException {

        //mock external calls
        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.createSubscription(any(CreateSubscriptionRequest.class))).thenThrow(new RemoteException());

        // prepare event
        final StartTransactionEvent event = StartTransactionEvent.Builder.aStartTransactionEvent()
                .publicId("1")
                .build();

        // consume event
        transactionStartConsumer.onMessage(event);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED_ERROR.getExchange()),
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED_ERROR.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "expected/update-open-zuora-fail-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenTransactionPayPalThrowsAnException() throws ApiException, ServiceException {

        //mock external calls
        when(userServiceFeign.getUser(anyLong())).thenReturn(userWithoutZuoraAccount());
        when(paypalService.getExpressCheckoutDetails(anyString())).thenThrow(new ServiceException("Error test", new Exception()));

        // prepare event
        final StartTransactionEvent event = StartTransactionEvent.Builder.aStartTransactionEvent()
                .publicId("1")
                .build();

        // consume event
        transactionStartConsumer.onMessage(event);

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED_ERROR.getExchange()),
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED_ERROR.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenZuoraTransactionTransactionNotFound() throws ApiException, UnexpectedErrorFault, RemoteException, ServiceException {

        // prepare event
        final StartTransactionEvent event = StartTransactionEvent.Builder.aStartTransactionEvent()
                .publicId("25abc")
                .build();

        // consume event
        Assertions.assertThatThrownBy(() -> transactionStartConsumer.onMessage(event))
                .isInstanceOf(RetryableServiceException.class);

        // check event publications
        verify(rabbitTemplate, never()).send(
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ZUORA_CONFIRMED.getRoutingKey()),
                any(Message.class));
        verify(userServiceFeign, never()).getUser(anyLong());
        verify(zuoraExternalService, never()).createSubscription(any());
    }
}
