package com.abaenglish.subscription.consumer;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.exception.service.queue.RetryableServiceException;
import com.abaenglish.external.abawebapps.domain.InsertUserRequest;
import com.abaenglish.external.abawebapps.domain.UpdateInvoiceRequest;
import com.abaenglish.external.abawebapps.domain.UpdateInvoiceResponse;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.external.zuora.soap.domain.subscription.InvoiceInfo;
import com.abaenglish.external.zuora.soap.domain.subscription.ZuoraSubscriptionResponse;
import com.abaenglish.external.zuora.soap.service.dto.CreateSubscriptionRequest;
import com.abaenglish.external.zuora.soap.service.IZuoraExternalServiceSoap;
import com.abaenglish.external.zuora.soap.service.impl.ZuoraExternalServiceSoap;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.controller.v1.TestConfiguration;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.queue.PaymentTransactionEventType;
import com.abaenglish.subscription.queue.consumer.ZuoraConfirmedConsumer;
import com.abaenglish.subscription.queue.event.AbaConfirmedEvent;
import com.abaenglish.subscription.queue.event.ZuoraConfirmedEvent;
import com.abaenglish.user.controller.v1.dto.response.InsertUserApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;
import com.zuora.api.axis2.UnexpectedErrorFault;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Arrays;

import static com.abaenglish.subscription.objectmother.PaymentTransactionObjectMother.zuoraConfirmedTransactionCreditCard;
import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.userWithoutZuoraAccount;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@DatabaseSetup(value = "../base/transactions.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../base/transactions-empty.xml")
public class ZuoraConfirmedConsumerIT {

    @Autowired
    private ZuoraConfirmedConsumer zuoraConfirmedConsumer;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private IUserServiceFeign userServiceFeign;

    @Autowired
    private IZuoraExternalServiceSoap zuoraExternalService;

    @Autowired
    private IAbawebappsExternalService abawebappsExternalService;

    private MessageConverter messageConverter = new SimpleMessageConverter();

    @Before
    public void beforeTest(){
        // clean previous calls
        reset(rabbitTemplate, userServiceFeign, zuoraExternalService, abawebappsExternalService);
        when(rabbitTemplate.getMessageConverter()).thenReturn(messageConverter);
    }

    @Test
    @ExpectedDatabase(value = "expected/update-process-zuora-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenTransaction() throws ApiException, NotFoundExternalException, UnexpectedErrorFault, RemoteException, ServiceException {

        final PaymentTransaction txn = zuoraConfirmedTransactionCreditCard();

        //mock external calls
        when(userServiceFeign.getUser(eq(txn.getUserId()))).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.getInvoiceInfo(anyString())).thenReturn(InvoiceInfo.Builder.anInvoiceInfo()
                .amount(txn.getPrice())
                .invoiceNumber(txn.getInvoiceIdZuora())
                .build());
        when(zuoraExternalService.getSubscriptionInfoById(anyString(), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenReturn(Arrays.asList(
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("1").accountId("1").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build(),
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("2").accountId("2").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build()));
        when(userServiceFeign.insertUser(any(InsertUserRequest.class))).thenReturn(InsertUserApiResponse.Builder.anInsertUserApiResponse().build());

        // prepare event
        ZuoraConfirmedEvent event = ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent()
                .publicId("3")
                .build();

        // consume event
        zuoraConfirmedConsumer.onMessage(event);

        //check calls to external services were as expected
        verify(userServiceFeign).getUser(eq(txn.getUserId()));
        verify(zuoraExternalService).getInvoiceInfo(eq(txn.getInvoiceIdZuora()));
        verify(zuoraExternalService).getSubscriptionInfoById(eq(txn.getSubscriptionIdZuora()), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE));
        InsertUserRequest expectedInsertUserRequest = InsertUserRequest.Builder.anInsertUserRequest()
                .zuoraAccountId(txn.getAccountIdZuora())
                .userId(txn.getUserId())
                .zuoraAccountNumber(txn.getAccountNumberZuora())
                .periodId(txn.getPeriod())
                .currency(userWithoutZuoraAccount().getUser().getCurrency())
                .idCountry(userWithoutZuoraAccount().getUser().getCountry())
                .invoiceId(txn.getInvoiceIdZuora())
                .subscriptionId(txn.getSubscriptionIdZuora())
                .subscriptionNumber(txn.getSubscriptionNumberZuora())
                .invoiceNumber(txn.getInvoiceIdZuora())
                .price(txn.getPrice())
                .finalPrice(txn.getPrice())
                .build();
        verify(userServiceFeign).insertUser(refEq(expectedInsertUserRequest, "idDiscount", "idRatePlanCharge", "subscriptionsList"));
        verify(abawebappsExternalService, never()).updateInvoiceZuora(any());

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void notAllowedZuoraConfirmedTransaction() throws ApiException, NotFoundExternalException, UnexpectedErrorFault, RemoteException, ServiceException {

        // consume events with invalid previous status
        zuoraConfirmedConsumer.onMessage(ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent().publicId("1").build());
        zuoraConfirmedConsumer.onMessage(ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent().publicId("2").build());
        zuoraConfirmedConsumer.onMessage(ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent().publicId("4").build());
        zuoraConfirmedConsumer.onMessage(ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent().publicId("5").build());
        zuoraConfirmedConsumer.onMessage(ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent().publicId("6").build());

        // check mocks never called
        verify(rabbitTemplate, never()).send(anyString(), anyString(), any(Message.class));
        verify(userServiceFeign, never()).getUser(anyLong());
        verify(zuoraExternalService, never()).createSubscription(any(CreateSubscriptionRequest.class));
        verify(abawebappsExternalService, never()).updateInvoiceZuora(any());
    }

    @Test
    @ExpectedDatabase(value = "expected/update-process-zuora-payment_transaction-error.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenTransactionThrowsException() throws ApiException, NotFoundExternalException, UnexpectedErrorFault, RemoteException, ServiceException {

        final PaymentTransaction txn = zuoraConfirmedTransactionCreditCard();

        //mock external calls
        when(userServiceFeign.getUser(eq(txn.getUserId()))).thenReturn(userWithoutZuoraAccount());
        when(zuoraExternalService.getInvoiceInfo(anyString())).thenReturn(InvoiceInfo.Builder.anInvoiceInfo()
                .amount(txn.getPrice())
                .invoiceNumber(txn.getInvoiceIdZuora())
                .build());
        when(zuoraExternalService.getSubscriptionInfoById(anyString(), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenThrow(RemoteException.class);

        // prepare event
        ZuoraConfirmedEvent event = ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent()
                .publicId("3")
                .build();

        // consume event
        zuoraConfirmedConsumer.onMessage(event);

        //check calls to external services were as expected
        verify(userServiceFeign).getUser(eq(txn.getUserId()));
        verify(zuoraExternalService).getInvoiceInfo(eq(txn.getInvoiceIdZuora()));
        verify(zuoraExternalService).getSubscriptionInfoById(eq(txn.getSubscriptionIdZuora()), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE));
        verify(userServiceFeign, never()).insertUser(any());
        verify(abawebappsExternalService, never()).updateInvoiceZuora(any());

        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ABA_CONFIRMED_ERROR.getExchange()),
                eq(PaymentTransactionEventType.ABA_CONFIRMED_ERROR.getRoutingKey()),
                any(Message.class));
    }


    @Test
    @ExpectedDatabase(value = "expected/update-process-zuora-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenTransactionInvoiceInfoNeverCollected() throws ApiException, NotFoundExternalException, UnexpectedErrorFault, RemoteException, ServiceException {

        final PaymentTransaction txn = zuoraConfirmedTransactionCreditCard();

        //mock external calls
        when(userServiceFeign.getUser(eq(txn.getUserId()))).thenReturn(userWithoutZuoraAccount());

        when(zuoraExternalService.getInvoiceInfo(anyString())).thenReturn(InvoiceInfo.Builder.anInvoiceInfo().amount(BigDecimal.ZERO).build());

        when(zuoraExternalService.getSubscriptionInfoById(anyString(), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenReturn(Arrays.asList(
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("1").accountId("1").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build(),
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("2").accountId("2").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build()));
        when(userServiceFeign.insertUser(any(InsertUserRequest.class))).thenReturn(InsertUserApiResponse.Builder.anInsertUserApiResponse().build());

        // prepare event
        ZuoraConfirmedEvent event = ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent()
                .publicId("3")
                .build();

        // consume event
        zuoraConfirmedConsumer.onMessage(event);

        //check calls to external services were as expected
        verify(userServiceFeign).getUser(eq(txn.getUserId()));
        verify(zuoraExternalService, times(4)).getInvoiceInfo(eq(txn.getInvoiceIdZuora()));
        verify(zuoraExternalService).getSubscriptionInfoById(eq(txn.getSubscriptionIdZuora()), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE));
        InsertUserRequest expectedInsertUserRequest = InsertUserRequest.Builder.anInsertUserRequest()
                .zuoraAccountId(txn.getAccountIdZuora())
                .userId(txn.getUserId())
                .zuoraAccountNumber(txn.getAccountNumberZuora())
                .periodId(txn.getPeriod())
                .currency(userWithoutZuoraAccount().getUser().getCurrency())
                .idCountry(userWithoutZuoraAccount().getUser().getCountry())
                .invoiceId(txn.getInvoiceIdZuora())
                .finalPrice(BigDecimal.ZERO)
                .subscriptionId(txn.getSubscriptionIdZuora())
                .subscriptionNumber(txn.getSubscriptionNumberZuora())
                .price(txn.getPrice())
                .build();
        verify(userServiceFeign).insertUser(refEq(expectedInsertUserRequest, "idDiscount", "idRatePlanCharge", "subscriptionsList"));
        verify(abawebappsExternalService, never()).updateInvoiceZuora(any());

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "expected/update-process-zuora-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenTransactionInvoiceInfoCollectedAfter() throws ApiException, NotFoundExternalException, UnexpectedErrorFault, RemoteException, ServiceException {

        final PaymentTransaction txn = zuoraConfirmedTransactionCreditCard();

        //mock external calls
        when(userServiceFeign.getUser(eq(txn.getUserId()))).thenReturn(userWithoutZuoraAccount());

        // mock multiple calls with different responses
        InvoiceInfo invoiceInfoCollected =InvoiceInfo.Builder.anInvoiceInfo()
                .amount(txn.getPrice())
                .invoiceNumber(txn.getInvoiceIdZuora())
                .build();
        InvoiceInfo invoiceInfoEmpty =InvoiceInfo.Builder.anInvoiceInfo().amount(BigDecimal.ZERO).build();
        when(zuoraExternalService.getInvoiceInfo(anyString())).thenAnswer(new Answer<InvoiceInfo>() {
            private int attempt = 0;
            public InvoiceInfo answer(InvocationOnMock invocation) {
                return (attempt++ == 1) ? invoiceInfoCollected : invoiceInfoEmpty;
            }
        });

        when(zuoraExternalService.getSubscriptionInfoById(anyString(), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenReturn(Arrays.asList(
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("1").accountId("1").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build(),
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("2").accountId("2").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build()));
        when(userServiceFeign.insertUser(any(InsertUserRequest.class))).thenReturn(InsertUserApiResponse.Builder.anInsertUserApiResponse().build());

        UpdateInvoiceRequest updateInvoiceRequest =UpdateInvoiceRequest.Builder.anUpdateInvoiceRequest()
                .invoiceId(txn.getInvoiceIdZuora())
                .invoiceNumber(invoiceInfoCollected.getInvoiceNumber())
                .totalAmount(invoiceInfoCollected.getAmount())
                .build();
        when(abawebappsExternalService.updateInvoiceZuora(refEq(updateInvoiceRequest)))
                .thenReturn(UpdateInvoiceResponse.Builder.anInsertUserResponse().success(true).build());

        // prepare event
        ZuoraConfirmedEvent event = ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent()
                .publicId("3")
                .build();

        // consume event
        zuoraConfirmedConsumer.onMessage(event);

        //check calls to external services were as expected
        verify(userServiceFeign).getUser(eq(txn.getUserId()));
        verify(zuoraExternalService, times(2)).getInvoiceInfo(eq(txn.getInvoiceIdZuora()));
        verify(zuoraExternalService).getSubscriptionInfoById(eq(txn.getSubscriptionIdZuora()), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE));
        InsertUserRequest expectedInsertUserRequest = InsertUserRequest.Builder.anInsertUserRequest()
                .zuoraAccountId(txn.getAccountIdZuora())
                .userId(txn.getUserId())
                .zuoraAccountNumber(txn.getAccountNumberZuora())
                .periodId(txn.getPeriod())
                .currency(userWithoutZuoraAccount().getUser().getCurrency())
                .idCountry(userWithoutZuoraAccount().getUser().getCountry())
                .invoiceId(txn.getInvoiceIdZuora())
                .finalPrice(BigDecimal.ZERO)
                .subscriptionId(txn.getSubscriptionIdZuora())
                .subscriptionNumber(txn.getSubscriptionNumberZuora())
                .price(txn.getPrice())
                .build();
        verify(userServiceFeign).insertUser(refEq(expectedInsertUserRequest, "idDiscount", "idRatePlanCharge", "subscriptionsList"));
        verify(abawebappsExternalService).updateInvoiceZuora(refEq(updateInvoiceRequest));

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "expected/update-process-zuora-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenTransactionInvoiceInfoCollectedAfterButAbaFails() throws ApiException, NotFoundExternalException, UnexpectedErrorFault, RemoteException, ServiceException {

        final PaymentTransaction txn = zuoraConfirmedTransactionCreditCard();

        //mock external calls
        when(userServiceFeign.getUser(eq(txn.getUserId()))).thenReturn(userWithoutZuoraAccount());

        // mock multiple calls with different responses
        InvoiceInfo invoiceInfoCollected =InvoiceInfo.Builder.anInvoiceInfo()
                .amount(txn.getPrice())
                .invoiceNumber(txn.getInvoiceIdZuora())
                .build();
        InvoiceInfo invoiceInfoEmpty =InvoiceInfo.Builder.anInvoiceInfo().amount(BigDecimal.ZERO).build();
        when(zuoraExternalService.getInvoiceInfo(anyString())).thenAnswer(new Answer<InvoiceInfo>() {
            private int attempt = 0;
            public InvoiceInfo answer(InvocationOnMock invocation) {
                return (attempt++ > 1) ? invoiceInfoCollected : invoiceInfoEmpty;
            }
        });

        when(zuoraExternalService.getSubscriptionInfoById(anyString(), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE))).thenReturn(Arrays.asList(
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("1").accountId("1").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build(),
                ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse().id("2").accountId("2").termStartDate("2016-09-19 00:00:00.0").termEndDate("2016-10-19 00:00:00.0").build()));
        when(userServiceFeign.insertUser(any(InsertUserRequest.class))).thenReturn(InsertUserApiResponse.Builder.anInsertUserApiResponse().build());

        UpdateInvoiceRequest updateInvoiceRequest =UpdateInvoiceRequest.Builder.anUpdateInvoiceRequest()
                .invoiceId(txn.getInvoiceIdZuora())
                .invoiceNumber(invoiceInfoCollected.getInvoiceNumber())
                .totalAmount(invoiceInfoCollected.getAmount())
                .build();
        when(abawebappsExternalService.updateInvoiceZuora(refEq(updateInvoiceRequest)))
                .thenReturn(UpdateInvoiceResponse.Builder.anInsertUserResponse()
                        .success(false)
                        .warnings("some error;another error")
                        .build());

        // prepare event
        ZuoraConfirmedEvent event = ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent()
                .publicId("3")
                .build();

        // consume event
        zuoraConfirmedConsumer.onMessage(event);

        //check calls to external services were as expected
        verify(userServiceFeign).getUser(eq(txn.getUserId()));
        verify(zuoraExternalService, times(4)).getInvoiceInfo(eq(txn.getInvoiceIdZuora()));
        verify(zuoraExternalService).getSubscriptionInfoById(eq(txn.getSubscriptionIdZuora()), eq(ZuoraExternalServiceSoap.ZStatus.ACTIVE));
        InsertUserRequest expectedInsertUserRequest = InsertUserRequest.Builder.anInsertUserRequest()
                .zuoraAccountId(txn.getAccountIdZuora())
                .userId(txn.getUserId())
                .zuoraAccountNumber(txn.getAccountNumberZuora())
                .periodId(txn.getPeriod())
                .currency(userWithoutZuoraAccount().getUser().getCurrency())
                .idCountry(userWithoutZuoraAccount().getUser().getCountry())
                .invoiceId(txn.getInvoiceIdZuora())
                .finalPrice(BigDecimal.ZERO)
                .subscriptionId(txn.getSubscriptionIdZuora())
                .subscriptionNumber(txn.getSubscriptionNumberZuora())
                .price(txn.getPrice())
                .build();
        verify(userServiceFeign).insertUser(refEq(expectedInsertUserRequest, "idDiscount", "idRatePlanCharge", "subscriptionsList"));
        verify(abawebappsExternalService, times(2)).updateInvoiceZuora(refEq(updateInvoiceRequest));

        // check event publications
        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void startOpenTransactionTransactionNotFound() throws ApiException, NotFoundExternalException, UnexpectedErrorFault, RemoteException, ServiceException {

        // prepare event
        ZuoraConfirmedEvent event = ZuoraConfirmedEvent.Builder.aZuoraConfirmedEvent()
                .publicId("25abc")
                .build();

        // consume event
        Assertions.assertThatThrownBy(() -> zuoraConfirmedConsumer.onMessage(event))
                .isInstanceOf(RetryableServiceException.class);

        //check calls to external services were as expected
        verify(userServiceFeign, never()).getUser(anyLong());
        verify(zuoraExternalService, never()).getInvoiceInfo(anyString());
        verify(zuoraExternalService, never()).getSubscriptionInfoById(anyString(), any());
        verify(userServiceFeign, never()).insertUser(any());
        verify(abawebappsExternalService, never()).updateInvoiceZuora(any());

        // check event publications
        verify(rabbitTemplate, never()).send(
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getExchange()),
                eq(PaymentTransactionEventType.ABA_CONFIRMED.getRoutingKey()),
                any(Message.class));
    }

}
