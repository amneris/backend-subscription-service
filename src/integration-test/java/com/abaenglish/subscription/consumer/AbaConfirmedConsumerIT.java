package com.abaenglish.subscription.consumer;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.exception.service.queue.RetryableServiceException;
import com.abaenglish.subscription.SubscriptionApplication;
import com.abaenglish.subscription.controller.v1.TestConfiguration;
import com.abaenglish.subscription.queue.consumer.AbaConfirmedConsumer;
import com.abaenglish.subscription.queue.event.AbaConfirmedEvent;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.*;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SubscriptionApplication.class, TestConfiguration.class})
@WebIntegrationTest(randomPort = true)
@TestExecutionListeners({DbUnitTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@DatabaseSetup(value = "../base/transactions.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = "../base/transactions-empty.xml")
public class AbaConfirmedConsumerIT {

    @Autowired
    private AbaConfirmedConsumer abaConfirmedConsumer;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private MessageConverter messageConverter = new SimpleMessageConverter();

    @Before
    public void beforeTest() {
         // clean previous calls
        reset(rabbitTemplate);

        when(rabbitTemplate.getMessageConverter()).thenReturn(messageConverter);
    }

    @Test
    @ExpectedDatabase(value = "expected/update-process-aba-payment_transaction.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void abaConfirmedTransaction() throws ServiceException {

        // prepare event
        AbaConfirmedEvent event = AbaConfirmedEvent.Builder.aAbaConfirmedEvent()
                .publicId("4")
                .build();

        // consume event
        abaConfirmedConsumer.onMessage(event);

        // check event publications
        verify(rabbitTemplate, never()).send(anyString(), anyString(), any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void notAllowedAbaConfirmedTransaction() throws ServiceException {

        // consume events with invalid previous status
        abaConfirmedConsumer.onMessage(AbaConfirmedEvent.Builder.aAbaConfirmedEvent().publicId("1").build());
        abaConfirmedConsumer.onMessage(AbaConfirmedEvent.Builder.aAbaConfirmedEvent().publicId("2").build());
        abaConfirmedConsumer.onMessage(AbaConfirmedEvent.Builder.aAbaConfirmedEvent().publicId("3").build());
        abaConfirmedConsumer.onMessage(AbaConfirmedEvent.Builder.aAbaConfirmedEvent().publicId("5").build());
        abaConfirmedConsumer.onMessage(AbaConfirmedEvent.Builder.aAbaConfirmedEvent().publicId("6").build());
        abaConfirmedConsumer.onMessage(AbaConfirmedEvent.Builder.aAbaConfirmedEvent().publicId("7").build());

        // check mocks never called
        verify(rabbitTemplate, never()).send(anyString(), anyString(), any(Message.class));
    }

    @Test
    @ExpectedDatabase(value = "../base/transactions.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void abaConfirmedTransactionTransactionNotFound() {

        // prepare event
        AbaConfirmedEvent event = AbaConfirmedEvent.Builder.aAbaConfirmedEvent()
                .publicId("25abc")
                .build();

        // consume event
        Assertions.assertThatThrownBy(() -> abaConfirmedConsumer.onMessage(event))
                .isInstanceOf(RetryableServiceException.class);


        // check event publications
        verify(rabbitTemplate, never()).send(anyString(), anyString(), any(Message.class));
    }
}
