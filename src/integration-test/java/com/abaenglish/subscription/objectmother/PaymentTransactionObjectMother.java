package com.abaenglish.subscription.objectmother;

import com.abaenglish.subscription.controller.v1.dto.transaction.CreatePaypalTransactionRequest;
import com.abaenglish.subscription.controller.v1.dto.transaction.CreateZuoraTransactionRequest;
import com.abaenglish.subscription.domain.PaymentTransaction;
import com.abaenglish.subscription.domain.SubscriptionType;
import com.abaenglish.subscription.service.dto.transaction.PayloadServiceRequest;
import com.abaenglish.subscription.statemachine.States;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;

import static com.abaenglish.subscription.objectmother.SubscriptionObjectMother.userWithoutZuoraAccount;

public class PaymentTransactionObjectMother {

    public static CreateZuoraTransactionRequest createZuoraTransactionRequest() {
        return new CreateZuoraTransactionRequest.Builder()
                .type(SubscriptionType.CreditCard)
                .userId(userWithoutZuoraAccount().getUser().getId())
                .period(6)
                .ratePlans(Arrays.asList("2c92c0f9555351c1015558ac836f6b26", "2c92c0f8552e5302015530abaa915b5c"))
                .zuoraRefId("123456789")
                .gatewayName("Stripe Gateway")
                .build();
    }

    public static CreatePaypalTransactionRequest createPaypalTransactionRequest() {
        return new CreatePaypalTransactionRequest.Builder()
                .type(SubscriptionType.PayPal)
                .userId(userWithoutZuoraAccount().getUser().getId())
                .period(6)
                .ratePlans(Arrays.asList("2c92c0f9555351c1015558ac836f6b26", "2c92c0f8552e5302015530abaa915b5c"))
                .paypalToken("XXXXXY")
                .gatewayName("PayPal Express Checkout")
                .build();
    }

    public static PaymentTransaction paymentTransaction() {
        return PaymentTransaction.Builder.aPaymentTransaction()
                .userId(12345L)
                .type(SubscriptionType.CreditCard.name())
                .publicId("12345")
                .creationDate(LocalDateTime.now())
                .state(States.OPEN)
                .subscriptionIdZuora("S12345")
                .invoiceIdZuora("I12345")
                .id(1L)
                .period(12)
                .gatewayName("Stripe Gateway")
                .build();
    }

    public static PaymentTransaction paymentTransactionClosed() {
        return PaymentTransaction.Builder.aPaymentTransaction()
                .userId(54321L)
                .type(SubscriptionType.PayPal.name())
                .gatewayName("PayPal Express Checkout")
                .publicId("54321")
                .creationDate(LocalDateTime.now())
                .state(States.CLOSED)
                .subscriptionIdZuora("S54321")
                .invoiceIdZuora("I12345")
                .id(2L)
                .period(24)
                .build();
    }

    public static PaymentTransaction openTransactionCreditCard() {
        return PaymentTransaction.Builder.aPaymentTransaction()
                .id(1L)
                .state(States.OPEN)
                .publicId("1")
                .userId(265L)
                .type(SubscriptionType.CreditCard.name())
                .period(6)
                .gatewayName("Stripe Gateway")
                .payload(PayloadServiceRequest.Builder.aPayload()
                        .period(6)
                        .userId(265L)
                        .paymentId("2c92c0f856bc84c90156c66b655a7f37")
                        .type(SubscriptionType.CreditCard)
                        .ratePlans(Arrays.asList("2c92c0f9555351c1015558ac836f6b26","2c92c0f8552e5302015530abaa915b5c"))
                        .build())
                .build();
    }

    public static PaymentTransaction openTransactionPaypal() {
        return PaymentTransaction.Builder.aPaymentTransaction()
                .id(6L)
                .state(States.OPEN)
                .publicId("6")
                .userId(265L)
                .type(SubscriptionType.PayPal.name())
                .period(12)
                .gatewayName("PayPal Express Checkout")
                .paypalToken("XXXX1")
                .payload(PayloadServiceRequest.Builder.aPayload()
                        .period(12)
                        .userId(265L)
                        .paymentId("2c92c0f856bc84c90156c66b655a7f37")
                        .type(SubscriptionType.PayPal)
                        .ratePlans(Arrays.asList("2c92c0f9555351c1015558ac836f6b26","2c92c0f8552e5302015530abaa915b5c"))
                        .build())
                .build();
    }

    public static PaymentTransaction zuoraConfirmedTransactionCreditCard() {
        return PaymentTransaction.Builder.aPaymentTransaction()
                .id(3L)
                .state(States.PROCESS_ZUORA)
                .publicId("3")
                .userId(265L)
                .type(SubscriptionType.CreditCard.name())
                .period(12)
                .gatewayName("Stripe Gateway")
                .subscriptionIdZuora("2345678901")
                .invoiceIdZuora("1234567890")
                .subscriptionNumberZuora("123456")
                .accountIdZuora("321654")
                .accountNumberZuora("789654")
                .price(new BigDecimal(100))
                .payload(PayloadServiceRequest.Builder.aPayload()
                        .period(12)
                        .userId(265L)
                        .paymentId("2c92c0f856bc84c90156c66b655a7f37")
                        .type(SubscriptionType.CreditCard)
                        .ratePlans(Arrays.asList("2c92c0f9555351c1015558ac836f6b26","2c92c0f8552e5302015530abaa915b5c"))
                        .build())
                .build();
    }

    public static PaymentTransaction zuoraConfirmedTransactionPayPal() {
        return PaymentTransaction.Builder.aPaymentTransaction()
                .id(7L)
                .state(States.PROCESS_ZUORA)
                .publicId("7")
                .userId(265L)
                .type(SubscriptionType.PayPal.name())
                .period(12)
                .paypalToken("XXXX2")
                .gatewayName("PayPal Express Checkout")
                .subscriptionIdZuora("00000699")
                .invoiceIdZuora("1234567890")
                .subscriptionNumberZuora("00000699")
                .accountIdZuora("987654321")
                .accountNumberZuora("123456789")
                .price(new BigDecimal(99.99))
                .payload(PayloadServiceRequest.Builder.aPayload()
                        .period(12)
                        .userId(265L)
                        .paypalToken("XXXX2")
                        .type(SubscriptionType.PayPal)
                        .ratePlans(Arrays.asList("2c92c0f9555351c1015558ac836f6b26","2c92c0f8552e5302015530abaa915b5c"))
                        .build())
                .build();
    }

    public static PaymentTransaction abaConfirmedTransactionCreditCard() {
        return PaymentTransaction.Builder.aPaymentTransaction()
                .id(4L)
                .state(States.PROCESS_ABA)
                .publicId("4")
                .userId(265L)
                .type(SubscriptionType.CreditCard.name())
                .period(12)
                .subscriptionIdZuora("S15345")
                .invoiceIdZuora("I12344")
                .subscriptionNumberZuora("33333")
                .accountIdZuora("22222")
                .accountNumberZuora("11111")
                .price(new BigDecimal(120))
                .payload(PayloadServiceRequest.Builder.aPayload()
                        .period(12)
                        .userId(265L)
                        .paymentId("2c92c0f856bc84c90156c66b655a7f37")
                        .type(SubscriptionType.CreditCard)
                        .ratePlans(Arrays.asList("2c92c0f9555351c1015558ac836f6b26","2c92c0f8552e5302015530abaa915b5c"))
                        .build())
                .build();
    }
}
