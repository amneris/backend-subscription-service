package com.abaenglish.subscription.objectmother;

import com.abaenglish.subscription.controller.v1.dto.subscription.CreateSubscriptionRequest;
import com.abaenglish.subscription.domain.SubscriptionType;
import com.abaenglish.subscription.service.dto.subscription.CreateSubscriptionServiceResponse;
import com.abaenglish.user.controller.v1.dto.response.InsertUserApiResponse;
import com.abaenglish.user.controller.v1.dto.response.UserApiResponse;
import com.abaenglish.user.controller.v1.dto.response.UserZuoraApiResponse;
import com.abaenglish.user.controller.v1.dto.response.ZuoraApiResponse;
import com.zuora.api.axis2.ZuoraServiceStub;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.zuora.api.axis2.ZuoraServiceStub.*;

public class SubscriptionObjectMother {

    public static CreateSubscriptionRequest newSubscriptionCreditCardRequest() {

        List<String> list = new ArrayList<>();
        list.add("123456789");

        CreateSubscriptionRequest createSubscriptionRequest = CreateSubscriptionRequest.Builder.aCreateSubscriptionRequest()
                .type(SubscriptionType.CreditCard)
                .paymentId("987654321")
                .period(12)
                .ratePlanId(list)
                .userId(265L)
                .gatewayName("Stripe Gateway")
                .build();
        return createSubscriptionRequest;

    }

    public static CreateSubscriptionRequest newSubscriptionPaypalRequest() {
        return CreateSubscriptionRequest.Builder.aCreateSubscriptionRequest()
                .type(SubscriptionType.PayPal)
                .paypalToken("XXXX1234")
                .period(12)
                .ratePlanId(Arrays.asList("123456789"))
                .userId(265L)
                .gatewayName("PayPal Express Checkout")
                .build();

    }

    public static UserZuoraApiResponse userWithZuoraAccount() {

        UserApiResponse user = UserApiResponse.Builder.anUserApiResponse()
                .id(265L)
                .country("MEX")
                .currency("MXN")
                .name("Name")
                .surname("Surname")
                .email("student+265@abaenglish.com")
                .build();

        ZuoraApiResponse zuora = ZuoraApiResponse.Builder.aZuoraApiResponse()
                .dateAdd(LocalDate.now().toString())
                .status("1")
                .zuoraAccountId("123456ab")
                .zuoraAccountNumber("123456789")
                .build();

        UserZuoraApiResponse userZuora = UserZuoraApiResponse.Builder.anUserZuoraApiResponse()
                .zuora(zuora)
                .user(user)
                .build();
        return userZuora;
    }

    public static UserZuoraApiResponse userWithoutZuoraAccount() {

        UserApiResponse user = UserApiResponse.Builder.anUserApiResponse()
                .id(265L)
                .country("MEX")
                .currency("MXN")
                .name("Name")
                .surname("Surname")
                .email("student+265@abaenglish.com")
                .build();

        UserZuoraApiResponse userZuora = UserZuoraApiResponse.Builder.anUserZuoraApiResponse()
                .user(user)
                .build();
        return userZuora;
    }

    public static SubscribeResult[] subscribeResults() {
        ID accountId = new ID();
        accountId.setID("987654321");
        SubscribeResult[] results = new SubscribeResult[1];
        SubscribeResult result = new SubscribeResult();
        result.setSuccess(true);
        result.setAccountNumber("123456789");
        result.setAccountId(accountId);

        ID invoiceId = new ID();
        invoiceId.setID("1234567890");
        result.setInvoiceId(invoiceId);
        result.setInvoiceNumber("INV00000699");
        result.setTotalTcv(new BigDecimal(99.99));

        ID subscriptionId = new ID();
        subscriptionId.setID("2345678901");
        result.setSubscriptionId(subscriptionId);
        result.setSubscriptionNumber("00000699");

        results[0] = result;
        return results;
    }

    public static SubscribeResult[] badSubscribeResults() {
        SubscribeResult[] results = new SubscribeResult[1];
        SubscribeResult result = new SubscribeResult();
        result.setSuccess(false);

        ErrorCode code = ErrorCode.INVALID_VALUE;

        ZuoraServiceStub.Error error = new ZuoraServiceStub.Error();
        error.setCode(code);
        error.setMessage("Problem with Subscription");

        result.setErrors(new ZuoraServiceStub.Error[]{error});

        results[0] = result;

        return results;
    }

    public static CreateSubscriptionServiceResponse createAccountSubscriptionServiceResponse() {

        CreateSubscriptionServiceResponse createSubscriptionServiceResponse = CreateSubscriptionServiceResponse.Builder.aCreateSubscriptionServiceResponse()
                .subscribeResults(subscribeResults())
                .userZuora(userWithoutZuoraAccount())
                .period(newSubscriptionCreditCardRequest().getPeriod())
                .build();
        return createSubscriptionServiceResponse;
    }

    public static CreateSubscriptionServiceResponse createSubscriptionServiceResponse() {

        CreateSubscriptionServiceResponse createSubscriptionServiceResponse = CreateSubscriptionServiceResponse.Builder.aCreateSubscriptionServiceResponse()
                .subscribeResults(subscribeResults())
                .userZuora(userWithZuoraAccount())
                .period(newSubscriptionCreditCardRequest().getPeriod())
                .build();
        return createSubscriptionServiceResponse;
    }

    public static InsertUserApiResponse responseInsertUser() {
        InsertUserApiResponse insertUserResponse = InsertUserApiResponse.Builder.anInsertUserApiResponse()
                .success(true)
                .build();
        return insertUserResponse;
    }

    public static ZuoraServiceStub.AmendResult[] successCancelAmendResults() {
        final ZuoraServiceStub.AmendResult amendResult = new ZuoraServiceStub.AmendResult();
        amendResult.setSuccess(true);
        final ZuoraServiceStub.ID amendId = new ZuoraServiceStub.ID();
        amendId.setID("2c92c0f8585839b5015868793c061e96");
        amendResult.setAmendmentIds(new ZuoraServiceStub.ID[] {amendId});
        return new ZuoraServiceStub.AmendResult[] {amendResult};
    }

    public static ZuoraServiceStub.AmendResult[] failCancelAmendResults() {
        final ZuoraServiceStub.AmendResult amendResult = new ZuoraServiceStub.AmendResult();
        amendResult.setSuccess(false);
        com.zuora.api.axis2.ZuoraServiceStub.Error error = new ZuoraServiceStub.Error();
        error.setCode(ErrorCode.INVALID_FIELD);
        error.setField("xxx");
        error.setMessage("Invalid field");
        amendResult.setErrors(new com.zuora.api.axis2.ZuoraServiceStub.Error[] {error});
        return new ZuoraServiceStub.AmendResult[] {amendResult};
    }
}
