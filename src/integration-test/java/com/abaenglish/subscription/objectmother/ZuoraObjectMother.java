package com.abaenglish.subscription.objectmother;

import com.abaenglish.external.zuora.soap.domain.subscription.*;
import com.abaenglish.external.zuora.soap.service.dto.subscription.ZuoraAmendmentResponse;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class ZuoraObjectMother {
    public static Payment paymentProcessedFromRenewal() {
        return Payment.Builder.aPayment()
                .id("2c92c09559b0489c0159cb955aed2fc8")
                .accountId("c92c0f9588ff00b0158923c53b2252b")
                .amount(BigDecimal.valueOf(19.99))
                .effectiveDate("2017-01-23")
                .gateway("Stripe Gateway")
                .gatewayResponse("Payment complete.")
                .paymentNumber("P-00005254")
                .status("Processed")
                .build();
    }

    public static Payment paymentProcessedFromNewSubscription() {
        return Payment.Builder.aPayment()
                .id("2c92c0f859d957c30159ffaf82e226de")
                .accountId("2c92c0f859d957c30159ffaf820a26c1")
                .amount(BigDecimal.valueOf(19.99))
                .effectiveDate("2017-02-02")
                .gateway("Stripe Gateway")
                .gatewayResponse("Payment complete.")
                .paymentNumber("P-00005636")
                .status("Processed")
                .build();
    }

    public static List<InvoicePayment> invoicePaymentsProcessedFromRenewal() {
        return Arrays.asList(InvoicePayment.Builder.anInvoicePayment()
                .id("2c92c09559b0489c0159cb955aee2fc9")
                .invoiceId("2c92c09459b02c5e0159cb9470b957e2")
                .paymentId("2c92c09559b0489c0159cb955aed2fc8")
                .build());
    }

    public static List<InvoicePayment> invoicePaymentsProcessedFromNewSubscription() {
        return Arrays.asList(InvoicePayment.Builder.anInvoicePayment()
                .id("2c92c0f859d957c30159ffaf82e226df")
                .invoiceId("2c92c0f859d957c30159ffaf82a126d2")
                .paymentId("2c92c0f859d957c30159ffaf82e226de")
                .build());
    }

    public static List<InvoiceItemsInfo> invoiceItemsProcessedFromRenewalInfo() {
        return Arrays.asList(InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                .id("2c92c09459b02c5e0159cb9470c257e4")
                .invoiceId("2c92c09459b02c5e0159cb9470b957e2")
                .subscriptionId("2c92c08659b048b70159caa0d87d0e10")
                .build());
    }

    public static List<InvoiceItemsInfo> invoiceItemsProcessedFromNewSubscriptionInfo() {
        return Arrays.asList(InvoiceItemsInfo.Builder.anInvoiceItemsInfo()
                .id("2c92c0f859d957c30159ffaf82a226d4")
                .invoiceId("2c92c0f859d957c30159ffaf82a126d2")
                .subscriptionId("2c92c0f859d957c30159ffaf823626c4")
                .build());
    }

    public static InvoiceInfo invoiceProcessedFromRenewalInfo() {
        return InvoiceInfo.Builder.anInvoiceInfo()
                .id("2c92c09459b02c5e0159cb9470b957e2")
                .amount(BigDecimal.valueOf(19.99))
                .invoiceNumber("INV00005503")
                .build();
    }

    public static InvoiceInfo invoiceProcessedFromNewSubscriptionInfo() {
        return InvoiceInfo.Builder.anInvoiceInfo()
                .id("2c92c0f859d957c30159ffaf82a126d2")
                .amount(BigDecimal.valueOf(19.99))
                .invoiceNumber("INV00005886")
                .build();
    }

    public static List<ZuoraSubscriptionResponse> activeSubscriptionsRenewed() {
        return Arrays.asList(ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse()
                .id("2c92c08659b048b70159caa0d87d0e10")
                .accountId("2c92c0f9588ff00b0158923c53b2252b")
                .originalId("2c92c0f9588ff00b0158923c53e9252e")
                .previousSubscriptionId("2c92c08559261bf601592ab2b665658e")
                .termStartDate("2017-01-23")
                .termEndDate("2017-02-23")
                .build());
    }

    public static List<ZuoraSubscriptionResponse> cancelledSubscriptions() {
        return Arrays.asList(ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse()
                .id("2c92c08659b048b70159caa0d87d0e10")
                .accountId("2c92c0f9588ff00b0158923c53b2252b")
                .originalId("2c92c0f9588ff00b0158923c53e9252e")
                .termStartDate("2017-01-23")
                .termEndDate("2017-02-23")
                .cancelledDate("2017-02-23")
                .build());
    }

    public static List<ZuoraSubscriptionResponse> activeSubscriptionsCreated() {
        return Arrays.asList(ZuoraSubscriptionResponse.Builder.aZuoraSubscriptionResponse()
                .id("2c92c0f859d957c30159ffaf823626c4")
                .accountId("2c92c0f859d957c30159ffaf820a26c1")
                .originalId("2c92c0f859d957c30159ffaf823626c4")
                .termStartDate("2017-02-02")
                .termEndDate("2017-03-02")
                .build());
    }

    public static List<ZuoraAmendmentResponse> renewalAmendPayed() {
        return Arrays.asList(ZuoraAmendmentResponse.Builder.aZuoraAmendmentResponse()
                .id("2c92c08659b048b70159caa0d8520de7")
                .code("A-AM00002133")
                .status("Completed")
                .subscriptionId("2c92c08559261bf601592ab2b665658e")
                .updatedDate("2017-01-23T10:20:55.000+01:00")
                .build());
    }

    public static List<ZuoraAmendmentResponse> cancelAmendPayed() {
        return Arrays.asList(ZuoraAmendmentResponse.Builder.aZuoraAmendmentResponse()
                .id("2c92c08659b048b70159caa0d8520de7")
                .code("A-AM00002133")
                .type("Cancellation")
                .status("Completed")
                .subscriptionId("2c92c08559261bf601592ab2b665658e")
                .updatedDate("2017-01-23T10:20:55.000+01:00")
                .build());
    }

    public static ZuoraRefundResponse refundResponse() {
        return ZuoraRefundResponse.Builder.aRefundInfo()
                .id("2c92c0f859d9576f0159f4e8bb912896")
                .accountId("2c92c0f859d9576e0159f4d0a1b42fff")
                .amount(BigDecimal.valueOf(59.99))
                .refundDate("31/01/2017")
                .build();
    }
}