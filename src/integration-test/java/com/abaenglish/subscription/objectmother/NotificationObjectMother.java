package com.abaenglish.subscription.objectmother;

import com.abaenglish.subscription.service.dto.notification.ZuoraNotificationServiceRequest;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;

import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasXPath;

public class NotificationObjectMother {

    public static String buildRequestXML(ZuoraNotificationServiceRequest serviceRequest) {
        StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
                .append("<callout>")
                .append("<parameter name=\"PaymentReferenceId\">ch_19JOhYHYXNF2v0CuwbUwKxpB</parameter>");
        Optional.ofNullable(serviceRequest.getEventDate())
                .ifPresent( el -> sb.append("<parameter name=\"EventDate\">").append(el).append("</parameter>"));
        Optional.ofNullable(serviceRequest.getEventCategory())
                .ifPresent( el -> sb.append("<parameter name=\"EventCategory\">").append(el).append("</parameter>"));
        Optional.ofNullable(serviceRequest.getEventTimestamp())
                .ifPresent( el -> sb.append("<parameter name=\"EventTimestamp\">").append(el).append("</parameter>"));

        Optional.ofNullable(serviceRequest.getAccountId())
                .ifPresent( el -> sb.append("<parameter name=\"AccountId\">").append(el).append("</parameter>"));
        Optional.ofNullable(serviceRequest.getAccountNumber())
                .ifPresent( el -> sb.append("<parameter name=\"AccountNumber\">").append(el).append("</parameter>"));

        Optional.ofNullable(serviceRequest.getPaymentId())
                .ifPresent( el -> sb.append("<parameter name=\"PaymentId\">").append(el).append("</parameter>"));
        Optional.ofNullable(serviceRequest.getPaymentStatus())
                .ifPresent( el -> sb.append("<parameter name=\"PaymentStatus\">").append(el).append("</parameter>"));

        Optional.ofNullable(serviceRequest.getPaymentGatewayResponse())
                .ifPresent( el -> sb.append("<parameter name=\"PaymentGatewayResponse\">").append(el).append("</parameter>"));
        Optional.ofNullable(serviceRequest.getPaymentGatewayResponseCode())
                .ifPresent( el -> sb.append("<parameter name=\"PaymentGatewayResponseCode\">").append(el).append("</parameter>"));
        Optional.ofNullable(serviceRequest.getPaymentGatewayState())
                .ifPresent( el -> sb.append("<parameter name=\"PaymentGatewayState\">").append(el).append("</parameter>"));

        Optional.ofNullable(serviceRequest.getSubscriptionVersionAmendmentId())
                .ifPresent( el -> sb.append("<parameter name=\"SubscriptionVersionAmendmentId\">").append(el).append("</parameter>"));
        Optional.ofNullable(serviceRequest.getSubscriptionVersionAmendmentType())
                .ifPresent( el -> sb.append("<parameter name=\"SubscriptionVersionAmendmentType\">").append(el).append("</parameter>"));
        Optional.ofNullable(serviceRequest.getSubscriptionOriginalId())
                .ifPresent( el -> sb.append("<parameter name=\"SubscriptionOriginalId\">").append(el).append("</parameter>"));

        Optional.ofNullable(serviceRequest.getRefundId())
                .ifPresent( el -> sb.append("<parameter name=\"RefundId\">").append(el).append("</parameter>"));

        sb.append("</callout>");
        return sb.toString();
    }

    public static ResponseSpecification buildResponseSpecificationXml(final boolean success, Optional<String> errors) {
        final StringBuilder resumeSB = new StringBuilder("success: ").append(success);
        errors.ifPresent( e -> resumeSB.append(", errors: ").append(e) );

        return new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectContentType(ContentType.XML)
                .expectBody(hasXPath("/ZuoraNotificationResponse/resume", containsString(resumeSB.toString())))
                .build();
    }


    public static ZuoraNotificationServiceRequest zuoraNotificationPaymentDeclinedReceivedServiceRequest() {
        return ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                        .eventDate("2016-11-18")
                        .eventCategory("PaymentDeclined")
                        .eventTimestamp("2016-11-18T04:13:48.211-0800")
                        .accountId("2c92c0f95876a6bd0158775b61472ae8")
                        .accountNumber("A00001896")
                        .paymentGatewayState("Submitted")
                        .paymentGatewayResponse("Payment declined.")
                        .paymentGatewayResponseCode("200")
                        .paymentId("2c92c0f95876a6bd0158775b62602b14")
                        .paymentStatus("Declined")
                        .build();
    }

    public static ZuoraNotificationServiceRequest zuoraNotificationPaymentDeclinedAlreadyProcessedServiceRequest() {
        return ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                .eventDate("2016-11-18")
                .eventCategory("PaymentDeclined")
                .eventTimestamp("2016-11-18T04:13:48.211-0800")
                .accountId("2c92c0f95876a6bd0158775b61472ae8")
                .accountNumber("A00001896")
                .paymentGatewayState("Submitted")
                .paymentGatewayResponse("Payment declined.")
                .paymentGatewayResponseCode("200")
                .paymentId("1c92c0f95876a6bd0158775b62602b14")
                .paymentStatus("Declined")
                .build();
    }

    public static ZuoraNotificationServiceRequest zuoraNotificationPaymentProcessedFromRenewalReceivedServiceRequest() {
        return ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                        .eventDate("2017-01-23")
                        .eventCategory("PaymentProcessed")
                        .eventTimestamp("2017-01-23T05:48:00.656-0800")
                        .accountId("2c92c0f9588ff00b0158923c53b2252b")
                        .accountNumber("A00002092")
                        .paymentGatewayState("Submitted")
                        .paymentGatewayResponse("Payment complete.")
                        .paymentGatewayResponseCode("200")
                        .paymentId("2c92c09559b0489c0159cb955aed2fc8")
                        .paymentStatus("Processed")
                        .build();
    }

    public static ZuoraNotificationServiceRequest zuoraNotificationPaymentProcessedFromNewSubscriptionReceivedServiceRequest() {
        return ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                .eventDate("2017-02-02")
                .eventCategory("PaymentProcessed")
                .eventTimestamp("2017-02-02T08:36:50.154-0800")
                .accountId("2c92c0f859d957c30159ffaf820a26c1")
                .accountNumber("A00003366")
                .paymentGatewayState("Submitted")
                .paymentGatewayResponse("Payment complete.")
                .paymentGatewayResponseCode("200")
                .paymentId("2c92c0f859d957c30159ffaf82e226de")
                .paymentStatus("Processed")
                .build();
    }

    public static ZuoraNotificationServiceRequest zuoraNotificationPaymentProcessedAlreadyExistsServiceRequest() {
        return ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                .eventDate("2017-01-23")
                .eventCategory("PaymentProcessed")
                .eventTimestamp("2017-01-23T05:48:00.656-0800")
                .accountId("2c92c0f9588ff00b0158923c53b2252b")
                .accountNumber("A00002092")
                .paymentGatewayState("Submitted")
                .paymentGatewayResponse("Payment complete.")
                .paymentGatewayResponseCode("200")
                .paymentId("2c92c09559b0489c0159cb955aed2fc9")
                .paymentStatus("Processed")
                .build();
    }

    public static ZuoraNotificationServiceRequest zuoraNotificationRenealAmendmentProcessedServiceRequest() {
        return ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                .eventDate("2017-02-06")
                .eventCategory("AmendmentProcessed")
                .eventTimestamp("2017-02-06T01:31:48.757-0800")
                .accountId("2c92c0f9588ff00b0158923c53b2252b")
                .accountNumber("A00002092")
                .subscriptionVersionAmendmentId("2c92c08659b048b70159caa0d8520de7")
                .subscriptionVersionAmendmentType("Renewal")
                .subscriptionOriginalId("2c92c0f9588ff00b0158923c53e9252e")
                .build();
    }

    public static ZuoraNotificationServiceRequest zuoraNotificationCancelAmendmentProcessedServiceRequest() {
        return ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                .eventDate("2017-02-06")
                .eventCategory("AmendmentProcessed")
                .eventTimestamp("2017-02-06T01:31:48.757-0800")
                .accountId("2c92c0f9588ff00b0158923c53b2252b")
                .accountNumber("A00002092")
                .subscriptionVersionAmendmentId("2c92c08659b048b70159caa0d8520de7")
                .subscriptionVersionAmendmentType("Cancellation")
                .subscriptionOriginalId("2c92c0f9588ff00b0158923c53e9252e")
                .build();
    }

    public static ZuoraNotificationServiceRequest zuoraNotificationRefundProcessedServiceRequest() {
        return ZuoraNotificationServiceRequest.Builder.aZuoraNotificationServiceRequest()
                .eventDate("2017-01-31")
                .eventCategory("PaymentRefundProcessed")
                .eventTimestamp("2017-01-31T01:31:48.757-0800")
                .accountId("2c92c0f859d9576e0159f4d0a1b42fff")
                .accountNumber("A00003172")
                .refundId("2c92c0f859d9576f0159f4e8bb912896")
                .build();
    }



}
